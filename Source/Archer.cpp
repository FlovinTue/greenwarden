// Archer.cpp

#include "stdafx.h"
#include "Archer.h"
#include "ArcherProjectile.h"

Archer::Archer(std::vector<Entity*>* entities, sf::Texture* unitTexture, sf::Texture* projectileTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder)
{
	m_mainTarget = mainTarget;
	m_x = x;
	m_y = y;
	m_sprite.setTexture(*unitTexture);
	m_projectileTexture = projectileTexture;
	m_entities = entities;
	m_pathfinder = pathfinder;

	Initialize();

	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);
	HealthBar* healthBar = new HealthBar(this);
	entities->push_back(healthBar);
}
Archer::~Archer()
{
	if (m_collider)
		delete m_collider;
}
SubType Archer::GetSubType()
{
	return E_ARCHER;
}
void Archer::Deactivate()
{
	m_dOTEffects.clear();
	m_active = false;
	m_effectHost->PlayEffect("archerdeath", false);
}
void Archer::Reactivate(int x, int y)
{
	m_attackTimer.restart();
	m_tempTarget = nullptr;
	m_slowed = false;
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_currentHealth = m_maxHealth;

	m_x = x;
	m_y = y;
	m_sprite.setPosition(m_x, m_y);

	AssignPath();

	m_active = true;
}
void Archer::Initialize()
{
	m_w = 64;
	m_h = 64;
	m_tempTarget = nullptr;
	m_attackTimer.restart();
	m_slowed = false;
	m_attackSpeed = AttributeManager::GetAttribute("Enemy_archer_AS");
	m_movementSpeed = AttributeManager::GetAttribute("Enemy_archer_movement");
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_directionX = 0.0f;
	m_directionY = 0.0f;
	m_slowStop = 4;
	m_detectRadius = AttributeManager::GetAttribute("Enemy_archer_detectRange");
	m_attackRange = AttributeManager::GetAttribute("Enemy_archer_range");
	m_damage = 0;
	m_maxHealth = AttributeManager::GetAttribute("Enemy_archer_HP");
	m_currentHealth = m_maxHealth;
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);

	PreGenerateProjectiles();

	m_active = false;
}
void Archer::Attack()
{
	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->IsActive() == false)
		{
			if (m_entities->at(i)->GetType() == E_ARCHERPROJECTILE)
			{
				if (m_tempTarget)
				{
					ArcherProjectile* archerProjectile = static_cast<ArcherProjectile*>(m_entities->at(i));
					archerProjectile->Reactivate(m_tempTarget, GetX() + (GetW() / 3), GetY() + (GetH() / 3));
					break;
				}
				else if (m_mainTarget)
				{
					ArcherProjectile* archerProjectile = static_cast<ArcherProjectile*>(m_entities->at(i));
					archerProjectile->Reactivate(m_mainTarget, GetX() + (GetW() / 3), GetY() + (GetH() / 3));
					break;
				}
			}
		}
	}

	m_attackTimer.restart();

	AnimateAttack();
}
void Archer::AnimateMovement()
{
	if (m_directionX < 0 && abs(m_directionX) + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "archerwalkleft", true);
	else if (m_directionX > 0 && m_directionX + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "archerwalkright", true);
	else if (m_directionY < 0 && abs(m_directionY)> abs(m_directionX))
		AnimationStore::Play(this, "archerwalkbackward", true);
	else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		AnimationStore::Play(this, "archerwalkforward", true);
}
void Archer::AnimateAttack()
{
	if (m_directionX < 0 && abs(m_directionX)>abs(m_directionY))
		AnimationStore::Play(this, "archerattackleft", false);
	else if (m_directionX > 0 && m_directionX > abs(m_directionY))
		AnimationStore::Play(this, "archerattackright", false);
	else if (m_directionY < 0 && abs(m_directionY)>abs(m_directionX))
		AnimationStore::Play(this, "archerattackbackward", false);
	else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		AnimationStore::Play(this, "archerattackforward", false);
}
void Archer::PreGenerateProjectiles()
{
	for (int i = 0; i < 5; i++)
	{
		ArcherProjectile* archerProjectile = new ArcherProjectile(-100, -100, m_projectileTexture);
		m_entities->push_back(archerProjectile);
	}
}