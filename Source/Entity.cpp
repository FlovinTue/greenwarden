// Entity.cpp

#include "stdafx.h"
#include "Entity.h"

void Entity::Update(float deltatime) 
{
}
sf::Sprite* Entity::GetSprite() 
{
	return &m_sprite;
}
float Entity::GetX() 
{
	return m_x;
}
float Entity::GetY() 
{
	return m_y;
}
int Entity::GetW() 
{
	return m_w;
}
int Entity::GetH() 
{
	return m_h;
}
bool Entity::IsActive() 
{
	return m_active;
}
Collider* Entity::GetCollider()
{
	return m_collider;
}
EType Entity::GetType()
{
	return E_UNKNOWN;
}
void Entity::Deactivate() 
{
	m_active = false;
}