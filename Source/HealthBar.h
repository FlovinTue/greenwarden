// HealthBar.h

#pragma once

#include "Entity.h"

class HealthBar : public Entity
{
public:
	HealthBar(Entity* host);
	~HealthBar();

	void Update(float deltatime);
	EType GetType();

	void SetReferenceHealthValue(int referenceHealthValue);
private:
	void RefreshBar(int currentHealthValue);
	int GetHostCurrentHealth();

private:
	float m_referenceHealthValue;
	int m_healthBarOffset;
	Entity* m_host;
};