#pragma once
#include "Entity.h"
class PlayerProjectile : public Entity
{
public:

	PlayerProjectile(std::vector<Entity*>* entities, int direction, int damage, int speed, float x, float y);
	~PlayerProjectile();

	void Update(float deltatime);
	EType GetType();
	void PlaceTower();
	void Reactivate(std::vector<Entity*>* entities, int direction, int damage, int speed, float x, float y);
private:
	float start_x;
	float start_y;
	std::vector<Entity*>* m_entities;
	sf::Texture m_texture;
	int m_damage;
	int m_speed;
	int m_direction;
	
	
};