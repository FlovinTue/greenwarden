// AbstractEnemy.h

#pragma once

#include "Entity.h"
#include "AttributeManager.h"
#include "HealthBar.h"
#include "Tower.h"
#include "LifeTree.h"
#include "Player.h"
#include "AnimationStore.h"
#include "DOTEffect.h"
#include "Sound.h"
#include "EffectHost.h"

class Pathfinding;

enum SubType
{
	E_LOGGERS,
	E_LOGGERL,
	E_ARCHER,
	E_FLAMETHROWER,
	E_SUICIDESQUAD
};

class AbstractEnemy : public Entity
{
public:
	~AbstractEnemy();

	virtual SubType GetSubType() = 0;
	virtual void Deactivate() = 0;
	virtual void Reactivate(int x, int y) = 0;

	virtual void Update(float deltatime);
	virtual EType GetType();
	virtual void SubstractHealth(float value);
	virtual void AddHealth(int value);
	virtual int GetCurrentHealth();
	virtual int GetMaxHealth();
	virtual void SubstractHealthOverTime(float totalDamage, int duration);
	virtual void ReduceSpeed(float percentage, int duration);
	virtual void SetMainTarget(Entity* target);
	virtual void SetTempTarget(Entity* target);

protected:
	virtual void Initialize() = 0;
	virtual void Attack() = 0;
	virtual void AnimateMovement() = 0;
	virtual void AnimateAttack() = 0;

	virtual int GetWalkingDistance(sf::Vector2f targetLoc);
	virtual int GetPixelDistance(sf::Vector2f targetLoc);
	virtual void RefreshOverTimeEffects();
	virtual bool ScanForTarget();
	virtual void MoveToNextLocation(float deltatime);
	virtual void AssignPath();
	virtual void SetAttackDirection();

protected:
	Entity* m_mainTarget;
	Entity* m_tempTarget;

	Pathfinding* m_pathfinder;

	EffectHost* m_effectHost;

	std::vector<Entity*>* m_entities;
	std::vector<sf::Vector2f> m_targetNodes;
	std::vector<DOTEffect> m_dOTEffects;

	sf::Clock m_slowTimer;
	sf::Clock m_attackTimer;
	sf::Clock m_scanTimer;
	sf::Clock m_playerFollowUpdateTimer;

	bool m_slowed;

	float m_movementSpeed;
	float m_movementSpeedFactor;
	float m_attackSpeed;
	float m_attackSpeedFactor;
	float m_directionX;
	float m_directionY;

	int m_slowStop;
	int m_detectRadius;
	int m_attackRange;
	int m_damage;
	int m_maxHealth;
	float m_currentHealth;

};