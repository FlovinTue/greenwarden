// Enemy.cpp

#include "stdafx.h"
#include "AbstractEnemy.h"
#include "Pathfinding.h"


AbstractEnemy::~AbstractEnemy()
{
}
void AbstractEnemy::Update(float deltatime)
{
	if (IsActive())
	{
		if (m_currentHealth <= 0)
		{
			Deactivate();
			return;
		}

		if (!m_tempTarget && m_scanTimer.getElapsedTime().asMilliseconds() > (500 + rand()% 150))
			if (ScanForTarget())
				AssignPath();

		sf::Vector2f targetLoc;

		if (m_tempTarget)
		{
			targetLoc.x = m_tempTarget->GetX();
			targetLoc.y = m_tempTarget->GetY();
			if (GetPixelDistance(targetLoc) < m_attackRange)
			{
				if (m_attackTimer.getElapsedTime().asMilliseconds() > (m_attackSpeed * m_attackSpeedFactor))
				{
					SetAttackDirection();
					Attack();
				}
			}
			else if (m_tempTarget->GetType() == E_PLAYER)
			{
				if (m_playerFollowUpdateTimer.getElapsedTime().asMilliseconds() > (500 + rand()%150))
				{
					if (!m_pathfinder->IsInaccessible(targetLoc))
					{
						m_tempTarget = nullptr;
						AssignPath();
					}
					else
					{
						AssignPath();
						m_playerFollowUpdateTimer.restart();
					}
				}
				MoveToNextLocation(deltatime);
			}
			else
				MoveToNextLocation(deltatime);

			if (m_tempTarget &&
				m_tempTarget->IsActive() != true ||
				GetPixelDistance(targetLoc) > (m_detectRadius * 2))
			{
				m_tempTarget = nullptr;
				AssignPath();
			}
		}
		else if (!m_tempTarget && m_mainTarget)
		{
			targetLoc.x = m_mainTarget->GetX();
			targetLoc.y = m_mainTarget->GetY();
			if (GetPixelDistance(targetLoc) < m_attackRange)
			{
				if (m_attackTimer.getElapsedTime().asMilliseconds() > (m_attackSpeed * m_attackSpeedFactor))
				{
					SetAttackDirection();
					Attack();
				}
			}
			else
				MoveToNextLocation(deltatime);
		}
		else
			MoveToNextLocation(deltatime);

		RefreshOverTimeEffects();

		m_sprite.setPosition(m_x, m_y);

		short textureRectW = m_sprite.getTextureRect().width;
		short textureRectH = m_sprite.getTextureRect().height;
		unsigned short originX = (textureRectW - 64) / 2;
		unsigned short originY = (textureRectH - 64) / 2;
		m_sprite.setOrigin(sf::Vector2f{ static_cast<float>(originX), static_cast<float>(originY) });

		m_collider->Refresh();
	}
}
EType AbstractEnemy::GetType()
{
	return E_ENEMY;
}
void AbstractEnemy::SubstractHealth(float value)
{
	m_currentHealth -= value;

	if (AnimationStore::GetActiveAnimation(m_effectHost) != "poisoneffect"&&
		AnimationStore::GetActiveAnimation(m_effectHost) != "holdeffect"&&
		AnimationStore::GetActiveAnimation(m_effectHost) != "sloweffect")
		AnimationStore::Play(m_effectHost, "bloodeffect1", false);

	if (m_currentHealth < 0)
		m_currentHealth = 0;
}
void AbstractEnemy::AddHealth(int value)
{
	m_currentHealth += value;

	if (m_currentHealth > m_maxHealth)
		m_currentHealth = m_maxHealth;
}
int AbstractEnemy::GetCurrentHealth()
{
	return m_currentHealth;
}
int AbstractEnemy::GetMaxHealth()
{
	return m_maxHealth;
}
int AbstractEnemy::GetWalkingDistance(sf::Vector2f targetLoc)
{
	int walkingDistance = 0;

	std::vector<sf::Vector2f> route{ m_pathfinder->getPath(sf::Vector2f{ GetX(), GetY() }, targetLoc) };

	for (int i = 0; i < route.size(); i++)
	{
		walkingDistance += GetPixelDistance(route[route.size()-1]);
		route.pop_back();
	}
	return walkingDistance;
}
int AbstractEnemy::GetPixelDistance(sf::Vector2f targetLoc)
{
	int originX = m_x;
	int originY = m_y;

	int targetX = targetLoc.x;
	int targetY = targetLoc.y;

	int distanceX = abs(originX - targetX);
	int distanceY = abs(originY - targetY);

	int distance = sqrt((distanceX * distanceX) + (distanceY * distanceY));

	return distance;
}
void AbstractEnemy::SetMainTarget(Entity* target)
{
	m_mainTarget = target;
}
void AbstractEnemy::SetTempTarget(Entity* target)
{
	m_tempTarget = target;
}
void AbstractEnemy::ReduceSpeed(float percentage, int duration)
{
	if (!((m_slowStop - m_slowTimer.getElapsedTime().asSeconds()) > duration)||
		!(1.0f-(percentage /100.f))>m_movementSpeedFactor)
	{
		m_slowed = true;
		m_slowStop = duration;
		m_movementSpeedFactor = 1.0f - (percentage / 100.0f);
		m_attackSpeedFactor = 1.0f + (percentage / 100.0f);
		m_slowTimer.restart();

		if (m_movementSpeedFactor == 0.0f)
			AnimationStore::Play(m_effectHost, "holdeffect", true);
		else
			AnimationStore::Play(m_effectHost, "sloweffect", true);
	}
}
void AbstractEnemy::SubstractHealthOverTime(float totalDamage, int duration)
{
	DOTEffect dOTEffect;
	
	dOTEffect.m_check = 0;
	dOTEffect.m_totalDamage = totalDamage;
	dOTEffect.m_duration = duration;
	dOTEffect.m_timer.restart();

	m_dOTEffects.push_back(dOTEffect);

}
void AbstractEnemy::RefreshOverTimeEffects()
{
	if (m_dOTEffects.size() > 0)
	{
		for (int i = 0; i < m_dOTEffects.size(); i++)
		{
			if (m_dOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_dOTEffects[i].m_check +1))
			{
				float tickDamage = m_dOTEffects[i].m_totalDamage / m_dOTEffects[i].m_duration;

				m_currentHealth -= tickDamage;

				if (m_currentHealth < 0)
					m_currentHealth = 0;

				m_dOTEffects[i].m_check++;

				if (AnimationStore::GetActiveAnimation(m_effectHost) != "holdeffect" &&
					AnimationStore::GetActiveAnimation(m_effectHost) != "sloweffect")
					AnimationStore::Play(m_effectHost, "poisoneffect", true);

				if (m_dOTEffects[i].m_check >= m_dOTEffects[i].m_duration )
					m_dOTEffects.erase(m_dOTEffects.begin() + i);
			}
		}
		if (m_dOTEffects.size() == 0 &&
			AnimationStore::GetActiveAnimation(m_effectHost) =="doteffect")
			AnimationStore::Stop(m_effectHost);
	}
	if (m_slowed)
	{
		if (m_slowTimer.getElapsedTime().asSeconds() >= m_slowStop)
		{
			m_slowed = false;
			m_movementSpeedFactor = 1.0f;
			m_attackSpeedFactor = 1.0f;
			AnimationStore::Stop(m_effectHost);
		}
	}
}
bool AbstractEnemy::ScanForTarget()
{
	Entity* target = nullptr;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->GetType() == E_TOWER ||
			m_entities->at(i)->GetType() == E_PLAYER)
		{
			if (m_entities->at(i)->IsActive())
			{
				sf::Vector2f radiusCheck;
				radiusCheck.x = m_entities->at(i)->GetX();
				radiusCheck.y = m_entities->at(i)->GetY();

				if (GetPixelDistance(radiusCheck) < m_detectRadius)
				{
					if (!m_pathfinder->IsInaccessible(radiusCheck))
					{
						return false;
					}
					if (GetWalkingDistance(radiusCheck) < GetPixelDistance(radiusCheck)*1.5)
					{
						if (!target)
							target = m_entities->at(i);
						else
						{
							sf::Vector2f targetLoc;
							targetLoc.x = target->GetX();
							targetLoc.y = target->GetY();
							sf::Vector2f otherTargetLoc;
							otherTargetLoc.x = m_entities->at(i)->GetX();
							otherTargetLoc.y = m_entities->at(i)->GetY();
							if (GetPixelDistance(otherTargetLoc) < GetPixelDistance(targetLoc))
								target = m_entities->at(i);
						}
					}
				}
			}
		}
	}

	m_scanTimer.restart();

	if (!target)
		return false;

	else if (target)
	{
		m_tempTarget = target;
		return true;
	}
	return false;
}
void AbstractEnemy::MoveToNextLocation(float deltatime)
{
	if (m_targetNodes.size() > 0)
	{
		if (m_mainTarget || m_tempTarget)
		{
			float distX = (m_targetNodes[m_targetNodes.size()-1].x - GetX());
			float distY = (m_targetNodes[m_targetNodes.size()-1].y - GetY());

			float units = 1 / (abs(distX) + abs(distY));

			m_directionX = (distX * units);
			m_directionY = (distY * units);

			m_x += m_directionX * m_movementSpeed * m_movementSpeedFactor * deltatime;
			m_y += m_directionY * m_movementSpeed * m_movementSpeedFactor * deltatime;

			if (m_x < (m_targetNodes[m_targetNodes.size() - 1].x + 2.0f) &&
				m_x >(m_targetNodes[m_targetNodes.size() - 1].x - 2.0f) &&
				m_y < (m_targetNodes[m_targetNodes.size() - 1].y + 2.0f) &&
				m_y >(m_targetNodes[m_targetNodes.size() - 1].y - 2.0f))
			{
				m_targetNodes.pop_back();
			}

			AnimateMovement();
		}
	}
}
void AbstractEnemy::AssignPath()	// Should assign a whole array of nodes
{	
	m_targetNodes.clear();

	sf::Vector2f targetLocation;
	sf::Vector2f thisLocation{ GetX(), GetY() };

	if (m_tempTarget)			
	{
		targetLocation.x = m_tempTarget->GetX();
		targetLocation.y = m_tempTarget->GetY();
	}
	else if (m_mainTarget)
	{
		targetLocation.x = m_mainTarget->GetX();
		targetLocation.y = m_mainTarget->GetY();
	}

	if (m_tempTarget || m_mainTarget)
		m_targetNodes = m_pathfinder->getPath(thisLocation, targetLocation);
}
void AbstractEnemy::SetAttackDirection()
{
	if (m_tempTarget)
	{
		float distX = (m_tempTarget->GetX() - GetX());
		float distY = (m_tempTarget->GetY() - GetY());
		float units = 1 / (abs(distX) + abs(distY));
		m_directionX = (distX * units);
		m_directionY = (distY * units);
	}
	else if (m_mainTarget)
	{
		float distX = (m_mainTarget->GetX() - GetX());
		float distY = (m_mainTarget->GetY() - GetY());
		float units = 1 / (abs(distX) + abs(distY));
		m_directionX = (distX * units);
		m_directionY = (distY * units);
	}
}