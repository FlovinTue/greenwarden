/// Sound.h
//
#pragma once


class Sound
{
public:
	Sound();
	Sound(const char* filename);

	bool loadFromFile(const char* filename);

	void play();
	void loop();
	void pause();
	void stop();

private:
	const char* m_filename;

	static std::map<const char*, sf::SoundBuffer> m_sounds;
	static std::map<const char*, sf::Sound> m_channels;
};