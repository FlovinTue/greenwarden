// AttributeManager.h

#pragma once
#include <string>
#include <map>


class AttributeManager
{
public:
	AttributeManager();
	~AttributeManager();

	static int GetAttribute(const std::string &attribute);
	static void Initialize(const std::string &configFile);
private:
	static void GenerateConfig(const std::string &configFile);

private:
	static std::map<std::string, int> m_storage;
	static std::string m_configFile;
};