// LoggerSmall.cpp

#include "stdafx.h"
#include "LoggerSmall.h"


LoggerSmall::LoggerSmall(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder)
{
	m_mainTarget = mainTarget;
	m_x = x;
	m_y = y;
	m_sprite.setTexture(*unitTexture);
	m_entities = entities;
	m_pathfinder = pathfinder;

	Initialize();
	
	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);
	HealthBar* healthBar = new HealthBar(this);
	entities->push_back(healthBar);
}
LoggerSmall::~LoggerSmall()
{
	if (m_collider)
		delete m_collider;
}
SubType LoggerSmall::GetSubType()
{
	return E_LOGGERS;
}
void LoggerSmall::Reactivate(int x, int y)
{
	m_attackTimer.restart();
	m_tempTarget = nullptr;
	m_slowed = false;
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_currentHealth = m_maxHealth;

	m_x = x;
	m_y = y;
	m_sprite.setPosition(m_x, m_y);
	AssignPath();

	m_active = true;

}
void LoggerSmall::Deactivate()
{
	m_dOTEffects.clear();
	m_active = false;
	m_effectHost->PlayEffect("loggersmalldeath", false);
}
void LoggerSmall::Initialize()
{
	m_w = 64;
	m_h = 64;
	m_tempTarget = nullptr;
	m_attackTimer.restart();
	m_slowed = false;
	m_attackSpeed = AttributeManager::GetAttribute("Enemy_loggersmall_AS");
	m_movementSpeed = AttributeManager::GetAttribute("Enemy_loggersmall_movement");
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_directionX = 0.0f;
	m_directionY = 0.0f;
	m_slowStop = 4;
	m_detectRadius = AttributeManager::GetAttribute("Enemy_loggersmall_detectRange");
	m_attackRange = AttributeManager::GetAttribute("Enemy_loggersmall_range");
	m_damage = AttributeManager::GetAttribute("Enemy_loggersmall_damage");
	m_maxHealth = AttributeManager::GetAttribute("Enemy_loggersmall_HP");
	m_currentHealth = m_maxHealth;
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);

	m_active = false;
}
void LoggerSmall::Attack()
{
	if (m_tempTarget)
	{
		if (m_tempTarget->GetType() == E_TOWER)
		{
			Tower* tower = static_cast<Tower*>(m_tempTarget);
			if (tower->GetTowerType() == 5)
				SubstractHealth(AttributeManager::GetAttribute("Tower_rangedefense_damage"));
			tower->SubstractHealth(m_damage);
			m_attackTimer.restart();
		}
		else if (m_tempTarget->GetType() == E_PLAYER)
		{
			Player* player = static_cast<Player*>(m_tempTarget);
			player->dealdamage(m_damage);
			m_attackTimer.restart();
		}
	}
	else if (m_mainTarget)
	{
		if (m_mainTarget->GetType() == E_LIFETREE)
		{
			LifeTree* lifeTree = static_cast<LifeTree*>(m_mainTarget);
			lifeTree->SubstractHealth(m_damage);
			m_attackTimer.restart();
		}
	}

	AnimateAttack();
}
void LoggerSmall::AnimateMovement()
{
	if (m_directionX < 0 && abs(m_directionX) + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "loggersmallwalkleft", true);
	else if (m_directionX > 0 && m_directionX + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "loggersmallwalkright", true);
	else if (m_directionY < 0 && abs(m_directionY)> abs(m_directionX))
		AnimationStore::Play(this, "loggersmallwalkbackward", true);
	else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		AnimationStore::Play(this, "loggersmallwalkforward", true);
}
void LoggerSmall::AnimateAttack()
{
	 if (m_directionX < 0 && abs(m_directionX)>abs(m_directionY))
		 AnimationStore::Play(this, "loggersmallattackleft", false);
	 else if (m_directionX > 0 && m_directionX > abs(m_directionY))
		 AnimationStore::Play(this, "loggersmallattackright", false);
	 else if (m_directionY < 0 && abs(m_directionY)>abs(m_directionX))
		 AnimationStore::Play(this, "loggersmallattackbackward", false);
	 else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		 AnimationStore::Play(this, "loggersmallattackforward", false);
}