// Tower.h

#pragma once
#include "Entity.h"
#include "HealthBar.h"
#include "Sound.h"

class DOTEffect;
class HOTEffect;
class Tile;
class EffectHost;

class Tower : public Entity
{
public:
	Tower(int x, int y, std::vector<Entity*>* entities, int towerType, sf::Texture* towerTexture, sf::Texture* projectileTexture, Tile* tile);
	~Tower();

	void SetTarget(Entity* target);
	Entity* GetTarget();

	void Update(float deltatime);
	EType GetType();

	void SetAttackSpeed(int attackSpeed);
	int GetAttackSpeed();
	int GetRadius();
	void SetRadius(int radius);
	void SubstractHealth(float value);
	void SubstractHealthOverTime(float totalDamage, int duration);
	void AddHealth(float value);
	void AddHealthOverTime(float totalHealing, int duration);
	int GetCurrentHealth();
	int GetMaxHealth();
	int GetTowerType(); 

	void Deactivate();
	void Reactivate(int x, int y, int towerType, sf::Texture* towerTexture, sf::Texture* projectileTexture, Tile* tile);

private:
	void SpawnPowerUp();
	void Attack();
	int GetPixelDistance(Entity* entity);
	Entity* GetClosestEnemy();
	void SetTowerType(int towerType); 
	void PreGenerateProjectiles();
	void RefreshOverTimeEffects();
	void PreGeneratePowerup();
	void AnimateAttack();
	void Spawn();
	void Despawn();

private:
	Entity* m_target;
	sf::Clock m_attackTimer;
	sf::Clock m_spawnTimer;
	sf::Clock m_despawnTimer;
	sf::Texture* m_projectileTexture;

	HealthBar* m_healthBar;
	Tile* m_tile;
	EffectHost* m_effectHost;

	std::vector<Entity*>* m_entities;
	std::vector<DOTEffect> m_dOTEffects;
	std::vector<HOTEffect> m_hOTEffects;

	float m_currentHealth;
	int m_projectileType; 
	int m_towerType;	  
	int m_radius;
	int m_attackSpeed;
	int m_maxHealth;
	int m_randtype;
	bool m_spawned;
	bool m_despawned;

	Sound m_soundDeath;
	Sound m_soundSpawn;
	Sound m_soundAttack;

};