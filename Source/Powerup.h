
#pragma once 

#include "Entity.h"

class PowerUp : public Entity
{
public:
	PowerUp(std::vector<Entity*>* entities, int type, float x, float y);
	~PowerUp();

	void Update(float deltatime);
	EType GetType();
	int PowerUpType();
	void Reactivate(int type, float x, float y);
private:
	std::vector<Entity*>* m_entities;
	sf::Texture m_texture;
	int m_type;
};