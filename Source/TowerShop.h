// TowerShop.h

#include "TextureManager.hpp"

class Entity;
class Tile;
class TowerShop
{
public:
	TowerShop(TextureManager* textureManager, std::vector<Entity*>* entityVector);
	~TowerShop();

	void CreateTower(int towerType, int x, int y, Tile* tile);

private:
	void AllocateTextures(TextureManager* textureManager);
	void PreGenerateTowers();

private:
	std::vector<Entity*>* m_entities;

	sf::Texture* m_tower1Texture;
	sf::Texture* m_tower2Texture;
	sf::Texture* m_tower3Texture;
	sf::Texture* m_tower4Texture;
	sf::Texture* m_tower5Texture;
	sf::Texture* m_tower6Texture;
	sf::Texture* m_tower7Texture;
	sf::Texture* m_tower8Texture;
	sf::Texture* m_tower9Texture;
	sf::Texture* m_projectile1Texture;
	sf::Texture* m_projectile2Texture;
	sf::Texture* m_projectile3Texture;
	sf::Texture* m_projectile4Texture;
	sf::Texture* m_projectile5Texture;
	sf::Texture* m_projectile6Texture;
};