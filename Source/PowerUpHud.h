#pragma once


#include "Entity.h"



class PowerUpHud : public Entity
{
public:
	PowerUpHud(float x, float y, int type);
	~PowerUpHud();

	void Update(float deltatime);
	EType GetType();
	void PlayAnimation();
	


private:
	int m_type;
	//sf::Sprite m_sprite;
	sf::Texture m_texture;
	
};