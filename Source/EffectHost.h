// EffectHost.h

#pragma once

#include "Entity.h"

class EffectHost : public Entity
{
public:
	EffectHost(Entity* host);
	~EffectHost();

	void Update(float deltatime);
	EType GetType();
	void PlayEffect(const std::string& effectName, bool looping);

private:
	Entity* m_host;
};