// HealthBar.cpp

#include "stdafx.h"
#include "HealthBar.h"
#include "Tower.h"
#include "AbstractEnemy.h"
#include "LifeTree.h"
#include "Player.h"

HealthBar::HealthBar(Entity* host)
{
	m_active = true;
	m_host = host;
	m_referenceHealthValue = GetHostCurrentHealth();
	m_healthBarOffset = 5;	
	m_w = m_host->GetW();  
	m_h = m_host->GetW()/10;

	sf::Texture rectangle;
	rectangle.create(m_w, m_h);
	m_sprite.setTexture(rectangle);

	RefreshBar(GetHostCurrentHealth());
	m_collider = nullptr;
}
HealthBar::~HealthBar()
{
}
void HealthBar::Update(float deltatime)
{
	if (m_host->IsActive())
		RefreshBar(GetHostCurrentHealth());
}
EType HealthBar::GetType()
{
	return E_HEALTHBAR;
}
void HealthBar::SetReferenceHealthValue(int referenceHealthValue)
{
	m_referenceHealthValue = referenceHealthValue;
}
void HealthBar::RefreshBar(int currentHealthValue)
{
	m_x = m_host->GetX();
	m_y = m_host->GetY() - GetH() - m_healthBarOffset;
	m_sprite.setPosition(GetX(), GetY());

	if (m_x > 0)
		bool hej = true;

	float health = currentHealthValue;

	if (health < 0)
		health = 0;

	float healthRatio = health / m_referenceHealthValue;

	int red, green, blue, alpha;

	red = 150;
	green = 200 + (200 * (healthRatio - 1));
	blue = 0;
	alpha = 205;

	int barWidth = GetW() * healthRatio;

	m_sprite.setColor(sf::Color(red, green, blue, alpha));
	m_sprite.setTextureRect(sf::IntRect(0, 0, barWidth, GetH()));
	
}
int HealthBar::GetHostCurrentHealth()
{
	switch (m_host->GetType())
	{
	case(E_TOWER) :
	{
		Tower* tower = static_cast<Tower*>(m_host);
		return tower->GetCurrentHealth();
	}
	case(E_ENEMY) :
	{
		AbstractEnemy* abstractEnemy = static_cast<AbstractEnemy*>(m_host);
		return abstractEnemy->GetCurrentHealth();
	}
	case(E_LIFETREE) :
	{
		LifeTree* lifeTree = static_cast<LifeTree*>(m_host);
		return lifeTree->GetCurrentHealth();
	}
	case(E_PLAYER) :
	{
		Player* player = static_cast<Player*>(m_host);
		return player->GetCurrentHealth();
	}
	default:
	{
		return 0;
	}
	}
}