// LoggerLarge.h

#pragma once

#include "AbstractEnemy.h"

class LoggerLarge : public AbstractEnemy
{
public:
	LoggerLarge(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder);
	~LoggerLarge();

	SubType GetSubType();
	void Deactivate();
	void Reactivate(int x, int y);

private:
	void Initialize();
	void Attack();
	void AnimateMovement();
	void AnimateAttack();
	void ScanForCleaveTarget();

private:
	Entity* m_cleaveTarget;


};