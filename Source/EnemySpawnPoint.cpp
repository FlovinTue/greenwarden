//EnemySpawnPoint.cpp

#include "stdafx.h"
#include "EnemySpawnPoint.h"
#include "AttributeManager.h"
#include "TextureManager.hpp"
#include "LoggerLarge.h"
#include "LoggerSmall.h"
#include "Archer.h"
#include "FlameThrower.h"
#include "Pathfinding.h"

EnemySpawnPoint::EnemySpawnPoint(int x, int y, TextureManager* textureManager, Entity* mainTarget, std::vector<Entity*>* entitiyVector, Pathfinding* pathfinder)
{
	m_x = x;
	m_y = y;
	m_mainTarget = mainTarget;
	m_entities = entitiyVector;
	m_textureManager = textureManager;
	m_pathfinder = pathfinder;
	Initialize();
	PreGenerateUnits();
}
EnemySpawnPoint::~EnemySpawnPoint()
{

}
void EnemySpawnPoint::Update(float deltatime)
{
	if (m_active)
	{
		if (m_roundActive)
		{
			if (m_waveTimer.getElapsedTime().asSeconds() >= m_waveCooldown&&
				!(m_currentWave >= m_wavesPerRound)&&
				!(m_currentRound >=m_maxRounds))
			{
				SpawnWave();
				m_currentWave++;
				m_waveTimer.restart();
			}
			else if (m_currentRound >= m_maxRounds)
			{
				// finalround
				Deactivate();
				return;
			}
		}

		if (m_roundTimer.getElapsedTime().asSeconds() >= m_roundLength&&
			m_roundActive == true)
		{
			m_roundActive = false;
			m_roundCooldowntimer.restart();
			m_currentWave = 0;
		}

		if (m_roundCooldowntimer.getElapsedTime().asSeconds() >= m_roundCooldownValue&&
			m_roundActive == false &&
			m_currentRound != 0)
		{
			m_currentRound++;

			m_roundTimer.restart();
			m_roundActive = true;

			SpawnWave();
			m_currentWave++;
			m_waveTimer.restart();
		}
		if (m_currentRound == 0 &&
			m_startTimer.getElapsedTime().asSeconds() >= m_startWaitingPeriod)
		{
			m_currentRound++;

			m_roundTimer.restart();
			m_roundActive = true;

			SpawnWave();
			m_currentWave++;
			m_waveTimer.restart();
		}
	}
}
void EnemySpawnPoint::Activate()
{
	m_roundCooldowntimer.restart();
	m_roundActive = false;
	m_active = true;
}
void EnemySpawnPoint::Deactivate()
{
	m_active = false;
}
unsigned int EnemySpawnPoint::GetRoundTimer()
{
	return m_roundLength - m_roundTimer.getElapsedTime().asSeconds();
}
unsigned int EnemySpawnPoint::GetRoundCooldownTimer()
{
	return m_roundCooldownValue - m_roundCooldowntimer.getElapsedTime().asSeconds();
}
int EnemySpawnPoint::GetCurrentRound()
{
	return m_currentRound;
}
void EnemySpawnPoint::Initialize()
{
	m_loggerSmallEntryRound = AttributeManager::GetAttribute("SpawnPoint_loggersmall_EntryRound");
	m_loggerLargeEntryRound = AttributeManager::GetAttribute("SpawnPoint_loggerlarge_EntryRound");
	m_archerEntryRound = AttributeManager::GetAttribute("SpawnPoint_archer_EntryRound");
	m_flameThrowerEntryRound = AttributeManager::GetAttribute("SpawnPoint_flamethrower_EntryRound");

	m_loggerSmallRoundStartValue = AttributeManager::GetAttribute("SpawnPoint_loggersmall_RoundStartValue");
	m_loggerLargeRoundStartValue = AttributeManager::GetAttribute("SpawnPoint_loggerlarge_RoundStartValue");
	m_archerRoundStartValue = AttributeManager::GetAttribute("SpawnPoint_archer_RoundStartValue");
	m_flameThrowerRoundStartValue = AttributeManager::GetAttribute("SpawnPoint_flamethrower_RoundStartValue");

	m_loggerSmallRoundEndValue = AttributeManager::GetAttribute("SpawnPoint_loggersmall_RoundEndValue");
	m_loggerLargeRoundEndValue = AttributeManager::GetAttribute("SpawnPoint_loggerlarge_RoundEndValue");
	m_archerRoundEndValue = AttributeManager::GetAttribute("SpawnPoint_archer_RoundEndValue");
	m_flameThrowerRoundEndValue = AttributeManager::GetAttribute("SpawnPoint_flamethrower_RoundEndValue");
	
	m_wavesPerRound = AttributeManager::GetAttribute("SpawnPoint_WavesPerRound");
	m_roundCooldownValue = AttributeManager::GetAttribute("SpawnPoint_RoundCooldown");
	m_roundLength = AttributeManager::GetAttribute("SpawnPoint_RoundLength");
	m_maxRounds = AttributeManager::GetAttribute("SpawnPoint_MaxRounds");
	m_globalRoundUnitMultiplier = (static_cast<float>(AttributeManager::GetAttribute("SpawnPoint_GlobalUnitRoundPercentageMultiplier")) * 0.01f);

	m_loggerSmallTexture = m_textureManager->CreateTextureFromFile("assets/staticsprites/LoggerSmallIdleForward.png");
	m_loggerLargeTexture = m_textureManager->CreateTextureFromFile("assets/staticsprites/LoggerLargeIdleForward.png");
	m_archerTexture = m_textureManager->CreateTextureFromFile("assets/staticsprites/ArcherIdleForward.png");
	m_flameThrowerTexture = m_textureManager->CreateTextureFromFile("assets/staticsprites/FlameThrowerIdleForward.png");
	m_archerProjectileTexture = m_textureManager->CreateTextureFromFile("assets/staticsprites/ArcherProjectile.png");

	m_startWaitingPeriod = AttributeManager::GetAttribute("SpawnPoint_StartWaitingPeriod");
	m_waveCooldown = m_roundLength / m_wavesPerRound;
	m_currentWave = 0;
	m_spawnRadius = 250;
	m_currentRound = 0;
	m_roundActive = false;
	m_active = false;
}
void EnemySpawnPoint::PreGenerateUnits()
{
	for (int i = 0; i < 50; i++)
	{
		LoggerSmall* loggerSmall = new LoggerSmall(m_entities, m_loggerSmallTexture, m_mainTarget, 5000, 0, m_pathfinder);
		m_entities->push_back(loggerSmall);
	}
	for (int i = 0; i < 40; i++)
	{
		LoggerLarge* loggerLarge = new LoggerLarge(m_entities, m_loggerLargeTexture, m_mainTarget, 5000, 0, m_pathfinder);
		m_entities->push_back(loggerLarge);
	}
	for (int i = 0; i < 30; i++)
	{
		Archer* archer = new Archer(m_entities, m_archerTexture, m_archerProjectileTexture, m_mainTarget, 5000, 0, m_pathfinder);
		m_entities->push_back(archer);
	}
	for (int i = 0; i < 30; i++)
	{
		FlameThrower* flameThrower = new FlameThrower(m_entities, m_flameThrowerTexture, m_mainTarget, 5000, 0, m_pathfinder);
		m_entities->push_back(flameThrower);
	}
}
void EnemySpawnPoint::SpawnWave()
{
	int loggerSmallCount = 0;
	int loggerLargeCount = 0;
	int archerCount = 0;
	int flameThrowerCount = 0;

	if (m_loggerSmallEntryRound <= m_currentRound)
	{
		float multiplier = ((m_globalRoundUnitMultiplier * (static_cast<float>(m_currentRound-m_loggerSmallEntryRound))) + 1.0f);
		int waveSize = m_loggerSmallRoundStartValue + ((m_loggerSmallRoundEndValue - m_loggerSmallRoundStartValue) / (m_roundLength / (m_roundTimer.getElapsedTime().asSeconds() + 1)));
		loggerSmallCount = (waveSize) * multiplier;
	}
	if (m_loggerLargeEntryRound <= m_currentRound)
	{
		float multiplier = ((m_globalRoundUnitMultiplier * (static_cast<float>(m_currentRound - m_loggerLargeEntryRound))) + 1.0f);
		int waveSize = m_loggerLargeRoundStartValue + ((m_loggerLargeRoundEndValue - m_loggerLargeRoundStartValue) / (m_roundLength / (m_roundTimer.getElapsedTime().asSeconds() + 1)));
		loggerLargeCount = (waveSize) * multiplier;
	}
	if (m_archerEntryRound <= m_currentRound)
	{
		float multiplier = ((m_globalRoundUnitMultiplier * (static_cast<float>(m_currentRound - m_archerEntryRound))) + 1.0f);
		int waveSize = m_archerRoundStartValue + ((m_archerRoundEndValue - m_archerRoundStartValue) / (m_roundLength / (m_roundTimer.getElapsedTime().asSeconds() + 1)));
		archerCount = (waveSize) * multiplier;
	}
	if (m_flameThrowerEntryRound <= m_currentRound)
	{
		float multiplier = ((m_globalRoundUnitMultiplier * (static_cast<float>(m_currentRound - m_flameThrowerEntryRound))) + 1.0f);
		int waveSize = m_flameThrowerRoundStartValue + ((m_flameThrowerRoundEndValue - m_flameThrowerRoundStartValue) / (m_roundLength / (m_roundTimer.getElapsedTime().asSeconds() + 1)));
		flameThrowerCount = (waveSize) * multiplier;
	}
	SpawnLoggerSmall(loggerSmallCount);
	SpawnLoggerLarge(loggerLargeCount);
	SpawnArcher(archerCount);
	SpawnFlameThrower(flameThrowerCount);
}
void EnemySpawnPoint::SpawnLoggerSmall(int amount)
{
	if (amount < 1)
		return;

	int spawnedCount = 0;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (!m_entities->at(i)->IsActive() &&
			m_entities->at(i)->GetType()==E_ENEMY)
		{
			AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
			if (enemy->GetSubType() == E_LOGGERS)
			{
				LoggerSmall* loggerSmall = static_cast<LoggerSmall*>(enemy);
				sf::Vector2i spawnPos = GetRandomSpawnPos();
				loggerSmall->Reactivate(spawnPos.x, spawnPos.y);
				spawnedCount++;

				if (spawnedCount >= amount)
					return;
			}
		}
	}
}
void EnemySpawnPoint::SpawnLoggerLarge(int amount)
{
	if (amount < 1)
		return;

	int spawnedCount = 0;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (!m_entities->at(i)->IsActive() &&
			m_entities->at(i)->GetType() == E_ENEMY)
		{
			AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
			if (enemy->GetSubType() == E_LOGGERL)
			{
				LoggerLarge* loggerLarge = static_cast<LoggerLarge*>(enemy);
				sf::Vector2i spawnPos = GetRandomSpawnPos();
				loggerLarge->Reactivate(spawnPos.x, spawnPos.y);
				spawnedCount++;

				if (spawnedCount >= amount)
					return;
			}
		}
	}
}
void EnemySpawnPoint::SpawnArcher(int amount)
{
	if (amount < 1)
		return;

	int spawnedCount = 0;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (!m_entities->at(i)->IsActive() &&
			m_entities->at(i)->GetType() == E_ENEMY)
		{
			AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
			if (enemy->GetSubType() == E_ARCHER)
			{
				Archer* archer = static_cast<Archer*>(enemy);
				sf::Vector2i spawnPos = GetRandomSpawnPos();
				archer->Reactivate(spawnPos.x, spawnPos.y);
				spawnedCount++;

				if (spawnedCount >= amount)
					return;
			}
		}
	}
}
void EnemySpawnPoint::SpawnFlameThrower(int amount)
{
	if (amount < 1)
		return;

	int spawnedCount = 0;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (!m_entities->at(i)->IsActive() &&
			m_entities->at(i)->GetType() == E_ENEMY)
		{
			AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
			if (enemy->GetSubType() == E_FLAMETHROWER)
			{
				FlameThrower* flameThrower = static_cast<FlameThrower*>(enemy);
				sf::Vector2i spawnPos = GetRandomSpawnPos();
				flameThrower->Reactivate(spawnPos.x, spawnPos.y);
				spawnedCount++;

				if (spawnedCount >= amount)
					return;
			}
		}
	}
}
sf::Vector2i EnemySpawnPoint::GetRandomSpawnPos()
{
	int hypotenuse = rand() % m_spawnRadius; // Johannes stuff
	int angle = rand() % 360;
	float posX = cos(angle) * hypotenuse;
	float posY = sin(angle) * hypotenuse;
	
	sf::Vector2i randomPos{ static_cast<int>(posX)+m_x, static_cast<int>(posY)+m_y };

	return randomPos;
}