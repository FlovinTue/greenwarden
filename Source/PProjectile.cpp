// PProjectile.cpp

#include "stdafx.h"
#include "PProjectile.h"
#include "CollisionManager.hpp"
#include "AttributeManager.h"
#include "AbstractEnemy.h"

PProjectile::PProjectile(Entity* target, std::vector<Entity*>* entities, sf::Texture* texture, int x, int y)
{
	m_active = false;
	m_x = x -8;
	m_y = y -8;
	m_w = 16;
	m_h = 16;
	m_entities = entities;
	SetDirection(target);
	m_sprite.setTexture(*texture);
	m_speed = AttributeManager::GetAttribute("Projectile_global_speed");
	m_damage = AttributeManager::GetAttribute("Projectile_piercing_damage");;
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);
}
PProjectile::~PProjectile()
{
	if (m_collider)
		delete m_collider;
}
void PProjectile::Update(float deltatime)
{
	if (m_active == true)
	{
		Travel(deltatime);
		DamageTargets();

		if (m_x < 0 || m_y < 0 || m_y > 1080 || m_x > 1920)
		{
			Deactivate();
		}

		for (int i = 0; i < m_entities->size(); i++)
		{
			if (m_entities->at(i)->GetType() == E_FOREST)
			{
				int overlapX = 0;
				int overlapY = 0;
				if (CollisionManager::Check(m_collider, m_entities->at(i)->GetCollider(), overlapX, overlapY))
				{
					Deactivate();
					return;
				}
			}
		}
		m_collider->Refresh();
		m_sprite.setPosition(m_x, m_y);
	}
}
EType PProjectile::GetType()
{
	return E_PPROJECTILE;
}
float PProjectile::GetSpeed()
{
	return m_speed;
}
void PProjectile::SetSpeed(float speed)
{
	m_speed = speed;
}
int PProjectile::GetDamage()
{
	return m_damage;
}
void PProjectile::SetDamage(int damage)
{
	m_damage = damage;
}

void PProjectile::SetDirection(Entity* m_target)
{
	if (m_target)
	{
		float distX = (m_target->GetX() + (m_target->GetW() / 2)) - (GetX() + (GetW() / 2));
		float distY = (m_target->GetY() + (m_target->GetH() / 2)) - (GetY() + (GetH() / 2));
		float units = 1 / (abs(distX) + abs(distY));
		m_directionX = (distX * units);
		m_directionY = (distY * units);
	}
}
void PProjectile::Travel(float deltatime)
{
	m_x += m_directionX * m_speed * deltatime;
	m_y += m_directionY * m_speed * deltatime;
}
bool PProjectile::PreviouslyHit(Entity* target)
{
	for (int i = 0; i < m_pierceTracker.size(); i++)
	{
		if (m_pierceTracker[i] == target)
		{
			return true;
		}
	}
	return false;
}
void PProjectile::DamageTargets()
{
	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->IsActive() != false)
		{
			if (m_entities->at(i)->GetType() == E_ENEMY)
			{
				int overlapX = 0;
				int overlapY = 0;
				if (CollisionManager::Check(m_entities->at(i)->GetCollider(), m_collider, overlapX, overlapY))
				{
					if (PreviouslyHit(m_entities->at(i)) == false)
					{
						AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
						enemy->SubstractHealth(m_damage);
						m_pierceTracker.push_back(m_entities->at(i));
					}
				}
			}
		}
	}
}
void PProjectile::Deactivate()
{
	m_active = false;
	m_pierceTracker.clear();
	AnimationStore::Stop(this);
}
void PProjectile::Reactivate(Entity* target, int x, int y)
{
	if (m_active == false)
	{
		m_active = true;
		AnimationStore::Play(this, "towerprojectile6", true);
		m_x = x;
		m_y = y;
		m_collider->Refresh();
		SetDirection(target);
	}
}
