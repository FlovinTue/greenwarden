/// Sound.cpp
//
#include "stdafx.h"
#include "Sound.h"


// static member variables
std::map<const char*, sf::SoundBuffer> Sound::m_sounds;
std::map<const char*, sf::Sound> Sound::m_channels;


Sound::Sound()
{
}
Sound::Sound(const char* filename)
{
	loadFromFile(filename);
}
bool Sound::loadFromFile(const char* filename)
{
	m_filename = filename;

	auto itr = m_sounds.find(filename);
	if (itr == m_sounds.end())
	{
		if (!m_sounds[filename].loadFromFile(filename)) // (kan h�nda att on�diga element r�kar skapas)
			return false;

		m_channels[filename].setBuffer(m_sounds[filename]);
	}

	return true;
}

void Sound::play()
{
	m_channels[m_filename].play();
}
void Sound::loop()
{
	m_channels[m_filename].setLoop(true);
	m_channels[m_filename].play();
}
void Sound::pause()
{
	m_channels[m_filename].pause();
}
void Sound::stop()
{
	m_channels[m_filename].stop();
}