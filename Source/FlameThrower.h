// FlameThrower.h

#pragma once

#include "AbstractEnemy.h"

class FlameThrower : public AbstractEnemy
{
public:
	FlameThrower(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder);
	~FlameThrower();

	SubType GetSubType();
	void Deactivate();
	void Reactivate(int x, int y);

private:
	void AnimateMovement();
	void AnimateAttack();
	void Initialize();
	void Attack();

private:
	int m_effectDuration;
};