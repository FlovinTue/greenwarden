
#include "stdafx.h"
#include "TowerShop.h"
#include "Tower.h"
#include "HProjectile.h"
#include "PProjectile.h"
#include "TileMap.h"

TowerShop::TowerShop(TextureManager* textureManager, std::vector<Entity*>* entityVector)
{
	m_entities = entityVector;
	AllocateTextures(textureManager);

	PreGenerateTowers();
}
TowerShop::~TowerShop()
{

}
void TowerShop::CreateTower(int towerType, int x, int y, Tile* tile)
{
	sf::Texture* towerTexture;
	sf::Texture* projectileTexture;

	switch (towerType)
	{
	case(0) :
	{
		return;
	}
	case(1) :
	{
		towerTexture = m_tower1Texture;
		projectileTexture = m_projectile1Texture;
		break;
	}
	case(2) :
	{
		towerTexture = m_tower2Texture;
		projectileTexture = nullptr;
		break;
	}
	case(3) :
	{
		towerTexture = m_tower3Texture;
		projectileTexture = m_projectile2Texture;
		break;
	}
	case(4) :
	{
		towerTexture = m_tower4Texture;
		projectileTexture = m_projectile3Texture;
		break;
	}
	case (5) :
	{
		towerTexture = m_tower5Texture;
		projectileTexture = nullptr;
		break;
	}
	case(6) :
	{
		towerTexture = m_tower6Texture;
		projectileTexture = m_projectile4Texture;
		break;
	}
	case(7) :
	{
		towerTexture = m_tower7Texture;
		projectileTexture = nullptr;
		break;
	}
	case(8) :
	{
		towerTexture = m_tower8Texture;
		projectileTexture = m_projectile5Texture;
		break;
	}
	case(9) :
	{
		towerTexture = m_tower9Texture;
		projectileTexture = m_projectile6Texture;
		break;
	}
	default:
	{
		towerTexture = m_tower1Texture;
		projectileTexture = m_projectile1Texture;
		break;
	}
	}

	Tower* tower = nullptr;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->GetType() == E_TOWER)
		{
			if (m_entities->at(i)->IsActive() == false)
			{
				tower = static_cast<Tower*>(m_entities->at(i));
				tower->Reactivate(x, y, towerType, towerTexture, projectileTexture, tile);
				return;
			}
		}
	}
	tower = new Tower(x, y, m_entities, towerType, towerTexture, projectileTexture, tile);
	m_entities->push_back(tower);

}
void TowerShop::AllocateTextures(TextureManager* textureManager)
{
	m_tower1Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower1.png");
	m_tower2Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower2.png");
	m_tower3Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower3.png");
	m_tower4Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower4.png");
	m_tower5Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower5.png");
	m_tower6Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower6.png");
	m_tower7Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower7.png");
	m_tower8Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower8.png");
	m_tower9Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/Tower9.png");
	m_projectile1Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile1.png");
	m_projectile2Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile2.png");
	m_projectile3Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile3.png");
	m_projectile4Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile4.png");
	m_projectile5Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile5.png");
	m_projectile6Texture = textureManager->CreateTextureFromFile("assets/StaticSprites/TowerProjectile6.png");
}
void TowerShop::PreGenerateTowers()
{
	for (int i = 0; i < 30; i++)
	{
		m_entities->push_back(new Tower(-100, -100, m_entities, 1, m_tower1Texture, m_projectile1Texture, nullptr));
	}
}