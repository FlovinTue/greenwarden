/// Tilemap.cpp
//
#include "stdafx.h"
#include "Tilemap.h"



void Tilemap::create(unsigned width, unsigned height, unsigned tilewidth, unsigned tileheight)
{
	m_tile_vector.reserve(width * height);

	m_width = width;
	m_height = height;
	m_tilewidth = tilewidth;
	m_tileheight = tileheight;


	for (int index = 0; index < m_tile_vector.capacity(); index++)
	{
		int row = index / m_width;
		int col = (index + m_width) % m_width;
		sf::Vector2f position(m_tilewidth * col, m_tileheight * row);

		// center of tile
		position.x += m_tilewidth / 2;
		position.y += m_tileheight / 2;

		m_tile_vector.push_back(Tile{ 0, index, position, m_tilewidth, m_tileheight });
	}
}



Tile& Tilemap::getTile(unsigned row, unsigned column)
{
	short index = column * m_width + row;

	return m_tile_vector[index];
}

Tile& Tilemap::getTile(sf::Vector2f point)
{
	int row = point.x / m_tilewidth;
	int col = point.y / m_tileheight;

	if (row >= m_width) row = m_width - 1;
	if (col >= m_height) col = m_height - 1;

	short index = col * m_width + row;


	return m_tile_vector[index];
}

Tile& Tilemap::getTile(short index)
{
	return m_tile_vector[index];
}

Tile& Tilemap::operator[](short index)
{
	return m_tile_vector[index];
}