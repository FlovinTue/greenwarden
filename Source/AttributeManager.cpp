// AttributeManager.cpp

#include "stdafx.h"
#include "AttributeManager.h"
#include <fstream>
#include <iostream>

std::string AttributeManager::m_configFile;
std::map<std::string, int> AttributeManager::m_storage;

AttributeManager::AttributeManager()
{
}
AttributeManager::~AttributeManager()
{
}

int AttributeManager::GetAttribute(const std::string &attribute)
{
	auto it = m_storage.begin();

	while (it != m_storage.end())
	{
		if (it->first == attribute)
			return it->second;
		it++;
	}

	std::pair<std::string, int> pair;
	pair.first = attribute;
	pair.second = 0;
	m_storage.insert(pair);

	std::ofstream attributeFile(m_configFile, std::ios::app);

	attributeFile << pair.first << ' ' << pair.second << std::endl;
	attributeFile.close();

	return pair.second;

}
void AttributeManager::Initialize(const std::string &configFile)
{
	m_configFile = configFile;

	std::ifstream attributeFile;
	attributeFile.open(configFile);


	if (!attributeFile)
	{ 
		attributeFile.close();
		GenerateConfig(configFile);
		attributeFile.open(configFile);
	}

	while (!attributeFile.eof())
	{
		std::pair < std::string, int > attribute;

		attributeFile >> attribute.first;
		attributeFile >> attribute.second;

		m_storage.insert(attribute);
	}

	attributeFile.close();
}
void AttributeManager::GenerateConfig(const std::string &configFile)
{
	std::ofstream config;
	config.open(configFile);

	config << "Tower_shooter_HP" << ' ' << '10' << std::endl;
	config << "Tower_shooter_AS" << ' ' << '500' << std::endl;
	config << "Tower_shooter_Range" << ' ' << '500'<< std::endl;
	config << "Tower_defense_HP" << ' ' << '1' << std::endl;
	config << "Tower_magic_HP" << ' ' << '1' << std::endl;
	config << "Tower_magic_AS" << ' ' << '500' << std::endl;
	config << "Tower_magic_Range" << ' ' << '500' << std::endl;
	config << "Tower_rangerange_HP" << ' ' << '1' << std::endl;
	config << "Tower_rangerange_AS" << ' ' << '500' << std::endl;
	config << "Tower_rangerange_Range" << ' ' << '500' << std::endl;
	config << "Tower_rangedefense_HP" << ' ' << '1' << std::endl;
	config << "Tower_rangedefense_damage" << ' ' << '4' << std::endl;
	config << "Tower_rangemagic_HP" << ' ' << '1' << std::endl;
	config << "Tower_rangemagic_AS" << ' ' << '500' << std::endl;
	config << "Tower_rangemagic_Range" << ' ' << '500' << std::endl;
	config << "Tower_defensedefense_HP" << ' ' << '1' << std::endl;
	config << "Tower_defensemagic_HP" << ' ' << '1' << std::endl;
	config << "Tower_defensemagic_AS" << ' ' << '500' << std::endl;
	config << "Tower_defensemagic_Range" << ' ' << '500' << std::endl;
	config << "Tower_magicmagic_HP" << ' ' << '1' << std::endl;
	config << "Tower_magicmagic_AS" << ' ' << '500' << std::endl;
	config << "Tower_magicmagic_Range" << ' ' << '500' << std::endl;

	config << "Projectile_global_speed" << ' ' << '2' << std::endl;
	config << "Projectile_piercing_damage" << ' ' << '1' << std::endl;
	config << "Projectile_shooter_damage" << ' ' << '1' << std::endl;
	config << "Projectile_magic_damage" << ' ' << '1' << std::endl;
	config << "Projectile_rangerange_damage" << ' ' << '1' << std::endl;
	config << "Projectile_rangemagic_damage" << ' ' << '1' << std::endl;
	config << "Projectile_rangemagic_duration" << ' ' << '10' << std::endl;
	config << "Projectile_defensemagic_duration" << ' ' << '10' << std::endl;
	config << "Projectile_defensemagic_percentage" << ' ' << '50' << std::endl;
	config << "Projectile_archer_damage" << ' ' << '8' << std::endl;
	config << "Projectile_archer_duration" << ' ' << '10' << std::endl;

	config << "Enemy_loggersmall_HP" << ' ' << '12' << std::endl;
	config << "Enemy_loggersmall_damage" << ' ' << '2' << std::endl;
	config << "Enemy_loggersmall_AS" << ' ' << '1000' << std::endl;
	config << "Enemy_loggersmall_movement" << ' ' << '500' << std::endl;
	config << "Enemy_loggersmall_range" << ' ' << '32' << std::endl;
	config << "Enemy_loggersmall_detectRange" << ' ' << '250' << std::endl;
	config << "Enemy_loggerlarge_HP" << ' ' << '18' << std::endl;
	config << "Enemy_loggerlarge_damage" << ' ' << '3' << std::endl;
	config << "Enemy_loggerlarge_AS" << ' ' << '1000' << std::endl;
	config << "Enemy_loggerlarge_movement" << ' ' << '23' << std::endl;
	config << "Enemy_loggerlarge_range" << ' ' << '1' << std::endl;
	config << "Enemy_loggerlarge_detectRange" << ' ' << '250' << std::endl;
	config << "Enemy_archer_HP" << ' ' << '9' << std::endl;
	config << "Enemy_archer_AS" << ' ' << '3000' << std::endl;
	config << "Enemy_archer_movement" << ' ' << '64' << std::endl;
	config << "Enemy_archer_range" << ' ' << '96' << std::endl;
	config << "Enemy_archer_detectRange" << ' ' << '500' << std::endl;
	config << "Enemy_flamethrower_HP" << ' ' << '12' << std::endl;
	config << "Enemy_flamethrower_damage" << ' ' << '8' << std::endl;
	config << "Enemy_flamethrower_AS" << ' ' << '3000' << std::endl;
	config << "Enemy_flamethrower_movement" << ' ' << '32' << std::endl;
	config << "Enemy_flamethrower_range" << ' ' << '32' << std::endl;
	config << "Enemy_flamethrower_detectRange" << ' ' << '250' << std::endl;
	config << "Enemy_flamethrower_effectDuration" << ' ' << '4' << std::endl;
	config << "Enemy_suicidesquad_HP" << ' ' << '12' << std::endl;
	config << "Enemy_suicidesquad_damage" << ' ' << '30' << std::endl;
	config << "Enemy_suicidesquad_movement" << ' ' << '96' << std::endl;
	config << "Enemy_suicidesquad_range" << ' ' << '96' << std::endl;
	config << "Enemy_suicidesquad_detectRange" << ' ' << '250' << std::endl;

	config << "LifeTree_MaxHealth" << ' ' << "200" << std::endl;

	config << "SpawnPoint_loggersmall_EntryRound" << ' ' << '1' << std::endl;
	config << "SpawnPoint_loggerlarge_EntryRound" << ' ' << '2' << std::endl;
	config << "SpawnPoint_archer_EntryRound" << ' ' << '3' << std::endl;
	config << "SpawnPoint_flamethrower_EntryRound" << ' ' << '4' << std::endl;
	config << "SpawnPoint_loggersmall_RoundStartValue" << ' ' << '2' << std::endl;
	config << "SpawnPoint_loggerlarge_RoundStartValue" << ' ' << '2' << std::endl;
	config << "SpawnPoint_archer_RoundStartValue" << ' ' << '2' << std::endl;
	config << "SpawnPoint_flamethrower_RoundStartValue" << ' ' << '2' << std::endl;
	config << "SpawnPoint_loggersmall_RoundEndValue" << ' ' << '5' << std::endl;
	config << "SpawnPoint_loggerlarge_RoundEndValue" << ' ' << '5' << std::endl;
	config << "SpawnPoint_archer_RoundEndValue" << ' ' << '5' << std::endl;
	config << "SpawnPoint_flamethrower_RoundEndValue" << ' ' << '5' << std::endl;
	config << "SpawnPoint_WavesPerRound" << ' ' << '6' << std::endl;
	config << "SpawnPoint_RoundCooldown" << ' ' << '10' << std::endl;
	config << "SpawnPoint_RoundLength" << ' ' << '60' << std::endl;
	config << "SpawnPoint_MaxRounds" << ' ' << '10' << std::endl;
	config << "SpawnPoint_GlobalUnitRoundPercentageMultiplier" << ' ' << '10' << std::endl;
	config << "SpawnPoint_StartWaitingPeriod" << ' ' << '10' << std::endl;


	config.close();
}
