
#include "stdafx.h"
#include "PowerUpHud.h"
#include "AnimationStore.h"
#include "Entity.h"

PowerUpHud::PowerUpHud(float x, float y, int type)
{
	m_x = x;
	m_y = y;

	m_type = type;
	switch (m_type)
	{
	case 1:
		m_texture.loadFromFile("assets/StaticSprites/PowerupBackground3.png");
		break;
	case 2:
		m_texture.loadFromFile("assets/StaticSprites/PowerupBackground1.png");
		break;
	case 3:
		m_texture.loadFromFile("assets/StaticSprites/PowerupBackground2.png");
		break;
	}
	//m_texture.loadFromFile("assets/StaticSprites/RED_timer00.png");
	//m_texture = m_texture_manager->CreateTextureFromFile("assets/StaticSprites/WardenIdleForward.png");
	m_sprite.setTexture(m_texture);
	m_sprite.setPosition(350, 50);
	m_active = true;
}
PowerUpHud::~PowerUpHud()
{

}

void PowerUpHud::Update(float deltatime)
{

}
EType PowerUpHud::GetType()
{
	return E_HUD;
}
void PowerUpHud::PlayAnimation()
{
	switch (m_type)
	{
	case 1:
		AnimationStore::Play(this, "powerup_red", false);
		break;
	case 2 :
		AnimationStore::Play(this, "powerup_blue", false);
		break;
	case 3:
		AnimationStore::Play(this, "powerup_green", false);
		break;
	}
	
}
