/// Seed.h
//
#pragma once
#include "Entity.h"


enum SEED_TYPE
{
	SEED_TYPE_1,
	SEED_TYPE_2,
	SEED_TYPE_3
};


class Seed : public Entity
{
public:
	Seed();
	~Seed();

	void Update(float deltatime);

	void setPosition(float x, float y);
	void setActive(bool active);
	void setSeedType(SEED_TYPE color);

	EType GetType();
	SEED_TYPE getSeedType();

	static void setTextures(sf::Texture* type1, sf::Texture* type2, sf::Texture* type3);

private:
	SEED_TYPE m_seed_type;
	float m_time;

	static sf::Texture* s_textures[3];
};