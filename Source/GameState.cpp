// GameState.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "GameState.h"
#include "AudioManager.hpp"

#include "TowerShop.h"
#include "AttributeManager.h"
#include "AnimationStore.h"
#include "Tilemap.h"
#include "EnemySpawnPoint.h"
#include "Pathfinding.h"
#include "LifeTree.h"
#include "Player.h"
#include "Tree.h"
#include "DefeatState.h"
#include "Water.h"
#include "SeedCombiner.h"
#include "Tower.h"

#define MAP_FILENAME "assets/Maps/themapcorridor.txt"


GameState::GameState()
{
	srand(static_cast<unsigned int>(time(0)));

	m_texture_manager = ServiceLocator<TextureManager>::GetService();
	m_draw_manager = ServiceLocator<DrawManager>::GetService();
	m_audio_manager = ServiceLocator<AudioManager>::GetService();

	m_animationStore = new AnimationStore(m_texture_manager);
	AttributeManager::Initialize("assets/config.txt");


	m_playerhealthbaroverlaytexture.loadFromFile("assets/StaticSprites/PlayerHealthBarOutline.png");
	m_playerhealthbaroverlay.setTexture(m_playerhealthbaroverlaytexture);
	m_playerhealthbaroverlay.setPosition(110, 50);

	m_lifetreehealthbaroverlaytexture.loadFromFile("assets/StaticSprites/LifeTreeHealthbarOutline.png");
	m_liftreehealthbaroverlay.setTexture(m_lifetreehealthbaroverlaytexture);
	m_liftreehealthbaroverlay.setPosition(-10, 120);

	m_seedcounterbackgroundtexture.loadFromFile("assets/StaticSprites/SeedCounterBG.png");
	m_seedcounterbackground.setTexture(m_seedcounterbackgroundtexture);
	m_seedcounterbackground.setPosition(20, 950);

	m_seedcountertexture.loadFromFile("assets/StaticSprites/SeedCounterFG.png");
	m_seedcounter.setTexture(m_seedcountertexture);
	m_seedcounter.setPosition(20, 900);
	// Loading map
	InitializeMap(MAP_FILENAME);

	m_font.loadFromFile("assets/darling.ttf");


	m_seedcount1 = sf::Text("0", m_font);
	m_seedcount1.setCharacterSize(30);
	m_seedcount1.setColor(sf::Color::White);
	m_seedcount1.setPosition(40, 970);

	m_seedcount2 = sf::Text("0", m_font);
	m_seedcount2.setCharacterSize(30);
	m_seedcount2.setColor(sf::Color::White);
	m_seedcount2.setPosition(160, 970);

	m_seedcount3 = sf::Text("0", m_font);
	m_seedcount3.setCharacterSize(30);
	m_seedcount3.setColor(sf::Color::White);
	m_seedcount3.setPosition(300, 970);

	m_seedtipsRG = sf::Text("Shooter + Defense", m_font);
	m_seedtipsRG.setCharacterSize(30);
	m_seedtipsRG.setColor(sf::Color::White);
	m_seedtipsRG.setPosition(50, 870);

	m_seedtipsRR = sf::Text("DOUBLE SHOOTER", m_font);
	m_seedtipsRR.setCharacterSize(30);
	m_seedtipsRR.setColor(sf::Color::White);
	m_seedtipsRR.setPosition(50, 870);

	m_seedtipsGG = sf::Text("DOUBLE DEFENSE", m_font);
	m_seedtipsGG.setCharacterSize(30);
	m_seedtipsGG.setColor(sf::Color::White);
	m_seedtipsGG.setPosition(50, 870);

	m_seedtipsBB = sf::Text("DOUBLE MAGIC", m_font);
	m_seedtipsBB.setCharacterSize(30);
	m_seedtipsBB.setColor(sf::Color::White);
	m_seedtipsBB.setPosition(50, 870);

	m_seedtipsB = sf::Text("MAGIC", m_font);
	m_seedtipsB.setCharacterSize(30);
	m_seedtipsB.setColor(sf::Color::White);
	m_seedtipsB.setPosition(50, 870);

	m_seedtipsR = sf::Text("SHOOTER", m_font);
	m_seedtipsR.setCharacterSize(30);
	m_seedtipsR.setColor(sf::Color::White);
	m_seedtipsR.setPosition(50, 870);

	m_seedtipsG = sf::Text("DEFENSE", m_font);
	m_seedtipsG.setCharacterSize(30);
	m_seedtipsG.setColor(sf::Color::White);
	m_seedtipsG.setPosition(50, 870);

	m_seedtipsGB = sf::Text("Defense + Magic", m_font);
	m_seedtipsGB.setCharacterSize(30);
	m_seedtipsGB.setColor(sf::Color::White);
	m_seedtipsGB.setPosition(50, 870);

	m_seedtipsRB = sf::Text("Shooter + Magic", m_font);
	m_seedtipsRB.setCharacterSize(30);
	m_seedtipsRB.setColor(sf::Color::White);
	m_seedtipsRB.setPosition(50, 870);


	m_time_text = sf::Text("0", m_font);
	m_time_text.setCharacterSize(40);
	m_time_text.setColor(sf::Color::White);
	m_time_text.setPosition(1800, 970);
}

GameState::~GameState()
{
	for (int i = 0; i < m_spawnpoints.size(); i++)
	{
		if (m_spawnpoints[i])
		{
			delete m_spawnpoints[i];
		}
	}
	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i])
		{
			delete m_entities[i];
		}
	}
	m_entities.clear();

	if (m_animationStore)
		delete m_animationStore;
	if (m_towerShop)
		delete m_towerShop;
	if (m_tilemap)
		delete m_tilemap;
	if (m_pathfinder)
		delete m_pathfinder;

}

bool GameState::Enter()
{
	return true;
}

void GameState::Exit()
{
}

bool GameState::Update(float deltatime)
{
	if (m_lifetree->GetCurrentHealth() <= 0)
	{
		return false;
	}
	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i]->IsActive() == true)
		{
			m_entities[i]->Update(deltatime);
		}
	}
	for (int i = 0; i < m_spawnpoints.size(); i++)
	{
		m_spawnpoints[i]->Update(deltatime);
	}
	m_animationStore->Update(deltatime);

	return true;
}

void GameState::Draw()
{
	// background
	m_draw_manager->Draw(m_background, sf::RenderStates::Default);

	SetDrawOrder();

	for (int i = 0; i < 5000; i++)
	{
		if (m_drawOrder[i] == -10000)
			break;

		if (m_entities[m_drawOrder[i]]->GetType() == E_HUD)	// Manual draw below
			continue;

		if (m_entities[m_drawOrder[i]]->IsActive() == true)
		{
			m_draw_manager->Draw(*m_entities[m_drawOrder[i]]->GetSprite(), sf::RenderStates::Default);
		}
	}
	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i]->GetType() == E_HUD)
		{
			if (m_entities[i]->IsActive() == true)
			{
				m_draw_manager->Draw(*m_entities[i]->GetSprite(), sf::RenderStates::Default);
			}
		}
	}
	m_draw_manager->Draw(m_playerhealthbaroverlay, sf::RenderStates::Default);


	std::string s4 = std::to_string(m_player->GetTime());
	m_time_text.setString(s4);
	std::string s1 = std::to_string(m_player->GetSeed1());
	m_seedcount1.setString(s1);
	std::string s2 = std::to_string(m_player->GetSeed2());
	m_seedcount2.setString(s2);
	std::string s3 = std::to_string(m_player->GetSeed3());
	m_seedcount3.setString(s3);
	m_draw_manager->Draw(m_seedcounter, sf::RenderStates::Default);
	m_draw_manager->Draw(m_seedcount1, sf::RenderStates::Default);
	m_draw_manager->Draw(m_seedcount2, sf::RenderStates::Default);
	m_draw_manager->Draw(m_seedcount3, sf::RenderStates::Default);
	m_draw_manager->Draw(m_time_text, sf::RenderStates::Default);
	m_draw_manager->Draw(m_liftreehealthbaroverlay, sf::RenderStates::Default);

	switch (SeedCombiner::CombineCheck(m_player, m_player->Getm_seed1_active(), m_player->Getm_seed2_active(), m_player->Getm_seed3_active(), m_player->Getm_seed1_2_active(), m_player->Getm_seed2_2_active(), m_player->Getm_seed3_2_active()))
	{
	case 1:
		m_draw_manager->Draw(m_seedtipsR, sf::RenderStates::Default);
		break;
	case 2:
		m_draw_manager->Draw(m_seedtipsG, sf::RenderStates::Default);
		break;
	case 3:
		m_draw_manager->Draw(m_seedtipsB, sf::RenderStates::Default);
		break;
	case 4:
		m_draw_manager->Draw(m_seedtipsRR, sf::RenderStates::Default);
		break;
	case 5:
		m_draw_manager->Draw(m_seedtipsRG, sf::RenderStates::Default);
		break;
	case 6:
		m_draw_manager->Draw(m_seedtipsRB, sf::RenderStates::Default);
		break;
	case 7:
		m_draw_manager->Draw(m_seedtipsGG, sf::RenderStates::Default);
		break;
	case 8:
		m_draw_manager->Draw(m_seedtipsGB, sf::RenderStates::Default);
		break;
	case 9:
		m_draw_manager->Draw(m_seedtipsBB, sf::RenderStates::Default);
		break;

	}
}

std::string GameState::GetNextState()
{
	return "DefeatState";
}



void GameState::InitializeMap(const char* filename)
{
	m_tilemap = new Tilemap();
	m_tilemap->create(60, 37, 32, 32);
	m_pathfinder = new Pathfinding();
	m_pathfinder->setTilemap(m_tilemap);

	// Background texture
	m_background.setTexture(*m_texture_manager->CreateTextureFromFile("assets/StaticSprites/bgv6.jpg"));

	// Tree textures
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_1.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_2.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_3.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_4.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_5.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_6.png"));
	Tree::addTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/Tree_7.png"));

	// Water texture
	//Water::setTexture(m_texture_manager->CreateTextureFromFile("assets/StaticSprites/WaterSheet.png"));

	// Seed textures
	Seed::setTextures
		(
		m_texture_manager->CreateTextureFromFile("assets/staticsprites/Seed_Attack.png"),
		m_texture_manager->CreateTextureFromFile("assets/staticsprites/Seed_Defense.png"),
		m_texture_manager->CreateTextureFromFile("assets/staticsprites/Seed_Magic.png")
		);

	m_lifetree = new LifeTree(&m_entities, m_texture_manager->CreateTextureFromFile("assets/staticsprites/LifeTree.png"));
	m_entities.push_back(m_lifetree);

	m_towerShop = new TowerShop(m_texture_manager, &m_entities);



	std::ifstream file;
	file.open(filename);
	if (!file.is_open());
	else
	{
		char in_stream;
		short tile_index = 0;

		while (!file.eof())
		{
			file.get(in_stream);
			switch (in_stream)
			{
			case '1':// [tree]
				m_entities.push_back(new Tree((*m_tilemap)[tile_index].position));
				m_tilemap->getTile(tile_index).state = 1;
				tile_index++;
				break;

			case '2':// [water]
				//m_entities.push_back(new Water(m_tilemap->getTile(tile_index).position));
				tile_index++;
				break;

			case '3':// [ground water]
				// n�mting----...
				tile_index++;
				break;

			case '4':// [life tree]
				m_lifetree->setPosition((*m_tilemap)[tile_index].position);
				tile_index++;
				break;

			case '5':// [warden] (player)
				m_player = new Player(m_tilemap, &m_entities, m_towerShop, (*m_tilemap)[tile_index].position.x, (*m_tilemap)[tile_index].position.y);
				m_entities.push_back(m_player);
				tile_index++;
				break;

			case '6':// [enemy spawnpoint]
				m_enemySpawnPoint = new  EnemySpawnPoint((*m_tilemap)[tile_index].position.x, (*m_tilemap)[tile_index].position.y, m_texture_manager, m_lifetree, &m_entities, m_pathfinder);
				m_enemySpawnPoint->Activate();
				m_spawnpoints.push_back(m_enemySpawnPoint);
				tile_index++;
				break;

			case '0':// [empty tile]
				tile_index++;
				break;
			}
		}
	}
}
void GameState::SetDrawOrder()
{
	short unordered[5000];
	for (int i = 0; i < m_entities.size(); i++)
	{
		unordered[i] = m_entities[i]->GetY();
	}

	if (m_entities.size() < 5000)
	{
		for (int i = 0; i < 5000 - m_entities.size(); i++)
		{
			unordered[m_entities.size() + i] = -10000;
		}
	}

	short yCheck = -96;
	short vectorPosCheck = 0;
	short entered = 0;
	while (yCheck != 1080)
	{
		if (unordered[vectorPosCheck] == yCheck)
		{
			m_drawOrder[entered] = vectorPosCheck;
			entered++;
		}
		if (vectorPosCheck == 4999 ||
			unordered[vectorPosCheck] == -10000)
		{
			vectorPosCheck = 0;
			yCheck++;
			continue;
		}
		vectorPosCheck++;
	}

	for (int i = 0; i < 5000 - entered; i++)
	{
		m_drawOrder[i + entered] = -10000;
	}
}