// AnimationStore.cpp

#include "stdafx.h"
#include "AnimationStore.h"

std::vector<Animation> AnimationStore::m_animationStore;
std::vector<Animation> AnimationStore::m_activeAnimations;

AnimationStore::AnimationStore(TextureManager* textureManager)
{
	m_texture_manager = textureManager;
	Initialize();
}
AnimationStore::~AnimationStore()
{
	m_activeAnimations.clear();
}

void AnimationStore::Update(float deltatime)
{

	for (int i = 0; i < m_activeAnimations.size(); i++)
	{
		

			m_activeAnimations[i].m_timer += deltatime;

			if (m_activeAnimations[i].m_timer > m_activeAnimations[i].m_frameDuration)
			{
				
					if (m_activeAnimations[i].m_currentFrame == m_activeAnimations[i].m_spriteCords.size())
					{
						if (m_activeAnimations[i].m_looping == false)
						{
							Stop(m_activeAnimations[i].m_host);
							continue;
						}
						else if (m_activeAnimations[i].m_looping == true)
							m_activeAnimations[i].m_currentFrame = 0;
					}
					m_activeAnimations[i].m_host->GetSprite()->setTextureRect(m_activeAnimations[i].m_spriteCords[m_activeAnimations[i].m_currentFrame]);
					m_activeAnimations[i].m_currentFrame++;
					m_activeAnimations[i].m_timer = 0.0f;
				
			

		}
	}
}
void AnimationStore::Play(Entity* host, std::string animationName, bool looping)
{
	for (int i = 0; i < m_activeAnimations.size(); i++)
	{
		if (m_activeAnimations[i].m_host == host && m_activeAnimations[i].m_animationName == animationName)
			return;

		else if (m_activeAnimations[i].m_host == host)
		{
			m_activeAnimations.erase(m_activeAnimations.begin() + i);
			break;
		}
	}

	Animation animation;

	for (int i = 0; i < m_animationStore.size(); i++)
	{
		if (m_animationStore[i].m_animationName == animationName)
		{
			animation = m_animationStore[i];
			break;
		}
	}
	animation.m_host = host;
	animation.m_looping = looping;
	host->GetSprite()->setTexture(*animation.m_spriteSheet);
	host->GetSprite()->setTextureRect(animation.m_spriteCords[animation.m_currentFrame]);
	animation.m_currentFrame++;
	m_activeAnimations.push_back(animation);
}
void AnimationStore::Stop(Entity* host)
{
	for (int i = 0; i < m_activeAnimations.size(); i++)
	{
		if (m_activeAnimations[i].m_host == host)
		{
			host->GetSprite()->setTexture(*m_activeAnimations[i].m_idleTexture);

			sf::Vector2u idleTextureSize{ m_activeAnimations[i].m_idleTexture->getSize() };
			sf::IntRect idleTextureRect{ 0, 0, 
				static_cast<int>(idleTextureSize.x), 
				static_cast<int>(idleTextureSize.y)};

			host->GetSprite()->setTextureRect(idleTextureRect);
			m_activeAnimations.erase(m_activeAnimations.begin() + i);

			return;
		}
	}
}
void AnimationStore::AddAnimation(std::string animationName, std::string defFilePath, std::string spriteSheet, float totalDuration, std::string idleSpritePath)
{
	Animation animation;

	animation.m_animationName = animationName;
	animation.m_spriteSheet = m_texture_manager->CreateTextureFromFile(spriteSheet);
	animation.m_host = nullptr;
	animation.m_currentFrame = 0;
	animation.m_looping = false;
	animation.m_timer = 0.0f;
	animation.m_idleTexture = m_texture_manager->CreateTextureFromFile(idleSpritePath);

	std::ifstream defFile;
	defFile.open(defFilePath);


	if (!defFile.is_open())
		return;

	while (!defFile.eof())
	{

		char readStart;

		defFile >> readStart;

		if (readStart == '=')
		{
			sf::IntRect cords;

			defFile >> cords.left;
			defFile >> cords.top;
			defFile >> cords.width;
			defFile >> cords.height;
			animation.m_spriteCords.push_back(cords);
		}
	}

	if (defFile.eof())
		defFile.close();

	int frameCount = animation.m_spriteCords.size();
	float animationDuration = totalDuration;
	animation.m_frameDuration = animationDuration / frameCount;

	m_animationStore.push_back(animation);
}
std::string AnimationStore::GetActiveAnimation(Entity* host)
{
	std::string nullStr = "";

	if (m_activeAnimations.size() == 0)
		return nullStr;

	for (int i = 0; i < m_activeAnimations.size(); i++)
	{
		if (m_activeAnimations[i].m_host == host)
			return m_activeAnimations[i].m_animationName;
	}

	return nullStr;
}
void AnimationStore::Initialize()
{
	AddAnimation("wardenwalkleft", "assets/animations/Warden/WardenWalkLeftSheet.txt", "assets/animations/Warden/WardenWalkLeftSheet.png", 0.25f, "assets/StaticSprites/WardenIdleLeft.png");
	AddAnimation("wardenwalkright", "assets/animations/Warden/WardenWalkRightSheet.txt", "assets/animations/Warden/WardenWalkRightSheet.png", 0.25f, "assets/StaticSprites/WardenIdleRight.png");
	AddAnimation("wardenwalkforward", "assets/animations/Warden/WardenWalkForwardSheet.txt", "assets/animations/Warden/WardenWalkForwardSheet.png", 0.25f, "assets/StaticSprites/WardenIdleForward.png");
	AddAnimation("wardenwalkback", "assets/animations/Warden/WardenWalkBackSheet.txt", "assets/animations/Warden/WardenWalkBackSheet.png", 0.25f, "assets/StaticSprites/WardenIdleBackward.png");
	AddAnimation("wardenattackforward", "assets/animations/Warden/WardenAttackSheet.txt", "assets/animations/Warden/WardenAttackSheet.png", 0.2f, "assets/StaticSprites/WardenIdleForward.png");
	AddAnimation("wardenattackbackward", "assets/animations/Warden/WardenAttackBackward.txt", "assets/animations/Warden/WardenAttackBackward.png", 0.2f, "assets/StaticSprites/WardenIdleBackward.png");
	AddAnimation("wardenattackleft", "assets/animations/Warden/WardenAttackLeft.txt", "assets/animations/Warden/WardenAttackLeft.png", 0.2f, "assets/StaticSprites/WardenIdleLeft.png");
	AddAnimation("wardenattackright", "assets/animations/Warden/WardenAttackRight.txt", "assets/animations/Warden/WardenAttackRight.png", 0.2f, "assets/StaticSprites/WardenIdleRight.png");
	
	AddAnimation("loggersmallwalkleft", "assets/animations/LoggerSmall/LoggerSmallWalkLeft.txt", "assets/animations/LoggerSmall/LoggerSmallWalkLeft.png", 0.4f, "assets/StaticSprites/LoggerSmallIdleLeft.png");
	AddAnimation("loggersmallwalkright", "assets/animations/LoggerSmall/LoggerSmallWalkRight.txt", "assets/animations/LoggerSmall/LoggerSmallWalkRight.png", 0.4f, "assets/StaticSprites/LoggerSmallIdleRight.png");
	AddAnimation("loggersmallwalkforward", "assets/animations/LoggerSmall/LoggerSmallWalkForward.txt", "assets/animations/LoggerSmall/LoggerSmallWalkForward.png", 0.4f, "assets/StaticSprites/LoggerSmallIdleForward.png");
	AddAnimation("loggersmallwalkbackward", "assets/animations/LoggerSmall/LoggerSmallWalkBackward.txt", "assets/animations/LoggerSmall/LoggerSmallWalkBackward.png", 0.4f, "assets/StaticSprites/LoggerSmallIdleBackward.png");
	AddAnimation("loggersmallattackright", "assets/animations/LoggerSmall/LoggerSmallAttackRight.txt", "assets/animations/LoggerSmall/LoggerSmallAttackRight.png", 0.15f, "assets/StaticSprites/LoggerSmallIdleRight.png");
	AddAnimation("loggersmallattackleft", "assets/animations/LoggerSmall/LoggerSmallAttackLeft.txt", "assets/animations/LoggerSmall/LoggerSmallAttackLeft.png", 0.15f, "assets/StaticSprites/LoggerSmallIdleLeft.png");
	AddAnimation("loggersmallattackforward", "assets/animations/LoggerSmall/LoggerSmallAttackForward.txt", "assets/animations/LoggerSmall/LoggerSmallAttackForward.png", 0.15f, "assets/StaticSprites/LoggerSmallIdleForward.png");
	AddAnimation("loggersmallattackbackward", "assets/animations/LoggerSmall/LoggerSmallAttackBackward.txt", "assets/animations/LoggerSmall/LoggerSmallAttackBackward.png", 0.15f, "assets/StaticSprites/LoggerSmallIdleBackward.png");
	
	AddAnimation("loggerlargewalkforward", "assets/animations/LoggerLarge/LoggerLargeWalkForward.txt", "assets/animations/LoggerLarge/LoggerLargeWalkForward.png", 1.0f, "assets/StaticSprites/LoggerLargeIdleForward.png");
	AddAnimation("loggerlargewalkleft", "assets/animations/LoggerLarge/LoggerLargeWalkLeft.txt", "assets/animations/LoggerLarge/LoggerLargeWalkLeft.png", 1.0f, "assets/StaticSprites/LoggerLargeIdleLeft.png");
	AddAnimation("loggerlargewalkright", "assets/animations/LoggerLarge/LoggerLargeWalkRight.txt", "assets/animations/LoggerLarge/LoggerLargeWalkRight.png", 1.0f, "assets/StaticSprites/LoggerLargeIdleRight.png");
	AddAnimation("loggerlargewalkbackward", "assets/animations/LoggerLarge/LoggerLargeWalkBackward.txt", "assets/animations/LoggerLarge/LoggerLargeWalkBackward.png", 1.0f, "assets/StaticSprites/LoggerLargeIdleBackward.png");
	AddAnimation("loggerlargeattackforward", "assets/animations/LoggerLarge/LoggerLargeAttack.txt", "assets/animations/LoggerLarge/LoggerLargeAttack.png", 0.4f, "assets/StaticSprites/LoggerLargeIdleForward.png");
	AddAnimation("loggerlargeattackbackward", "assets/animations/LoggerLarge/LoggerLargeAttack.txt", "assets/animations/LoggerLarge/LoggerLargeAttack.png", 0.4f, "assets/StaticSprites/LoggerLargeIdleBackward.png");
	AddAnimation("loggerlargeattackleft", "assets/animations/LoggerLarge/LoggerLargeAttack.txt", "assets/animations/LoggerLarge/LoggerLargeAttack.png", 0.4f, "assets/StaticSprites/LoggerLargeIdleLeft.png");
	AddAnimation("loggerlargeattackright", "assets/animations/LoggerLarge/LoggerLargeAttack.txt", "assets/animations/LoggerLarge/LoggerLargeAttack.png", 0.4f, "assets/StaticSprites/LoggerLargeIdleRight.png");

	AddAnimation("archerwalkforward", "assets/animations/Archer/ArcherWalkForward.txt", "assets/animations/Archer/ArcherWalkForward.png", 0.6f, "assets/StaticSprites/ArcherIdleForward.png");
	AddAnimation("archerwalkleft", "assets/animations/Archer/ArcherWalkLeft.txt", "assets/animations/Archer/ArcherWalkLeft.png", 0.6f, "assets/StaticSprites/ArcherIdleForward.png");
	AddAnimation("archerwalkright", "assets/animations/Archer/ArcherWalkRight.txt", "assets/animations/Archer/ArcherWalkRight.png", 0.6f, "assets/StaticSprites/ArcherIdleForward.png");
	AddAnimation("archerwalkbackward", "assets/animations/Archer/ArcherWalkBackward.txt", "assets/animations/Archer/ArcherWalkBackward.png", 0.6f, "assets/StaticSprites/ArcherIdleForward.png");
	AddAnimation("archerattackforward", "assets/animations/Archer/ArcherAttackForward.txt", "assets/animations/Archer/ArcherAttackForward.png", 0.4f, "assets/StaticSprites/ArcherIdleForward.png");
	AddAnimation("archerattackleft", "assets/animations/Archer/ArcherAttackLeft.txt", "assets/animations/Archer/ArcherAttackLeft.png", 0.4f, "assets/StaticSprites/ArcherIdleLeft.png");
	AddAnimation("archerattackright", "assets/animations/Archer/ArcherAttackRight.txt", "assets/animations/Archer/ArcherAttackRight.png", 0.4f, "assets/StaticSprites/ArcherIdleRight.png");
	AddAnimation("archerattackbackward", "assets/animations/Archer/ArcherAttackBackward.txt", "assets/animations/Archer/ArcherAttackBackward.png", 0.4f, "assets/StaticSprites/ArcherIdleBackward.png");

	AddAnimation("powerup_blue", "assets/Animations/BLUE/BLUE_timerSheet.txt", "assets/Animations/BLUE/BLUE_TimerSheet.png", 10.0f, "assets/StaticSprites/PowerupBackground1.png");
	AddAnimation("powerup_green", "assets/Animations/GREEN/GREEN_timerSheet.txt", "assets/Animations/GREEN/GREEN_TimerSheet.png", 10.0f, "assets/StaticSprites/PowerupBackground2.png");
	AddAnimation("powerup_red", "assets/Animations/RED/RED_timerSheet.txt", "assets/Animations/RED/RED_timerSheet.png", 10.0f, "assets/StaticSprites/PowerupBackground3.png");

	AddAnimation("tower1attack", "assets/animations/towers/attack/Tower1Attack.txt", "assets/animations/Towers/attack/Tower1Attack.png", 0.2f, "assets/StaticSprites/Tower1Idle.png");
	AddAnimation("tower3attack", "assets/animations/towers/attack/Tower3Attack.txt", "assets/animations/Towers/attack/Tower3Attack.png", 0.2f, "assets/StaticSprites/Tower3Idle.png");
	AddAnimation("tower4attack", "assets/animations/towers/attack/Tower4Attack.txt", "assets/animations/Towers/attack/Tower4Attack.png", 0.2f, "assets/StaticSprites/Tower4Idle.png");
	AddAnimation("tower6attack", "assets/animations/towers/attack/Tower6Attack.txt", "assets/animations/Towers/attack/Tower6Attack.png", 0.2f, "assets/StaticSprites/Tower6Idle.png");
	AddAnimation("tower8attack", "assets/animations/towers/attack/Tower8Attack.txt", "assets/animations/Towers/attack/Tower8Attack.png", 0.2f, "assets/StaticSprites/Tower8Idle.png");
	AddAnimation("tower9attack", "assets/animations/towers/attack/Tower9Attack.txt", "assets/animations/Towers/attack/Tower9Attack.png", 0.2f, "assets/StaticSprites/Tower9Idle.png");
	AddAnimation("tower1death", "assets/animations/towers/death/Tower1Death.txt", "assets/animations/Towers/death/Tower1Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower2death", "assets/animations/towers/death/Tower2Death.txt", "assets/animations/Towers/death/Tower2Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower3death", "assets/animations/towers/death/Tower3Death.txt", "assets/animations/Towers/death/Tower3Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower4death", "assets/animations/towers/death/Tower4Death.txt", "assets/animations/Towers/death/Tower4Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower5death", "assets/animations/towers/death/Tower5Death.txt", "assets/animations/Towers/death/Tower5Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower6death", "assets/animations/towers/death/Tower6Death.txt", "assets/animations/Towers/death/Tower6Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower7death", "assets/animations/towers/death/Tower7Death.txt", "assets/animations/Towers/death/Tower7Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower8death", "assets/animations/towers/death/Tower8Death.txt", "assets/animations/Towers/death/Tower8Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");
	AddAnimation("tower9death", "assets/animations/towers/death/Tower9Death.txt", "assets/animations/Towers/death/Tower9Death.png", 0.5f, "assets/StaticSprites/AlphaSquare.png");

	AddAnimation("lifetreeidle", "assets/animations/LifeTree/LifeTreeIdle.txt", "assets/animations/LifeTree/LifeTreeIdle.png", 2.0f, "assets/staticsprites/LifeTree.png");

	AddAnimation("bloodeffect1", "assets/animations/effects/BloodEffect1.txt", "assets/animations/Effects/BloodEffect1.png", 0.2f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("fireeffect1", "assets/animations/effects/FireEffect1.txt", "assets/animations/Effects/FireEffect1.png", 1.0f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("loggersmalldeath", "assets/animations/effects/LoggerSmallDeath.txt", "assets/animations/Effects/LoggerSmallDeath.png", 0.2f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("archerdeath", "assets/animations/effects/ArcherDeath.txt", "assets/animations/Effects/ArcherDeath.png", 0.2f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("loggerlargedeath", "assets/animations/effects/LoggerLargeDeath.txt", "assets/animations/Effects/LoggerLargeDeath.png", 0.2f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("towerspawn1", "assets/animations/effects/towerspawn1.txt", "assets/animations/Effects/towerspawn1.png", 1.0f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("towerspawn2", "assets/animations/effects/towerspawn2.txt", "assets/animations/Effects/towerspawn2.png", 1.0f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("healingeffect", "assets/animations/effects/HealingEffect.txt", "assets/animations/effects/HealingEffect.png", 0.7f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("poisoneffect", "assets/animations/effects/PoisonEffect.txt", "assets/animations/effects/PoisonEffect.png", 0.7f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("holdeffect", "assets/animations/effects/HoldEffect.txt", "assets/animations/effects/HoldEffect.png", 0.7f, "assets/staticsprites/AlphaSquare.png");
	AddAnimation("sloweffect", "assets/animations/effects/SlowEffect.txt", "assets/animations/effects/SlowEffect.png", 0.7f, "assets/staticsprites/AlphaSquare.png");

	AddAnimation("towerprojectile6", "assets/animations/Projectiles/towerprojectile6.txt", "assets/animations/Projectiles/towerprojectile6.png", 0.2f, "assets/staticsprites/TowerProjectileDefault.png");
	AddAnimation("archerprojectile", "assets/Animations/Projectiles/ArcherProjectile.txt", "assets/animations/Projectiles/ArcherProjectile.png", 0.2f, "assets/staticsprites/TowerProjectileDefault.png");
	AddAnimation("towerprojectile5", "assets/Animations/Projectiles/TowerProjectile5.txt", "assets/animations/Projectiles/TowerProjectile5.png", 0.2f, "assets/staticsprites/TowerProjectileDefault.png");

}
