// GameState.cpp

#include "stdafx.h"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "DrawManager.hpp"
#include "MenuState.h"
#include "AudioManager.hpp"
#include "Powerup.h"
#include "TowerShop.h"
#include "AttributeManager.h"
#include "AnimationStore.h"
#include "Player.h"
#include "TileMap.h"

MenuState::MenuState()
{
	
	m_texture.loadFromFile("assets/StaticSprites/arrow.png");
	m_sprite.setTexture(m_texture);
	m_texture_manager = ServiceLocator<TextureManager>::GetService();
	m_draw_manager = ServiceLocator<DrawManager>::GetService();
	m_audio_manager = ServiceLocator<AudioManager>::GetService();

	m_animationStore = new AnimationStore(m_texture_manager);
	AttributeManager::Initialize("assets/config.txt");
	

	m_sprite.setPosition(500, 435);

	m_backgroundt.loadFromFile("assets/StaticSprites/Menu1.jpg");
	m_background.setTexture(m_backgroundt);
	m_background.setPosition(0, 0);

	m_startt.loadFromFile("assets/StaticSprites/Start.png");
	m_start.setTexture(m_startt);
	m_start.setPosition(0, 0);

	m_controlst.loadFromFile("assets/StaticSprites/Controls.png");
	m_controls.setTexture(m_controlst);
	m_controls.setPosition(0, 0);


	m_creditst.loadFromFile("assets/StaticSprites/Credits.png");
	m_credits.setTexture(m_creditst);
	m_credits.setPosition(0, 0);

	m_soundt.loadFromFile("assets/StaticSprites/Sound.png");
	m_sound.setTexture(m_soundt);
	m_sound.setPosition(0, 0);

	m_quitt.loadFromFile("assets/StaticSprites/Quit.png");
	m_quit.setTexture(m_quitt);
	m_quit.setPosition(0, 0);

	m_state = 1;
	m_Wkeypressed = false;
	m_Skeypressed = false;
}

MenuState::~MenuState()
{
	
	if (m_animationStore)
		delete m_animationStore;

	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i])
		{
			delete m_entities[i];
		}
	}
	m_entities.clear();
}

bool MenuState::Enter()
{
	return true;
}

void MenuState::Exit()
{
}

bool MenuState::Update(float deltatime)
{
	if (m_state > 5)
	{
		m_state = 1;
	}
	if (m_state < 1)
	{
		m_state = 5;
	}

	

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && m_Skeypressed == false)
		{
			m_state += 1;
			m_Skeypressed = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && m_Wkeypressed == false)
		{
			m_state -= 1;
			m_Wkeypressed = true;
		}
	
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		m_Wkeypressed = false;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		m_Skeypressed = false;
	}

	//switch (m_state)
	//{
	//case 1:
	//	m_sprite.setPosition(800, 435);
	//	break;
	//case 2:
	//	m_sprite.setPosition(800, 1000);
	//	break;
	//}
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		switch (m_state)
		{
		case 1:
			return false;
		case 5:
			m_draw_manager->GetWindow()->close();
			//return false;
		}
	}

	//m_keypressed = false;
	m_animationStore->Update(deltatime);

	return true;
}

void MenuState::Draw()
{
	m_draw_manager->Draw(m_background, sf::RenderStates::Default);
	for (int i = 0; i < m_entities.size(); i++)
	{
		if (m_entities[i]->IsActive() == true)
		{
			m_draw_manager->Draw(*m_entities[i]->GetSprite(), sf::RenderStates::Default);
		}
	}
	//m_draw_manager->Draw(m_text1, sf::RenderStates::Default);
	//m_draw_manager->Draw(m_text2, sf::RenderStates::Default);
	//m_draw_manager->Draw(m_text3, sf::RenderStates::Default);
	//m_draw_manager->Draw(m_sprite, sf::RenderStates::Default);

	switch (m_state)
	{
	case 1: 
		m_draw_manager->Draw(m_start, sf::RenderStates::Default);
		break;
	case 2:
		m_draw_manager->Draw(m_controls, sf::RenderStates::Default);
		break;
	case 3:
		m_draw_manager->Draw(m_credits, sf::RenderStates::Default);
		break;
	case 4:
		m_draw_manager->Draw(m_sound, sf::RenderStates::Default);
		break;
	case 5:
		m_draw_manager->Draw(m_quit, sf::RenderStates::Default);
		break;
	}
	
}

std::string MenuState::GetNextState()
{
	switch (m_state)
	{
	case 1:
		return "GameState";
		break;
	}
	//return "GameState";
}
