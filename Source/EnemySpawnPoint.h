// EnemySpawnPoint.h

#pragma once

class TextureManager;
class Entity;
class Pathfinding;

class EnemySpawnPoint
{
public:
	EnemySpawnPoint(int x, int y, TextureManager* textureManager, Entity* mainTarget, std::vector<Entity*>* entities, Pathfinding* pathfinder);
	~EnemySpawnPoint();

	void Update(float deltatime);

	void Activate();
	void Deactivate();

	unsigned int GetRoundTimer();
	unsigned int GetRoundCooldownTimer();
	int GetCurrentRound();
	int GetMaxRounds();

private:
	void Initialize();
	void PreGenerateUnits();

	void SpawnWave();
	void SpawnLoggerSmall(int amount);
	void SpawnLoggerLarge(int amount);
	void SpawnArcher(int amount);
	void SpawnFlameThrower(int amount);
	sf::Vector2i GetRandomSpawnPos();

private:
	std::vector<Entity*>* m_entities;

	TextureManager* m_textureManager;

	Entity* m_mainTarget;

	sf::Clock m_roundTimer;
	sf::Clock m_roundCooldowntimer;
	sf::Clock m_waveTimer;
	sf::Clock m_startTimer;

	sf::Texture* m_loggerSmallTexture;
	sf::Texture* m_loggerLargeTexture;
	sf::Texture* m_archerTexture;
	sf::Texture* m_flameThrowerTexture;
	sf::Texture* m_archerProjectileTexture;

	Pathfinding* m_pathfinder;

	bool m_active;
	bool m_roundActive;

	int m_x;
	int m_y;

	int m_startWaitingPeriod;
	int m_roundLength;
	int m_spawnRadius;
	int m_wavesPerRound;
	int m_currentWave;
	int m_waveCooldown;
	int m_roundCooldownValue;
	int m_maxRounds;
	int m_currentRound;

	int m_loggerSmallEntryRound;
	int m_loggerLargeEntryRound;
	int m_archerEntryRound;
	int m_flameThrowerEntryRound;

	int m_loggerSmallRoundStartValue;
	int m_loggerLargeRoundStartValue;
	int m_archerRoundStartValue;
	int m_flameThrowerRoundStartValue;

	int m_loggerSmallRoundEndValue;
	int m_loggerLargeRoundEndValue;
	int m_archerRoundEndValue;
	int m_flameThrowerRoundEndValue;

	float m_globalRoundUnitMultiplier;
};