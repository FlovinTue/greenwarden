/// Tree.cpp
//
#include "stdafx.h"
#include "Tree.h"

// static variable
std::vector<sf::Texture*> Tree::m_static_textures;


Tree::Tree(sf::Vector2f position)
{
	m_active = true;
	m_collider = new Collider(position.x, position.y, 32, 32);

	// set position
	m_sprite.setPosition(position);
	m_x = position.x;
	m_y = position.y;


	// random texture
	m_sprite.setTexture(*m_static_textures[rand() % m_static_textures.size()]);
	m_sprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().height / 2);//center

	// random color
	m_sprite.setColor(sf::Color(rand() % 100 + 155, rand() % 25 + 230, rand() % 55 + 200));

	// random X-angle (mirrored, or not)
	switch (rand() % 2)
	{
	case 0: m_sprite.setScale(1, 1); break;
	case 1: m_sprite.setScale(-1, 1); break;
	}

}

Tree::~Tree()
{
	if (m_collider)
		delete m_collider;
}

EType Tree::GetType() { return E_FOREST; };