
#pragma once

#include "Entity.h"
#include "TextureManager.hpp"
#include "Sound.h"
//...
class Tilemap;
class TextureManager;
class Wave;
class TowerShop;
class PlayerHealthBar;
class DOTEffect;
class HOTEffect;
class SeedCounterHud;
class PowerUpHud;
class EffectHost;

class Player : public Entity
{
public:
	Player(Tilemap* tilemap, std::vector<Entity*>* entities, TowerShop* towershop, float x, float y);
	~Player();

	void Update(float deltatime);
	EType GetType();
	void dealdamage(float damage);
	void AddHealth(float value);
	void PlaceTower();
	void fireprojectile(int direction);
	void ActivateStrengthPowerUp();
	void DeactivateStrengthPowerUp();
	void SubstractHealthOverTime(float damage, int duration);
	void AddHealthOverTime(float totalHealing, int duration);
	void RefreshOverTimeEffects();
	int GetCurrentHealth();
	void Reset();
	void SetDead();

	int GetSeed1();
	int GetSeed2();
	int GetSeed3();

	bool Getm_seed1_active();
	bool Getm_seed2_active();
	bool Getm_seed3_active();

	bool Getm_seed1_2_active();
	bool Getm_seed2_2_active();
	bool Getm_seed3_2_active();

	void DecreaseSeed1();
	void DecreaseSeed2();
	void DecreaseSeed3();
	int GetTime();

private:
	//bool m_active;
	std::vector<Entity*>* m_entities;
	std::vector<DOTEffect> m_dOTEffects;
	std::vector<HOTEffect> m_hOTEffects;

	PlayerHealthBar* m_healthBar;
	TowerShop* m_towershop;
	Tilemap* m_tilemap;
	TextureManager* m_texture_manager;
	PowerUpHud* m_poweruphud1;
	PowerUpHud* m_poweruphud2;
	PowerUpHud* m_poweruphud3;
	EffectHost* m_effectHost;

	sf::Texture m_texture;
	sf::Time m_time;
	sf::Clock m_clock;
	sf::Time m_strengthpoweruptimer;
	sf::Time m_respawntimer;

	sf::Texture m_healthbaroutlinetexture;
	sf::Sprite m_healthbaroutline;

	sf::Texture m_healthbarbackgroundtexture;
	sf::Sprite m_healthbarbackground;
	bool m_dead;
	float m_healthpoints;
	int m_maxHealth;
	float m_attackspeed;

	bool m_firing;
	bool m_strengthpowerupactivated;

	int m_maxtoweramount;
	int m_toweramount;
	int m_damage;
	int m_projectilespeed;

	Sound m_sound_attack;
	Sound m_sound_get_hit;
	Sound m_sound_death;
	Sound m_sound_pick_up_seed;
	Sound m_sound_plant_seed;
	Sound m_sound_towerspawn; // <--- !
	Sound m_sound_powerup1;
	Sound m_sound_powerup2;

	unsigned m_seed_count_type1;
	unsigned m_seed_count_type2;
	unsigned m_seed_count_type3;

	bool m_seed1_active;
	bool m_seed2_active;
	bool m_seed3_active;

	bool m_seed1_2_active;
	bool m_seed2_2_active;
	bool m_seed3_2_active;

	bool m_key1_pressed;
	bool m_key2_pressed;
	bool m_key3_pressed;


	SeedCounterHud* m_seedcounterhud1;
	SeedCounterHud* m_seedcounterhud2;
	SeedCounterHud* m_seedcounterhud3;

};