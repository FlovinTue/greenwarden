// EffectHost.cpp

#include "stdafx.h"
#include "EffectHost.h"
#include "AnimationStore.h"

EffectHost::EffectHost(Entity* host)
{
	m_host = host;

	m_x = 0;
	m_y = 0;
	m_w = 64;
	m_h = 64;
	m_collider = nullptr;
	m_active = true;
}
EffectHost::~EffectHost()
{

}
void EffectHost::Update(float deltatime)
{
	if (m_host)
	{
		if (AnimationStore::GetActiveAnimation(this) != "")
		{
			short hostCenterX = m_host->GetX() + (m_host->GetSprite()->getTextureRect().width / 2);
			short hostCenterY = m_host->GetY() + (m_host->GetSprite()->getTextureRect().height / 2);
			short hostOriginX = m_host->GetSprite()->getOrigin().x;
			short hostOriginY = m_host->GetSprite()->getOrigin().y;
			short posX = hostCenterX - hostOriginX - (m_sprite.getTextureRect().width / 2);
			short posY = hostCenterY - hostOriginY - (m_sprite.getTextureRect().height / 2);

			m_sprite.setPosition(posX, posY);

			m_y = m_host->GetY() + 1; // For drawing on-top via sorting method in GameState
		}
	}
}
EType EffectHost::GetType()
{
	return E_EFFECTHOST;
}
void EffectHost::PlayEffect(const std::string& effectName, bool looping)
{
	AnimationStore::Play(this, effectName, looping);

	short hostCenterX = m_host->GetX() + (m_host->GetSprite()->getTextureRect().width / 2);
	short hostCenterY = m_host->GetY() + (m_host->GetSprite()->getTextureRect().height / 2);
	short hostOriginX = m_host->GetSprite()->getOrigin().x;
	short hostOriginY = m_host->GetSprite()->getOrigin().y;
	short posX = hostCenterX - hostOriginX - (m_sprite.getTextureRect().width / 2);
	short posY = hostCenterY - hostOriginY - (m_sprite.getTextureRect().height / 2);

	m_sprite.setPosition(posX, posY);

	m_y = m_host->GetY() + 1;
}
