// GameState.hpp

#pragma once

#include "Entity.h"
#include "AbstractState.hpp"
class TowerShop;
class TileMap;
class AnimationStore;

class TextureManager;
class DrawManager;
class AudioManager;

class MenuState : public AbstractState
{
public:
	MenuState();
	~MenuState();

	virtual bool Enter();
	virtual void Exit();
	virtual bool Update(float deltatime);
	virtual void Draw();
	virtual std::string GetNextState();

private:
	std::vector<Entity*> m_entities;
	sf::Font m_font;
	sf::Text m_text1;
	sf::Text m_text2;
	sf::Text m_text3;
	TextureManager* m_texture_manager;
	DrawManager* m_draw_manager;
	AudioManager* m_audio_manager;
	sf::Sprite m_sprite;
	sf::Texture m_texture;
	bool m_Wkeypressed;
	bool m_Skeypressed;
	//int m_arrowposition;
	int m_state;
	AnimationStore* m_animationStore;
	sf::Texture m_backgroundt;
	sf::Sprite m_background;

	sf::Texture m_startt;
	sf::Sprite m_start;


	sf::Texture m_controlst;
	sf::Sprite m_controls;

	sf::Texture m_soundt;
	sf::Sprite m_sound;

	sf::Texture m_creditst;
	sf::Sprite m_credits;

	sf::Texture m_quitt;
	sf::Sprite m_quit;




};


