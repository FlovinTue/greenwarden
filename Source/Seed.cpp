/// Seed.cpp
//
#include "stdafx.h"
#include "Seed.h"


// static variables
sf::Texture* Seed::s_textures[3];



Seed::Seed()
{
	m_sprite.setTexture(*s_textures[0]);

	m_collider = new Collider(m_x, m_y, 15, 16);
	m_collider->SetParent(this);
	m_active = false;

	m_time = 0.0f;

	// default seed type
	setSeedType(SEED_TYPE_1);

}
Seed::~Seed()
{
	if (m_collider)
		delete m_collider;
}


void Seed::Update(float deltatime)
{
	// time++.
	m_time += deltatime;

	// cute up-and-down animation.
	m_y += cos(m_time*3.0f) / 3.0f;
	m_sprite.setPosition(m_x, m_y);

}


void Seed::setPosition(float x, float y)
{
	m_x = x;
	m_y = y;
	m_sprite.setPosition(x, y);
}

void Seed::setActive(bool active)
{
	m_collider->SetPosition(m_x, m_y);
	m_active = active;
}


void Seed::setSeedType(SEED_TYPE seed_type)
{
	m_seed_type = seed_type;
	switch (seed_type)
	{
	case SEED_TYPE_1: m_sprite.setTexture(*s_textures[0]);
		break;
	case SEED_TYPE_2: m_sprite.setTexture(*s_textures[1]);
		break;
	case SEED_TYPE_3: m_sprite.setTexture(*s_textures[2]);
		break;
	}
}


SEED_TYPE Seed::getSeedType()
{
	return m_seed_type;
}
EType Seed::GetType()
{
	return E_SEED;
}



void Seed::setTextures(sf::Texture* type1, sf::Texture* type2, sf::Texture* type3)
{
	s_textures[0] = type1;
	s_textures[1] = type2;
	s_textures[2] = type3;
}