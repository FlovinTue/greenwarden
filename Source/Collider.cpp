// Collider.cpp

#include "stdafx.h"
#include "Entity.h"
#include "Collider.h"

Collider::Collider(int x, int y, int w, int h)
{
	m_parent = nullptr;
	m_x = x;
	m_y = y;
	m_w = w;
	m_h = h;
}

bool Collider::HasParent()
{
	return m_parent != nullptr;
}

void Collider::SetParent(Entity* parent)
{
	m_parent = parent;
}

Entity* Collider::GetParent()
{
	return m_parent;
}

void Collider::SetPosition(int x, int y)
{
	m_x = x;
	m_y = y;
}

void Collider::SetWidthHeight(int width, int height)
{
	m_w = width;
	m_h = height;
}

int Collider::GetX()
{
	return m_x;
}

int Collider::GetY()
{
	return m_y;
}

int Collider::GetWidth()
{
	return m_w;
}

int Collider::GetHeight()
{
	return m_h;
}

void Collider::Refresh()
{
	m_x = m_parent->GetX();
	m_y = m_parent->GetY();
}
