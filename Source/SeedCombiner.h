#pragma once

class Player;
class SeedCombiner
{
	SeedCombiner(const SeedCombiner&);
	SeedCombiner& operator=(const SeedCombiner&);
public:
	SeedCombiner();
	~SeedCombiner();

	static int Combine(Player* m_player, bool seed1, bool seed2, bool seed3, bool seed1_2, bool seed2_2, bool seed3_2);
	static int CombineCheck(Player* m_player, bool seed1, bool seed2, bool seed3, bool seed1_2, bool seed2_2, bool seed3_2);
private:

};