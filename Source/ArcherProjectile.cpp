// ArcherProjectile.cpp

#include "stdafx.h"
#include "ArcherProjectile.h"
#include "CollisionManager.hpp"
#include "AttributeManager.h"
#include "Tower.h"
#include "LifeTree.h"
#include "Player.h"
#include "AnimationStore.h"

ArcherProjectile::ArcherProjectile(int x, int y, sf::Texture* texture)
{
	m_x = x;
	m_y = y;
	m_w = 16;
	m_h = 16;
	m_damage = AttributeManager::GetAttribute("Projectile_archer_damage");
	m_speed = AttributeManager::GetAttribute("Projectile_global_speed");
	m_effectDuration = AttributeManager::GetAttribute("Projectile_archer_duration");
	m_active = false;
	m_sprite.setTexture(*texture);

	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);
}
ArcherProjectile::~ArcherProjectile()
{
	if (m_collider)
		delete m_collider;
}
void ArcherProjectile::Update(float deltatime)
{
	if (!m_target)
	{
		Deactivate();
		return;
	}

	if (m_target->IsActive() == true && IsActive() == true)
	{
		HomeOnTarget(deltatime);

		int overlapX = 2;
		int overlapY = 2;
		if (CollisionManager::Check(m_collider, m_target->GetCollider(), overlapX, overlapY))
		{
			if (rand() % 2 == 1)
				ApplyProjectileEffect();

			Deactivate(); 
		}
		m_collider->Refresh();
		m_sprite.setPosition(m_x, m_y);
		m_sprite.setRotation(m_spriteRotation);
	}
	else if (m_target->IsActive() == false)
		Deactivate();
}
EType ArcherProjectile::GetType()
{
	return E_ARCHERPROJECTILE;
}
void ArcherProjectile::Reactivate(Entity* target, int x, int y)
{
	if (m_active == false)
	{
		m_active = true;
		m_target = target;
		m_x = x;
		m_y = y;
		m_collider->Refresh();
		AnimationStore::Play(this, "archerprojectile", true);
	}
}

void ArcherProjectile::HomeOnTarget(float deltatime)
{
	float distX = (m_target->GetX() + (m_target->GetW() / 2)) - (GetX() + (GetW() / 2));
	float distY = (m_target->GetY() + (m_target->GetH() / 2)) - (GetY() + (GetH() / 2));
	float units = 1.0f / (abs(distX) + abs(distY));
	float directionX = (distX * units);
	float directionY = (distY * units);

	m_x += directionX * m_speed * deltatime;
	m_y += directionY * m_speed * deltatime;

	// SpriteRotation
	short spriteRotation = 0;

	if (directionX > 0)
	{
		if (directionY < 0)
			spriteRotation = directionX * 90.0f;
		else if (directionY > 0)
			spriteRotation = (directionY * 90.0f) + 90.0f;
	}
	else if (directionX < 0)
	{
		if (directionY > 0)
			spriteRotation = abs(directionX * 90.0f) + 180.0f;
		else if (directionY < 0)
			spriteRotation = abs(directionY * 90.0f) + 270.0f;
	}
	short rotationOffset = spriteRotation + 90; // Offset

	m_spriteRotation = rotationOffset % 360;
}
void ArcherProjectile::ApplyProjectileEffect()
{
	if (m_target)
	{
		switch (m_target->GetType())
		{
		case (E_TOWER) :
		{
			Tower* tower = static_cast<Tower*>(m_target);
			tower->SubstractHealthOverTime(m_damage, m_effectDuration);
			break;
		}
		case(E_PLAYER) :
		{
			Player* player = static_cast<Player*>(m_target);
			player->SubstractHealthOverTime(m_damage, m_effectDuration);
			break;
		}
		case(E_LIFETREE) :
		{
			LifeTree* lifeTree = static_cast<LifeTree*>(m_target);
			lifeTree->SubstractHealthOverTime(m_damage, m_effectDuration);
			break;
		}
		default:
			break;
		}
	}
}
void ArcherProjectile::Deactivate()
{
	m_active = false;
	AnimationStore::Stop(this);
}