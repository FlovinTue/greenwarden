// CollisionManager.hpp

#ifndef COLLISIONMANAGER_HPP_INCLUDED
#define COLLISIONMANAGER_HPP_INCLUDED


class Collider;

class CollisionManager
{
	// note(tommi): making the copy constructor and 
	// assignment operator private we make the class
	// non-copyable
	CollisionManager(const CollisionManager&);
	CollisionManager& operator=(const CollisionManager&);

public:
	CollisionManager();
	~CollisionManager();

	static bool Check(Collider* lhs, Collider* rhs, int& overlapX, int& overlapY);

	bool Initialize();
	void Shutdown();

private:

};


#endif // COLLISIONMANAGER_HPP_INCLUDED
