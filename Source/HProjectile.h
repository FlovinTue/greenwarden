// HomingHProjectile.h

#pragma once
#include "Entity.h"

// 1: small range(damage) 2: small magic(damage) 3: large range(high damage) 4: DoT 5: Slow 6: Piercing

class HProjectile : public Entity
{
public:
	HProjectile(Entity* target, sf::Texture* texture, int x, int y, int projectileType);
	~HProjectile();

	void Update(float deltatime);
	EType GetType();

	float GetSpeed();
	void SetSpeed(float speed);
	int GetDamage();
	void SetDamage(int damage);
	Entity* GetTarget();
	void SetTarget(Entity* target);
	void Reactivate(Entity* target, sf::Texture* texture, int projectileType, int x, int y);
	void Deactivate();

private:
	void HomeOnTarget(float deltatime);
	void ApplyProjectileEffect();
	void SetProjectileType(int projectileType);

private:
	Entity* m_target;

	float m_speed;
	float m_spriteRotation;

	int m_damage;
	int m_projectileType; 
	int m_effectDuration;
	int m_slowPercentage;

};