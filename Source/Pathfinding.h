/// Pathfinding.h
//
#pragma once

class Tilemap;

struct Node
{
	unsigned short x, y;
	float F, H, G;
	Node* parent;
	bool passable;
};


class Pathfinding
{
public:
	Pathfinding();
	~Pathfinding();

	void setTilemap(Tilemap* parent);

	bool IsInaccessible(sf::Vector2f targetLoc);

	std::vector<sf::Vector2f> getPath(sf::Vector2f start_pos, sf::Vector2f goal_pos);
	std::vector<sf::Vector2f> getPath(short start_index, short goal_index);


private:
	Node* GetNodeAt(unsigned x, unsigned y);
	Node* FindLowestScoreInOpenList();

	void ResetNodes();

	void AddNodeToList(std::vector<Node*>& list, Node* node);
	void RemoveNodeFromList(std::vector<Node*>& list, Node* node);

	bool IsNodeInList(std::vector<Node*>& list, Node* node);

	float calculate_H(float x, float y);

private:

	Tilemap* m_tilemap;

	std::vector<Node> m_node_vector;

	std::vector<Node*> m_open_list;
	std::vector<Node*> m_closed_list;

	unsigned m_start_index;
	unsigned m_goal_index;

	unsigned m_map_width;
	unsigned m_map_height;
};