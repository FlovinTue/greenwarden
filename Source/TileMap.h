/// Tilemap.h
//
#pragma once


struct Tile
{
	short state;
	const short index;
	const sf::Vector2f position;
	const unsigned& width;
	const unsigned& height;
};


class Tilemap
{
public:

	void create(unsigned width, unsigned height, unsigned tilewidth, unsigned tileheight);

	Tile& getTile(unsigned row, unsigned column);
	Tile& getTile(sf::Vector2f point);
	Tile& getTile(short index);

	Tile& operator[](short index);

	unsigned size() { return m_width * m_height; };
	unsigned size_width() { return m_width; };
	unsigned size_height() { return m_height; };

private:
	std::vector<Tile> m_tile_vector;

	unsigned m_width, m_height;
	unsigned m_tilewidth, m_tileheight;
};