#include "stdafx.h"
#include "SeedCombiner.h"
#include "Player.h"
SeedCombiner::SeedCombiner()
{

}
SeedCombiner::~SeedCombiner()
{

}

int SeedCombiner::Combine(Player* m_player, bool seed1, bool seed2, bool seed3, bool seed1_2, bool seed2_2, bool seed3_2)
{
	/*if (seed1 == true && seed2 == true && seed3 == true)
	{
	return 0;
	}*/
	if (seed1 == true && seed2 == false && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed1() > 0)
		{
			m_player->DecreaseSeed1();
			return 1;
			
		}
		else {
			return 0;
		}
	}
	else if (seed1 == false && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed2() > 0)
		{
			m_player->DecreaseSeed2();
			return 2;
		}
		else {
			return 0;
		}
	}
	else if (seed1 == false && seed2 == false && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed3() > 0)
		{
			m_player->DecreaseSeed3();
			return 3;
		}
		else {
			return 0;
		}
	}
	else if (seed1 == true && seed2 == false && seed3 == false && seed1_2 == true && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed1() > 1)
		{
			m_player->DecreaseSeed1();
			m_player->DecreaseSeed1();
			return 4;
		}
		else {
			return 0;
		}
	}


	if (seed1 == true && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed1() > 0 && m_player->GetSeed2() > 0)
		{
			m_player->DecreaseSeed1();
			m_player->DecreaseSeed2();
			return 5;
		}
		else {
			return 0;
		}

	}
	if (seed1 == true && seed2 == false && seed3 == true  && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed1() > 0 && m_player->GetSeed3() > 0)
		{
			m_player->DecreaseSeed1();
			m_player->DecreaseSeed3();
			return 6;
		}
		else {
			return 0;
		}


	}
	if (seed1 == false && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == true && seed3_2 == false)
	{
		if (m_player->GetSeed2() > 1)
		{
			m_player->DecreaseSeed2();
			m_player->DecreaseSeed2();
			return 7;
		}
		else {
			return 0;
		}

	}
	if (seed1 == false && seed2 == true && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		if (m_player->GetSeed2() > 0 && m_player->GetSeed3() > 0)
		{
			m_player->DecreaseSeed2();
			m_player->DecreaseSeed3();
			return 8;
		}
		else {
			return 0;
		}


	}
	if (seed1 == false && seed2 == false && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == true)
	{
		if (m_player->GetSeed3() > 1)
		{
			m_player->DecreaseSeed3();
			m_player->DecreaseSeed3();
			return 9;
		}
		else {
			return 0;
		}

	}

	return 0;
}

int SeedCombiner::CombineCheck(Player* m_player, bool seed1, bool seed2, bool seed3, bool seed1_2, bool seed2_2, bool seed3_2)
{
	/*if (seed1 == true && seed2 == true && seed3 == true)
	{
	return 0;
	}*/
	if (seed1 == true && seed2 == false && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			
			return 1;

		
	}
	else if (seed1 == false && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			return 2;
		
	}
	else if (seed1 == false && seed2 == false && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			return 3;
		
		
	}
	else if (seed1 == true && seed2 == false && seed3 == false && seed1_2 == true && seed2_2 == false && seed3_2 == false)
	{
		
			return 4;
		
	
	}


	if (seed1 == true && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			
			return 5;
		

	}
	if (seed1 == true && seed2 == false && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			return 6;
		
		


	}
	if (seed1 == false && seed2 == true && seed3 == false && seed1_2 == false && seed2_2 == true && seed3_2 == false)
	{
		
			return 7;
		
		

	}
	if (seed1 == false && seed2 == true && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == false)
	{
		
			return 8;
	


	}
	if (seed1 == false && seed2 == false && seed3 == true && seed1_2 == false && seed2_2 == false && seed3_2 == true)
	{
		
			return 9;
		
	}

	return 0;
}