// Collider.h

#ifndef COLLIDER_H_INCLUDED
#define COLLIDER_H_INCLUDED

class Entity;

class Collider
{
public:
	Collider(int x, int y, int w, int h);

	bool HasParent();
	void SetParent(Entity* parent);
	Entity* GetParent();

	void SetPosition(int x, int y);
	void SetWidthHeight(int width, int height);

	int GetX();
	int GetY();
	int GetWidth();
	int GetHeight();

	void Refresh();

private:
	Entity* m_parent;
	int m_x;
	int m_y;
	int m_w;
	int m_h;
};

#endif // COLLIDER_H_INCLUDED
