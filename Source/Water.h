/// Water.h
//
#pragma once
#include "Entity.h"


class Tilemap;

class Water : public Entity
{
	sf::Sprite m_sprite;
	static sf::Texture* m_texture;
	static Tilemap* m_tilemap;
	static std::vector<Water*> m_water_objects;

public:
	Water(sf::Vector2f position);
	~Water();

	static void setTilemap(Tilemap* tilemap);
	static void setTexture(sf::Texture* texture);

private:
	static void UpdateSprites();
};