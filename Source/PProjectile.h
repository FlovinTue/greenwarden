// PProjectile.h

#pragma once
#include "Entity.h"

class PProjectile : public Entity
{
public:
	PProjectile(Entity* target, std::vector<Entity*>* entities, sf::Texture* texture, int x, int y);
	~PProjectile();
	void Update(float deltatime);
	EType GetType();

	float GetSpeed();
	void SetSpeed(float speed);
	int GetDamage();
	void SetDamage(int damage);
	void Deactivate();
	void Reactivate(Entity* target, int x, int y);

private:
	void SetDirection(Entity* target);
	void Travel(float deltatime);
	bool PreviouslyHit(Entity* target);
	void DamageTargets();
private:
	std::vector<Entity*> m_pierceTracker;
	std::vector<Entity*>* m_entities;
	float m_directionX;
	float m_directionY;
	int m_damage;
	float m_speed;
};