// HealthBar.cpp

#include "stdafx.h"
#include "PlayerHealthbar.h"
#include "Tower.h"
#include "AbstractEnemy.h"
#include "LifeTree.h"
#include "Player.h"

PlayerHealthBar::PlayerHealthBar(Entity* host)
{
	m_active = true;
	m_host = host;
	m_referenceHealthValue = GetHostCurrentHealth();
	m_healthBarOffset = 5;

	//sf::Texture rectangle;
	m_texture.loadFromFile("assets/StaticSprites/PlayerHealthBAR.png");
	m_sprite.setTexture(m_texture);
	m_sprite.setPosition(120,50);


	RefreshBar(GetHostCurrentHealth());
	m_collider = nullptr;
}
PlayerHealthBar::~PlayerHealthBar()
{
}
void PlayerHealthBar::Update(float deltatime)
{
	if (m_host->IsActive())
		RefreshBar(GetHostCurrentHealth());
}
EType PlayerHealthBar::GetType()
{
	return E_HUD;
}
void PlayerHealthBar::SetReferenceHealthValue(int referenceHealthValue)
{
	m_referenceHealthValue = referenceHealthValue;
}
void PlayerHealthBar::RefreshBar(int currentHealthValue)
{
	//m_x = m_host->GetX();
	//m_y = m_host->GetSprite()->getPosition().y - GetH() - m_healthBarOffset;

	float health = currentHealthValue;

	if (health < 0)
		health = 0;

	float healthRatio = health / m_referenceHealthValue;

	int red, green, blue, alpha;

	red = (1 - healthRatio) * 255;
	green = healthRatio * 255;
	blue = 0;
	alpha = 255;

	int barWidth = 230 * healthRatio;

	//m_sprite.setTexture(m_te)
	m_sprite.setTextureRect(sf::IntRect(0, 0, barWidth, 125));

}
int PlayerHealthBar::GetHostCurrentHealth()
{
	
		Player* player = static_cast<Player*>(m_host);
		return player->GetCurrentHealth();

}