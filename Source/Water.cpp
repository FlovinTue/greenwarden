/// Water.cpp
//
#include "stdafx.h"
#include "Water.h"

#include "Tilemap.h"

sf::Texture* Water::m_texture;
Tilemap* Water:: m_tilemap;
std::vector<Water*> Water::m_water_objects;



Water::Water(sf::Vector2f position)
{
	m_active = true;

	m_x = position.x;
	m_y = position.y;
	m_sprite.setPosition(position);
	m_sprite.setTexture(*m_texture);

	//
	//if (m_tilemap) m_water_objects[m_tilemap->getTile(position).index] = this;


	//
	//UpdateSprites();
}

Water::~Water()
{
};



/// STATIC
void Water::setTilemap(Tilemap* tilemap)
{
	m_tilemap = tilemap;
	m_water_objects.clear();
	m_water_objects.resize(tilemap->size());
}
/// STATIC
void Water::setTexture(sf::Texture* texture)
{
	m_texture = texture;
}


/*private*/
/// STATIC
void Water::UpdateSprites()
{
	for (unsigned index = 0; index < m_water_objects.size(); index++)
	{
		if (m_water_objects[index] != nullptr)
		{


			
		}
	}
}