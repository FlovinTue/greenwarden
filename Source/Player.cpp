#include "stdafx.h"
#include "Player.h"
#include "Entity.h"
#include "Tower.h"
#include "TowerShop.h"
#include "Seed.h"
#include "Tilemap.h"
#include "CollisionManager.hpp"
#include "PlayerProjectile.h"
#include "Powerup.h"
#include "Sound.h"
#include "TextureManager.hpp"
#include "ServiceLocator.hpp"
#include "AbstractEnemy.h"
#include "AnimationStore.h"
#include "DOTEffect.h"
#include "PlayerHealthbar.h"
#include "SeedCombiner.h"
#include "PowerUpHud.h"
#include "HOTEffect.h"
#include "EffectHost.h"

#include "SeedCounterHud.h"

Player::Player(Tilemap* tilemap, std::vector<Entity*>* entities, TowerShop* towershop, float x, float y)
{
	m_entities = entities;
	m_towershop = towershop;
	m_texture.loadFromFile("assets/StaticSprites/WardenIdleForward.png");
	//m_texture = m_texture_manager->CreateTextureFromFile("assets/StaticSprites/WardenIdleForward.png");
	m_sprite.setTexture(m_texture);
	m_x = x;
	m_y = y;
	m_active = true;
	m_attackspeed = 0.6f;
	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);

	m_seed_count_type1 = 0;
	m_seed_count_type2 = 0;
	m_seed_count_type3 = 0;

	m_sound_attack.loadFromFile("assets/sounds/warden_attack.wav");
	m_sound_get_hit.loadFromFile("assets/sounds/warden_gets_hit.wav");
	m_sound_death.loadFromFile("assets/sounds/warden_death.wav");
	m_sound_pick_up_seed.loadFromFile("assets/sounds/warden_pick_up_seed.wav");
	m_sound_plant_seed.loadFromFile("assets/sounds/warden_plant_seed.wav");
	m_sound_towerspawn.loadFromFile("assets/sounds/tower_spawn.wav"); // <--- !
	m_sound_powerup1.loadFromFile("assets/sounds/power_up1.wav");
	m_sound_powerup2.loadFromFile("assets/sounds/power_up2.wav");
	
	m_tilemap = tilemap;
	m_damage = 3;
	m_projectilespeed = 1000;
	m_firing = false;
	m_time = m_clock.getElapsedTime();
	m_maxtoweramount = 5;
	m_toweramount = 0;
	m_dead = false;
	m_maxHealth = 30;
	m_healthpoints = m_maxHealth;
	m_poweruphud1 = new PowerUpHud(300, 0, 1);
	entities->push_back(m_poweruphud1);
	m_poweruphud2 = new PowerUpHud(300, 0, 2);
	entities->push_back(m_poweruphud2);
	m_poweruphud3 = new PowerUpHud(300, 0, 3);
	entities->push_back(m_poweruphud3);
	m_healthBar = new PlayerHealthBar(this);
	entities->push_back(m_healthBar); m_entities = entities;
	m_towershop = towershop;
	m_texture.loadFromFile("assets/StaticSprites/WardenIdleForward.png");
	//m_texture = m_texture_manager->CreateTextureFromFile("assets/StaticSprites/WardenIdleForward.png");
	m_sprite.setTexture(m_texture);
	m_x = x;
	m_y = y;
	m_active = true;

	m_collider = new Collider(m_x, m_y, 64, 64);
	m_collider->SetParent(this);

	m_seed_count_type1 = 0;
	m_seed_count_type2 = 0;
	m_seed_count_type3 = 0;

	m_sound_attack.loadFromFile("assets/sounds/warden_attack.wav");
	m_sound_get_hit.loadFromFile("assets/sounds/warden_gets_hit.wav");
	m_sound_death.loadFromFile("assets/sounds/warden_death.wav");
	m_sound_pick_up_seed.loadFromFile("assets/sounds/warden_pick_up_seed.wav");
	m_sound_plant_seed.loadFromFile("assets/sounds/warden_plant_seed.wav");
	m_sound_towerspawn.loadFromFile("assets/sounds/tower_spawn.wav"); // <--- !
	m_sound_powerup1.loadFromFile("assets/sounds/power_up1.wav");
	m_sound_powerup2.loadFromFile("assets/sounds/power_up2.wav");

	m_tilemap = tilemap;
	m_damage = 3;
	m_projectilespeed = 1000;
	m_firing = false;
	m_time = m_clock.getElapsedTime();
	m_maxtoweramount = 5;
	m_toweramount = 0;
	m_dead = false;
	m_healthpoints = 30;
	m_poweruphud1 = new PowerUpHud(300, 0, 1);
	entities->push_back(m_poweruphud1);
	m_poweruphud2 = new PowerUpHud(300, 0, 2);
	entities->push_back(m_poweruphud2);
	m_poweruphud3 = new PowerUpHud(300, 0, 3);
	entities->push_back(m_poweruphud3);
	m_healthBar = new PlayerHealthBar(this);
	entities->push_back(m_healthBar);

	m_seedcounterhud1 = new SeedCounterHud(this, 20.0f, 900.0f, 1);
	entities->push_back(m_seedcounterhud1);
	m_seedcounterhud2 = new SeedCounterHud(this, 20.0f, 900.0f, 2);
	entities->push_back(m_seedcounterhud2);
	m_seedcounterhud3 = new SeedCounterHud(this, 20.0f, 900.0f, 3);
	entities->push_back(m_seedcounterhud3);
	m_seed1_active = false;
	m_seed2_active = false;
	m_seed3_active = false;

	m_seed1_2_active = false;
	m_seed2_2_active = false;
	m_seed3_2_active = false;
	m_key1_pressed = false;
	m_key2_pressed = false;
	m_key3_pressed = false;

}

Player::~Player()
{
	if (m_collider)
		delete m_collider;
}

void Player::Update(float deltatime)
{
	
	/*sf::Time elapsed2;
	sf::Clock clock;
	sf::Time elapsed1 = clock.getElapsedTime();
	clock.restart();*/
	sf::Time elapsed;
	elapsed = m_clock.getElapsedTime();

	if (m_x < 0 && m_dead == false)
	{
		m_x = 0;
	}

	if (m_x > 1880 && m_dead == false)
	{
		m_x = 1880;
	}

	if (m_y < 0 && m_dead == false)
	{
		m_y = 0;
	}

	if (m_y > 1080 && m_dead == false)
	{
		m_y = 1080;
	}
	if (m_dead == true && elapsed.asSeconds() - 10.0f > m_respawntimer.asSeconds())
	{
		Reset();
	}
	if (m_healthpoints <= 0 && m_dead == false)
	{
		SetDead();
		m_sound_death.play();
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W) &&
		!sf::Keyboard::isKeyPressed(sf::Keyboard::S) &&
		!sf::Keyboard::isKeyPressed(sf::Keyboard::A) &&
		!sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		if (AnimationStore::GetActiveAnimation(this) == "wardenwalkleft" ||
			AnimationStore::GetActiveAnimation(this) == "wardenwalkright" ||
			AnimationStore::GetActiveAnimation(this) == "wardenwalkback" ||
			AnimationStore::GetActiveAnimation(this) == "wardenwalkforward")
			AnimationStore::Stop(this);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && sf::Keyboard::isKeyPressed(sf::Keyboard::A) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkleft", true);
		m_x -= 178.0f * deltatime;
		m_y -= 178.0f * deltatime;

	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && sf::Keyboard::isKeyPressed(sf::Keyboard::D) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkright", true);
		m_x += 178.0f * deltatime;
		m_y -= 178.0f * deltatime;

	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && sf::Keyboard::isKeyPressed(sf::Keyboard::A) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkleft", true);
		m_x -= 178.0f * deltatime;
		m_y += 178.0f * deltatime;

	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && sf::Keyboard::isKeyPressed(sf::Keyboard::D) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkright", true);
		m_x += 178.0f * deltatime;
		m_y += 178.0f * deltatime;

	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkback", true);
		m_y -= 250.0f * deltatime;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkforward", true);
		m_y += 250.0f * deltatime;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkleft", true);
		m_x -= 250.0f * deltatime;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && m_dead == false)
	{
		AnimationStore::Play(this, "wardenwalkright", true);
		m_x += 250.0f * deltatime;
	}

	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
	//{
	//	Wave* wave = new Wave(m_entities, 5, 500.0f, 500.0f);
	//}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::I) && m_firing == false && elapsed.asSeconds() - m_attackspeed > m_time.asSeconds())
	{
		m_firing = true;
		fireprojectile(1);
		m_time = m_clock.getElapsedTime();
		//m_poweruphud1->PlayAnimation();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::K) && m_firing == false && elapsed.asSeconds() - m_attackspeed > m_time.asSeconds())
	{
		m_firing = true;
		fireprojectile(2);
		m_time = m_clock.getElapsedTime();
		//m_poweruphud2->PlayAnimation();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::L) && m_firing == false && elapsed.asSeconds() - m_attackspeed > m_time.asSeconds())
	{
		m_firing = true;
		fireprojectile(3);
		m_time = m_clock.getElapsedTime();
		//m_poweruphud3->PlayAnimation();

	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::J) && m_firing == false && elapsed.asSeconds() - m_attackspeed > m_time.asSeconds())
	{
		m_firing = true;
		fireprojectile(4);
		m_time = m_clock.getElapsedTime();

	}

	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1) && m_key1_pressed == false)
	{
		if (m_seed1_active == false)
		{
			m_seed1_active = true;
			m_seed2_2_active = false;
			m_seed3_2_active = false;
		}
		else if (m_seed1_active == true && m_seed1_2_active == false)
		{
			m_seed1_2_active = true;
			m_seed2_active = false;
			m_seed3_active = false;

			m_seed2_2_active = false;
			m_seed3_2_active = false;
		}
		else if (m_seed1_active == true && m_seed1_2_active == true)
		{
			m_seed1_2_active = false;
			m_seed1_active = false;
		}
		m_key1_pressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
	{
		m_key1_pressed = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2) && m_key2_pressed == false)
	{
		if (m_seed2_active == false)
		{
			m_seed2_active = true;
			m_seed1_2_active = false;
			m_seed3_2_active = false;
		}
		else if (m_seed2_active == true && m_seed2_2_active == false)
		{
			m_seed2_2_active = true;
			m_seed1_active = false;
			m_seed3_active = false;

			m_seed1_2_active = false;
			m_seed3_2_active = false;
		}
		else if (m_seed2_active == true && m_seed2_2_active == true)
		{
			m_seed2_2_active = false;
			m_seed2_active = false;
		}
		m_key2_pressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
	{
		m_key2_pressed = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3) && m_key3_pressed == false)
	{
		
		if (m_seed3_active == false)
		{
			m_seed3_active = true;
			m_seed1_2_active = false;
			m_seed2_2_active = false;
		}
		else if (m_seed3_active == true && m_seed3_2_active == false)
		{
			m_seed3_2_active = true;
			m_seed1_active = false;
			m_seed2_active = false;

			m_seed1_2_active = false;
			m_seed2_2_active = false;
		}
		else if (m_seed3_active == true && m_seed3_2_active == true)
		{
			m_seed3_2_active = false;
			m_seed3_active = false;
		}
		m_key3_pressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
	{
		m_key3_pressed = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		
		Tile* tile = &m_tilemap->getTile(sf::Vector2f(m_x, m_y));
		if (tile->state == 0)
		{
			int towernumber;
			towernumber = SeedCombiner::Combine(this, m_seed1_active, m_seed2_active, m_seed3_active, m_seed1_2_active, m_seed2_2_active, m_seed3_2_active);
			
			m_towershop->CreateTower(towernumber, tile->position.x, tile->position.y, tile);
			if (towernumber != 0)
			{
				m_sound_plant_seed.play();
				m_sound_towerspawn.play();

				tile->state = 2;
				m_toweramount += 1;

				m_seed1_active = false;
				m_seed2_active = false;
				m_seed3_active = false;
				m_seed1_2_active = false;
				m_seed2_2_active = false;
				m_seed3_2_active = false;
			}
		}

	}

	for (unsigned int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->GetType() == E_ENEMY)
		{

			//Enemy* enemy = static_cast<Enemy*>(m_entities->at(i));

			//int overlapX = 2;
			//int overlapY = 2;
			//if (enemy->IsActive() == true)
			//{
			//	if (CollisionManager::Check(m_collider, enemy->GetCollider(), overlapX, overlapY) == true)
			//	{

			//		//Deactivate();
			//		//m_x = 50;
			//		//m_y = 500;
			//	}
			//}

		}
		if (m_entities->at(i)->GetType() == E_POWERUP)
		{
			PowerUp* powerup = static_cast<PowerUp*>(m_entities->at(i));
			int overlapX, overlapY;
			if (powerup->IsActive() == true)
			{
				if (CollisionManager::Check(m_collider, powerup->GetCollider(), overlapX, overlapY) == true)
				{
					m_sound_powerup1.play();

					if (powerup->PowerUpType() == 1)
					{
						powerup->Deactivate();
						ActivateStrengthPowerUp();
						m_strengthpowerupactivated = true;
						m_poweruphud1->PlayAnimation();
					}

					if (powerup->PowerUpType() == 2)
					{
						AddHealthOverTime(m_maxHealth, 10);

						for (unsigned int i = 0; i < m_entities->size(); i++)
						{
							if (m_entities->at(i)->IsActive())
							{
								if (m_entities->at(i)->GetType() == E_TOWER)
								{
									Tower* tower = static_cast<Tower*>(m_entities->at(i));
									short towerMaxHP = tower->GetMaxHealth();
									tower->AddHealthOverTime(towerMaxHP, 10);
								}
								else if (m_entities->at(i)->GetType() == E_LIFETREE)
								{
									LifeTree* lifeTree = static_cast<LifeTree*>(m_entities->at(i));
									short lifeTreeMaxHP = lifeTree->GetMaxHealth();
									lifeTree->AddHealthOverTime(lifeTreeMaxHP / 3, 10);
								}
							}
						}
						powerup->Deactivate();
						m_poweruphud3->PlayAnimation();
					}
					if (powerup->PowerUpType() == 3)
					{
						for (unsigned int i = 0; i < m_entities->size(); i++)
						{
							if (m_entities->at(i)->GetType() == E_ENEMY)
							{
								AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
								enemy->ReduceSpeed(100.0f, 10);

							}
						}
						powerup->Deactivate();
						m_poweruphud2->PlayAnimation();

					}
				}
			}
		}
		if (m_entities->at(i)->GetType() == E_SEED)
		{
			Seed* seed = static_cast<Seed*>(m_entities->at(i));
			int overlapX, overlapY;
			if (seed->IsActive() == true)
			{
				if (CollisionManager::Check(m_collider, seed->GetCollider(), overlapX, overlapY) == true)
				{
					switch (seed->getSeedType())
					{
					case SEED_TYPE_1: m_seed_count_type1++;
						break;
					case SEED_TYPE_2: m_seed_count_type2++;
						break;
					case SEED_TYPE_3: m_seed_count_type3++;
						break;
					}

					seed->Deactivate();
					m_sound_pick_up_seed.play();
				}
			}
		}
	}

	if (elapsed.asSeconds() - 10.0f > m_strengthpoweruptimer.asSeconds())
	{
		DeactivateStrengthPowerUp();
	}
	RefreshOverTimeEffects();
	m_firing = false;
	m_sprite.setPosition(m_x, m_y);
	m_collider->Refresh();
	


}
EType Player::GetType(){
	return E_PLAYER;
}
void Player::PlaceTower()
{
	//sf::Texture texture1, texture2;
	//texture1.loadFromFile("assets/StaticSprites/tower1.png");
	//texture2.loadFromFile("assets/StaticSprites/towerProjectile1.png");
//	Tower* tower = new Tower(GetX(), GetY(), m_entities, 1, &texture1, &texture2);
//	m_entities->push_back(tower);
}
void Player::dealdamage(float damage)
{
	/*if (AnimationStore::GetActiveAnimation(m_effectHost) != "fireeffect1")
		AnimationStore::Play(m_effectHost, "", false);*/

	m_sound_get_hit.play();
	m_healthpoints -= damage;
}

void Player::fireprojectile(int direction)
{
	m_sound_attack.play();

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->GetType() == E_PLAYERPROJECTILE)
		{
			if (m_entities->at(i)->IsActive() == false)
			{
				PlayerProjectile* Projectile = static_cast<PlayerProjectile*>(m_entities->at(i));
				Projectile->Reactivate(m_entities, direction, m_damage, m_projectilespeed, GetX() + 32, GetY() + 32);
				return;
			}
		}
	}
	PlayerProjectile* projectile = new PlayerProjectile(m_entities, direction, m_damage, m_projectilespeed, GetX() + 32, GetY() + 32);
	m_entities->push_back(projectile);
}

void Player::ActivateStrengthPowerUp()
{
	if (!m_strengthpowerupactivated)
	{
		m_strengthpoweruptimer = m_clock.getElapsedTime();
		m_attackspeed -= 0.4f;
		m_damage += 1;
		m_strengthpowerupactivated = true;

		m_sound_powerup2.play();
	}
}

void Player::DeactivateStrengthPowerUp()
{
	if (m_strengthpowerupactivated)
	{
		m_attackspeed += 0.4f;
		m_damage -= 1;
		m_strengthpowerupactivated = false;
	}
}

int Player::GetCurrentHealth()
{
	return m_healthpoints;
}
void Player::SubstractHealthOverTime(float totalDamage, int duration)
{
	DOTEffect dOTEffect;

	dOTEffect.m_check = 0;
	dOTEffect.m_totalDamage = totalDamage;
	dOTEffect.m_duration = duration;
	dOTEffect.m_timer.restart();

	m_dOTEffects.push_back(dOTEffect);

	if (AnimationStore::GetActiveAnimation(m_effectHost) != "healingeffect")
		AnimationStore::Play(m_effectHost, "fireeffect1", true);
}

void Player::RefreshOverTimeEffects()
{
	if (m_dOTEffects.size() > 0)
	{
		for (int i = 0; i < m_dOTEffects.size(); i++)
		{
			if (m_dOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_dOTEffects[i].m_check + 1))
			{
				float tickDamage = m_dOTEffects[i].m_totalDamage / m_dOTEffects[i].m_duration;
				m_healthpoints -= tickDamage;
				if (m_healthpoints <= 0)
					m_healthpoints = 0;

				m_dOTEffects[i].m_check++;

				if (m_dOTEffects[i].m_check >= m_dOTEffects[i].m_duration)
					m_dOTEffects.erase(m_dOTEffects.begin() + i);
			}
		}
		if (m_dOTEffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}
	if (m_hOTEffects.size() > 0)
	{
		for (int i = 0; i < m_hOTEffects.size(); i++)
		{
			if (m_hOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_hOTEffects[i].m_check + 1))
			{
				float tickHealing = m_hOTEffects[i].m_totalHealing / m_hOTEffects[i].m_duration;
				AddHealth(tickHealing);
				m_hOTEffects[i].m_check++;

				if (m_hOTEffects[i].m_check >= m_hOTEffects[i].m_duration)
					m_hOTEffects.erase(m_hOTEffects.begin() + i);
			}
		}
		if (m_hOTEffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}

}

void Player::Reset()
{
	m_x = 200;
	m_y = 500;
	m_active = true;
	m_healthpoints = 30;
	m_dead = false;
}

void Player::SetDead()
{
	if (m_dead == false)
	{
		m_x = -100;
		m_y = -100;
		m_respawntimer = m_clock.getElapsedTime();
	}

	AnimationStore::Stop(m_effectHost);
	m_hOTEffects.clear();
	m_dOTEffects.clear();
	m_dead = true;
}

bool Player::Getm_seed1_active()
{
	return m_seed1_active;
}
bool Player::Getm_seed2_active()
{
	return m_seed2_active;
}
bool Player::Getm_seed3_active()
{
	return m_seed3_active;
}

bool Player::Getm_seed1_2_active()
{
	return m_seed1_2_active;
}
bool Player::Getm_seed2_2_active()
{
	return m_seed2_2_active;
}
bool Player::Getm_seed3_2_active()
{
	return m_seed3_2_active;
}

int Player::GetSeed1()
{
	return m_seed_count_type1;
}
int Player::GetSeed2()
{
	return m_seed_count_type2;
}
int Player::GetSeed3()
{
	return m_seed_count_type3;
}


void Player::DecreaseSeed1()
{
	m_seed_count_type1 -= 1;
}
void Player::DecreaseSeed2()
{
	m_seed_count_type2--;
}
void Player::DecreaseSeed3()
{
	m_seed_count_type3--;
}

int Player::GetTime()
{
	return m_clock.getElapsedTime().asSeconds();
}
void Player::AddHealth(float value)
{
	m_healthpoints += value;
	if (m_healthpoints > m_maxHealth)
		m_healthpoints = m_maxHealth;
}
void Player::AddHealthOverTime(float totalHealing, int duration)
{
	HOTEffect hOTEffect;

	hOTEffect.m_check = 0;
	hOTEffect.m_totalHealing = totalHealing;
	hOTEffect.m_duration = duration;
	hOTEffect.m_timer.restart();

	m_hOTEffects.push_back(hOTEffect);

	AnimationStore::Play(m_effectHost, "healingeffect", true);
}