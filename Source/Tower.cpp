// Tower.cpp

#include "stdafx.h"
#include "Tower.h"
#include "HProjectile.h"
#include "PProjectile.h"
#include "AttributeManager.h"
#include "Powerup.h"
#include "AnimationStore.h"
#include "DOTEffect.h"
#include "HOTEffect.h"
#include "EffectHost.h"
#include "TileMap.h"
// 1: Small Range 2:Small Defense 3: Small Magic 4: Range Range 5: Range Defense 6: Range Magic 7: Defense Defense 8: Defense Magic 9: Magic Magic

Tower::Tower(int x, int y, std::vector<Entity*>* entities, int towerType, sf::Texture* towerTexture, sf::Texture* projectileTexture, Tile* tile)
{
	m_x = x;
	m_y = y;
	m_w = 64;
	m_h = 64;
	m_active = false;
	m_target = nullptr;
	m_sprite.setTexture(*towerTexture);
	m_projectileTexture = projectileTexture;
	m_entities = entities;
	SetTowerType(towerType);
	m_currentHealth = m_maxHealth;
	m_despawned = true;
	m_spawned = false;

	m_tile = tile;

	m_soundAttack.loadFromFile("assets/sounds/tower_attack.wav");
	m_soundDeath.loadFromFile("assets/sounds/tower_death.wav");
	m_soundSpawn.loadFromFile("assets/sounds/tower_spawn.wav");

	PreGenerateProjectiles();
	PreGeneratePowerup();

	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);
	m_effectHost = new EffectHost(this);
	m_entities->push_back(m_effectHost);
	m_healthBar = new HealthBar(this);
	entities->push_back(m_healthBar);
}

Tower::~Tower()
{
	if (m_collider)
		delete m_collider;
}
void Tower::Update(float deltatime)
{
	if (m_active == true)
	{
		if (m_spawnTimer.getElapsedTime().asMilliseconds() >= 1000 && 
			m_spawned == false &&
			m_despawned == false)
			Spawn();

		if (m_spawned == true &&
			m_despawned == false)
		{
			if (m_currentHealth <= 0)
			{
				if (rand() % 2 == 1)
					SpawnPowerUp();

				Deactivate();
				return;
			}
			if (m_towerType != 2 &&
				m_towerType != 5 &&
				m_towerType != 7)
			{
				if (!m_target)
				{
					m_target = GetClosestEnemy();
				}
				if (m_target)
				{
					if (m_target->IsActive() == false || m_radius < GetPixelDistance(m_target))
						m_target = nullptr;

					else if (m_attackTimer.getElapsedTime().asMilliseconds() > m_attackSpeed)
					{
						Attack();
						m_attackTimer.restart();
					}
				}
			}
			RefreshOverTimeEffects();

			m_collider->Refresh();
			m_sprite.setPosition(m_x, m_y);
		}
		if (m_despawnTimer.getElapsedTime().asMilliseconds() >= 1000 &&
			m_despawned == true &&
			m_spawned == true)
			Despawn();
	}
}

EType Tower::GetType()
{
	return E_TOWER;
}
int Tower::GetRadius()
{
	return m_radius;
}
void Tower::SetRadius(int radius)
{
	m_radius = radius;
}
int Tower::GetAttackSpeed()
{
	return m_attackSpeed;
}
void Tower::SetAttackSpeed(int attackSpeed)
{
	m_attackSpeed = attackSpeed;
}
Entity* Tower::GetClosestEnemy() // Returns closest active enemy entity (pixeldistance)
{
	Entity* enemyEnt = nullptr;

	for (int i = 0; i < m_entities->size(); i++)
	{
		Entity* entity = m_entities->at(i);

		if (entity->GetType() == E_ENEMY)
		{
			if (entity->IsActive()==true)
			{
				int distance = GetPixelDistance(entity);

				if (distance < m_radius)
				{
					if (!enemyEnt)					
						enemyEnt = m_entities->at(i);

					else if (GetPixelDistance(enemyEnt)>distance)
						enemyEnt = m_entities->at(i);
				}
				
			}
		}
	}	
	return enemyEnt;
}
void Tower::Attack()
{
	if (m_projectileType >= 1 && m_projectileType < 6)
	{
		for (int i = 0; i < m_entities->size(); i++)
		{
			if (m_entities->at(i)->GetType() == E_HPROJECTILE)
			{
				if (m_entities->at(i)->IsActive() == false)
				{
					HProjectile* hProjectile = static_cast<HProjectile*>(m_entities->at(i));
					hProjectile->Reactivate(m_target, m_projectileTexture, m_projectileType, (m_x + (GetW() / 2)), (m_y + (GetH() / 2)));
					AnimateAttack();
					return;
				}
			}
		}
	}
	else if (m_projectileType == 6)
	{
		for (int i = 0; i < m_entities->size(); i++)
		{
			if (m_entities->at(i)->GetType() == E_PPROJECTILE)
			{
				if (m_entities->at(i)->IsActive() == false)
				{
					PProjectile* pProjectile = static_cast<PProjectile*>(m_entities->at(i));
					pProjectile->Reactivate(m_target, (m_x + (GetW() / 2)), (m_y + (GetH() / 2)));
					AnimateAttack();
					return;
				}
			}
		}
	}
}
int Tower::GetPixelDistance(Entity* entity)
{
	int hostCenterX = m_x + (GetW() / 2);
	int hostCenterY = m_y + (GetH() / 2);

	int unitCenterX = entity->GetX() + (entity->GetW() / 2);
	int unitCenterY = entity->GetY() + (entity->GetH() / 2);

	int distanceX = abs(hostCenterX - unitCenterX);
	int distanceY = abs(hostCenterY - unitCenterY);

	int distance = sqrt((distanceX * distanceX) + (distanceY * distanceY));

	return distance;
}
void Tower::SubstractHealth(float value)
{
	m_currentHealth -= value;
	if (m_currentHealth < 1)
		m_currentHealth = 0;
}
void Tower::AddHealth(float value)
{
	m_currentHealth += value;

	if (m_currentHealth > m_maxHealth)
		m_currentHealth = m_maxHealth;
}
void Tower::SetTowerType(int towerType) // 1: Small Range 2:Small Defense 3: Small Magic 4: Range Range 5: Range Defense 6: Range Magic 7: Defense Defense 8: Defense Magic 9: Magic Magic
{
	if (towerType < 1)
		m_towerType = 1;
	else if (towerType > 9)
		m_towerType = 1;
	else
		m_towerType = towerType;

	switch (m_towerType) // 1: small range(damage) 2: small magic(damage) 3: large range(high damage) 4: DoT 5: Slow 
	{
	case(1) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_shooter_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_shooter_AS");
		m_radius = AttributeManager::GetAttribute("Tower_shooter_Range");
		m_projectileType = 1;
		break;
	}
	case(2) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_defense_HP");
		break;
	}
	case(3) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_magic_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_magic_AS");
		m_radius = AttributeManager::GetAttribute("Tower_magic_Range");
		m_projectileType = 2;
		break;
	}
	case(4) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_rangerange_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_rangerange_AS");
		m_radius = AttributeManager::GetAttribute("Tower_rangerange_Range");
		m_projectileType = 3;
		break;
	}
	case(5) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_rangedefense_HP");
		break;
	}
	case(6) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_rangemagic_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_rangemagic_AS");
		m_radius = AttributeManager::GetAttribute("Tower_rangemagic_Range");
		m_projectileType = 4;
		break;
	}
	case(7) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_defensedefense_HP");
		break;
	}
	case(8) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_defensemagic_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_defensemagic_AS");
		m_radius = AttributeManager::GetAttribute("Tower_defensemagic_Range");
		m_projectileType = 5;
		break;
	}
	case(9) :
	{
		m_maxHealth = AttributeManager::GetAttribute("Tower_magicmagic_HP");
		m_attackSpeed = AttributeManager::GetAttribute("Tower_magicmagic_AS");
		m_radius = AttributeManager::GetAttribute("Tower_magicmagic_Range");
		m_projectileType = 6;
		break;
	}
	}
}
void Tower::Deactivate()
{
	m_despawned = true;
	m_tile->state = false;
	m_despawnTimer.restart();
	m_soundDeath.play();
	m_dOTEffects.clear();
	m_hOTEffects.clear();
	AnimationStore::Stop(m_effectHost);

	m_sprite.setPosition(sf::Vector2f{ -100, -100 });
	switch (m_towerType)
	{
	case(1) :
	{
		AnimationStore::Play(m_effectHost, "tower1death", false);
		break;
	}
	case(2) :
	{
		AnimationStore::Play(m_effectHost, "tower2death", false);
		break;
	}
	case(3) :
	{
		AnimationStore::Play(m_effectHost, "tower3death", false);
		break;
	}
	case(4) :
	{
		AnimationStore::Play(m_effectHost, "tower4death", false);
		break;
	}
	case(5) :
	{
		AnimationStore::Play(m_effectHost, "tower5death", false);
		break;
	}
	case(6) :
	{
		AnimationStore::Play(m_effectHost, "tower6death", false);
		break;
	}
	case(7) :
	{
		AnimationStore::Play(m_effectHost, "tower7death", false);
		break;
	}
	case(8) :
	{
		AnimationStore::Play(m_effectHost, "tower8death", false);
		break;
	}
	case(9) :
	{
		AnimationStore::Play(m_effectHost, "tower9death", false);
		break;
	}
	}
}
void Tower::Reactivate(int x, int y, int towerType, sf::Texture* towerSprite, sf::Texture* projectileTexture, Tile* tile)
{
	if (m_active == false)
	{
		m_tile = tile;
		m_active = true;
		m_despawned = false;
		m_x = x;
		m_y = y;
		m_spawnTimer.restart();
		AnimationStore::Play(m_effectHost, "towerspawn1", false);
		m_sprite.setPosition(sf::Vector2f{ -100, -100 });
		SetTowerType(towerType);
		m_currentHealth = m_maxHealth;
		m_sprite.setTexture(*towerSprite);
		m_projectileTexture = projectileTexture;
		m_healthBar->SetReferenceHealthValue(m_maxHealth);
	}
}
int Tower::GetCurrentHealth()
{
	return m_currentHealth;
}
int Tower::GetMaxHealth()
{
	return m_maxHealth;
}
void Tower::PreGenerateProjectiles()
{
	for (int i = 0; i < 10; i++)
	{
		HProjectile* hProjectile = new HProjectile(nullptr, m_projectileTexture, -100, -100, 1);
		m_entities->push_back(hProjectile);
	}
	for (int i = 0; i < 5; i++)
	{
		PProjectile* pProjectile = new PProjectile(nullptr, m_entities, m_projectileTexture, -100, -100);
		m_entities->push_back(pProjectile);
	}
}
void Tower::SubstractHealthOverTime(float totalDamage, int duration)
{
	DOTEffect dOTEffect;

	dOTEffect.m_check = 0;
	dOTEffect.m_totalDamage = totalDamage;
	dOTEffect.m_duration = duration;
	dOTEffect.m_timer.restart();

	m_dOTEffects.push_back(dOTEffect);

	if (AnimationStore::GetActiveAnimation(m_effectHost) != "towerspawn1"||
		AnimationStore::GetActiveAnimation(m_effectHost) != "healingeffect")
		AnimationStore::Play(m_effectHost, "fireeffect1", true);
}
void Tower::AddHealthOverTime(float totalHealing, int duration)
{
	HOTEffect hOTEffect;

	hOTEffect.m_check = 0;
	hOTEffect.m_totalHealing = totalHealing;
	hOTEffect.m_duration = duration;
	hOTEffect.m_timer.restart();

	m_hOTEffects.push_back(hOTEffect);

	if (!m_despawned)
		AnimationStore::Play(m_effectHost, "healingeffect", true);
}
void Tower::RefreshOverTimeEffects()
{
	if (m_dOTEffects.size() > 0)
	{
		for (int i = 0; i < m_dOTEffects.size(); i++)
		{
			if (m_dOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_dOTEffects[i].m_check + 1))
			{
				float tickDamage = m_dOTEffects[i].m_totalDamage / m_dOTEffects[i].m_duration;
				SubstractHealth(tickDamage);
				m_dOTEffects[i].m_check++;

				if (m_dOTEffects[i].m_check >= m_dOTEffects[i].m_duration)
					m_dOTEffects.erase(m_dOTEffects.begin() + i);
			}
		}
		if (m_dOTEffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}

	if (m_hOTEffects.size() > 0)
	{
		for (int i = 0; i < m_hOTEffects.size(); i++)
		{
			if (m_hOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_hOTEffects[i].m_check + 1))
			{
				float tickHealing = m_hOTEffects[i].m_totalHealing / m_hOTEffects[i].m_duration;
				AddHealth(tickHealing);
				m_hOTEffects[i].m_check++;

				if (m_hOTEffects[i].m_check >= m_hOTEffects[i].m_duration)
					m_hOTEffects.erase(m_hOTEffects.begin() + i);
			}
		}
		if (m_hOTEffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}
}
int Tower::GetTowerType() // 1: Small Range 2:Small Defense 3: Small Magic 4: Range Range 5: Range Defense 6: Range Magic 7: Defense Defense 8: Defense Magic 9: Magic Magic
{
	return m_towerType;
}

void Tower::SpawnPowerUp()
{
	m_randtype = rand() % 3 + 1;
	if (rand() % 100 > 50)
	{
		for (int i = 0; i < m_entities->size(); i++)
		{
			if (m_entities->at(i)->GetType() == E_POWERUP)
			{
				if (m_entities->at(i)->IsActive() == false)
				{

					PowerUp* powerup = static_cast<PowerUp*>(m_entities->at(i));
					powerup->Reactivate(m_randtype, GetX(), GetY());
					return;
				}
			}
		}
	}
}
void Tower::PreGeneratePowerup()
{
	PowerUp* powerup = new PowerUp(m_entities, m_randtype, GetX(), GetY());
	powerup->Deactivate();
	m_entities->push_back(powerup);
}
void Tower::AnimateAttack()
{
	switch (m_towerType)
	{
	case (1) :
	{
		AnimationStore::Play(this, "tower1attack", false);
		break;
	}
	case (3) :
	{
		AnimationStore::Play(this, "tower3attack", false);
		break;
	}
	case (4) :
	{
		AnimationStore::Play(this, "tower4attack", false);
		break;
	}
	case (6) :
	{
		AnimationStore::Play(this, "tower6attack", false);
		break;
	}
	case (8) :
	{
		AnimationStore::Play(this, "tower8attack", false);
		break;
	}
	case (9) :
	{
		AnimationStore::Play(this, "tower9attack", false);
		break;
	}
	default:
		return;
	}
}
void Tower::Spawn()
{
	m_sprite.setPosition(m_x, m_y);
	m_spawned = true;
	AnimationStore::Play(m_effectHost, "towerspawn2", false);
}
void Tower::Despawn()
{
	m_active = false;
	m_spawned = false;
	AnimationStore::Stop(m_effectHost);
}