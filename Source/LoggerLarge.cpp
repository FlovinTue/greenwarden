// LoggerLarge.cpp

#include "stdafx.h"
#include "LoggerLarge.h"

LoggerLarge::LoggerLarge(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder)
{
	m_mainTarget = mainTarget;
	m_x = x;
	m_y = y;
	m_sprite.setTexture(*unitTexture);
	m_entities = entities;
	m_pathfinder = pathfinder;

	Initialize();

	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);
	HealthBar* healthBar = new HealthBar(this);
	entities->push_back(healthBar);
}
LoggerLarge::~LoggerLarge()
{
	if (m_collider)
		delete m_collider;
}
SubType LoggerLarge::GetSubType()
{
	return E_LOGGERL;
}
void LoggerLarge::Deactivate()
{
	m_dOTEffects.clear();
	m_active = false;
	m_effectHost->PlayEffect("loggerlargedeath", false);
}
void LoggerLarge::Reactivate(int x, int y)
{
	m_attackTimer.restart();
	m_tempTarget = nullptr;
	m_cleaveTarget = nullptr;
	m_slowed = false;
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_currentHealth = m_maxHealth;

	m_x = x;
	m_y = y;
	m_sprite.setPosition(m_x, m_y);

	AssignPath();

	m_active = true;
}
void LoggerLarge::Initialize()
{
	m_w = 64;
	m_h = 64;
	m_tempTarget = nullptr;
	m_cleaveTarget = nullptr;
	m_attackTimer.restart();
	m_slowed = false;
	m_attackSpeed = AttributeManager::GetAttribute("Enemy_loggerlarge_AS");
	m_movementSpeed = AttributeManager::GetAttribute("Enemy_loggerlarge_movement");
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_directionX = 0.0f;
	m_directionY = 0.0f;
	m_slowStop = 4;
	m_detectRadius = AttributeManager::GetAttribute("Enemy_loggerlarge_detectRange");
	m_attackRange = AttributeManager::GetAttribute("Enemy_loggerlarge_range");
	m_damage = AttributeManager::GetAttribute("Enemy_loggerlarge_damage");
	m_maxHealth = AttributeManager::GetAttribute("Enemy_loggerlarge_HP");
	m_currentHealth = m_maxHealth;
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);

	m_active = false;
}
void LoggerLarge::Attack()
{
	if (m_tempTarget)
	{
		if (m_tempTarget->GetType() == E_TOWER)
		{
			Tower* tower = static_cast<Tower*>(m_tempTarget);
			if (tower->GetTowerType() == 5)
				SubstractHealth(AttributeManager::GetAttribute("Tower_rangedefense_damage"));
			tower->SubstractHealth(m_damage);
			m_attackTimer.restart();
		}
		else if (m_tempTarget->GetType() == E_PLAYER)
		{
			Player* player = static_cast<Player*>(m_tempTarget);
			player->dealdamage(m_damage);
			m_attackTimer.restart();
		}
	}
	else if (m_mainTarget)
	{
		if (m_mainTarget->GetType() == E_LIFETREE)
		{
			LifeTree* lifeTree = static_cast<LifeTree*>(m_mainTarget);
			lifeTree->SubstractHealth(m_damage);
			m_attackTimer.restart();
		}
	}

	ScanForCleaveTarget();

	if (m_cleaveTarget)
	{
		switch (m_cleaveTarget->GetType())
		{
		case (E_TOWER) :
		{
			Tower* tower = static_cast<Tower*>(m_cleaveTarget);
			tower->SubstractHealth(m_damage);
			break;
		}
		case(E_PLAYER) :
		{
			Player* player = static_cast<Player*>(m_cleaveTarget);
			player->dealdamage(m_damage);
			break;
		}
		case(E_LIFETREE) :
		{
			LifeTree* lifeTree = static_cast<LifeTree*>(m_cleaveTarget);
			lifeTree->SubstractHealth(m_damage);
			break;
		}
		default:
			break;
		}

		m_cleaveTarget = nullptr;
	}

	AnimateAttack();
}
void LoggerLarge::ScanForCleaveTarget()
{
	Entity* baseTarget = nullptr;

	if (m_tempTarget)
		baseTarget = m_tempTarget;
	else
		baseTarget = m_mainTarget;

	std::vector<Entity*> potentialTargets;

	for (int i = 0; i < m_entities->size(); i++)
	{
		if (m_tempTarget && m_entities->at(i) == m_tempTarget ||
			!m_tempTarget && m_entities->at(i) == m_mainTarget)
			continue;


		if (m_entities->at(i)->IsActive())
			if (m_entities->at(i)->GetType() == E_TOWER ||
				m_entities->at(i)->GetType() == E_PLAYER ||
				m_entities->at(i)->GetType() == E_LIFETREE)
			{
				sf::Vector2f cleaveTargetPos{ m_entities->at(i)->GetX(), m_entities->at(i)->GetY() };
				if (GetPixelDistance(cleaveTargetPos) < (m_attackRange * 1.5f))
				{
					if ((abs(m_entities->at(i)->GetX() - baseTarget->GetX()) < (m_entities->at(i)->GetW()*1.5)) &&
						(abs(m_entities->at(i)->GetY() - baseTarget->GetY()) < (m_entities->at(i)->GetH()*1.5)))
					{
						potentialTargets.push_back(m_entities->at(i));

						if (potentialTargets.size() > 5)
							break;
					}

				}
			}
	}

	if (potentialTargets.size() > 0)
		m_cleaveTarget = potentialTargets[rand() % potentialTargets.size()];
}
void LoggerLarge::AnimateMovement()
{
	if (m_directionX < 0 && abs(m_directionX) + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "loggerlargewalkleft", true);
	else if (m_directionX > 0 && m_directionX + 0.01f> abs(m_directionY))
		AnimationStore::Play(this, "loggerlargewalkright", true);
	else if (m_directionY < 0 && abs(m_directionY)> abs(m_directionX))
		AnimationStore::Play(this, "loggerlargewalkbackward", true);
	else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		AnimationStore::Play(this, "loggerlargewalkforward", true);
}
void LoggerLarge::AnimateAttack()
{
	if (m_directionX < 0 && abs(m_directionX)>abs(m_directionY))
		AnimationStore::Play(this, "loggerlargeattackleft", false);
	else if (m_directionX > 0 && m_directionX > abs(m_directionY))
		AnimationStore::Play(this, "loggerlargeattackright", false);
	else if (m_directionY < 0 && abs(m_directionY)>abs(m_directionX))
		AnimationStore::Play(this, "loggerlargeattackbackward", false);
	else if (m_directionY > 0 && m_directionY > abs(m_directionX))
		AnimationStore::Play(this, "loggerlargeattackforward", false);
}