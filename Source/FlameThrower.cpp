// FlameThrower.cpp

#include "stdafx.h"
#include "FlameThrower.h"

FlameThrower::FlameThrower(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder)
{
	m_mainTarget = mainTarget;
	m_x = x;
	m_y = y;
	m_sprite.setTexture(*unitTexture);
	m_entities = entities;
	m_pathfinder = pathfinder;

	Initialize();

	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);
	HealthBar* healthBar = new HealthBar(this);
	entities->push_back(healthBar);
}
FlameThrower::~FlameThrower()
{
	if (m_collider)
		delete m_collider;
}
SubType FlameThrower::GetSubType()
{
	return E_FLAMETHROWER;
}
void FlameThrower::Reactivate(int x, int y)
{
	m_attackTimer.restart();
	m_tempTarget = nullptr;
	m_slowed = false;
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_currentHealth = m_maxHealth;

	m_x = x;
	m_y = y;

	AssignPath();

	m_active = true;

}
void FlameThrower::Deactivate()
{
	m_dOTEffects.clear();
	m_active = false;
}
void FlameThrower::Initialize()
{
	m_w = 64;
	m_h = 64;
	m_tempTarget = nullptr;
	m_attackTimer.restart();
	m_slowed = false;
	m_attackSpeed = AttributeManager::GetAttribute("Enemy_flamethrower_AS");
	m_movementSpeed = AttributeManager::GetAttribute("Enemy_flamethrower_movement");
	m_movementSpeedFactor = 1.0f;
	m_attackSpeedFactor = 1.0f;
	m_directionX = 0.0f;
	m_directionY = 0.0f;
	m_slowStop = 4;
	m_detectRadius = AttributeManager::GetAttribute("Enemy_flamethrower_detectRange");
	m_attackRange = AttributeManager::GetAttribute("Enemy_flamethrower_range");
	m_damage = AttributeManager::GetAttribute("Enemy_flamethrower_damage");
	m_maxHealth = AttributeManager::GetAttribute("Enemy_flamethrower_HP");
	m_effectDuration = AttributeManager::GetAttribute("Enemy_flamethrower_effectDuration");
	m_currentHealth = m_maxHealth;
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);

	m_active = false;
}
void FlameThrower::Attack()
{
	if (m_tempTarget)
	{
		if (m_tempTarget->GetType() == E_TOWER)
		{
			Tower* tower = static_cast<Tower*>(m_tempTarget);
			tower->SubstractHealthOverTime(m_damage, m_effectDuration);
			m_attackTimer.restart();
		}
		else if (m_tempTarget->GetType() == E_PLAYER)
		{
			Player* player = static_cast<Player*>(m_tempTarget);
			player->SubstractHealthOverTime(m_damage, m_effectDuration);
			m_attackTimer.restart();
		}
	}
	else if (m_mainTarget)
	{
		if (m_mainTarget->GetType() == E_LIFETREE)
		{
			LifeTree* lifeTree = static_cast<LifeTree*>(m_mainTarget);
			// lifeTree->SubstractHealthOverTime(m_damage, m_effectDuration);
			m_attackTimer.restart();
		}
	}

	AnimateAttack();
}
void FlameThrower::AnimateMovement()
{
	//if (m_directionX < 0 && abs(m_directionX)> abs(m_directionY))
	//	AnimationStore::Play(this, "FlameThrowerwalkleft", true);
	//else if (m_directionX > 0 && m_directionX > abs(m_directionY))
	//	AnimationStore::Play(this, "FlameThrowerwalkright", true);
}
void FlameThrower::AnimateAttack()
{

}