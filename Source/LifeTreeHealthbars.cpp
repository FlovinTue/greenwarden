// HealthBar.cpp

#include "stdafx.h"
#include "LifeTreeHealthbars.h"
#include "Tower.h"
#include "AbstractEnemy.h"
#include "LifeTree.h"
#include "Player.h"

LifetreeHealthBar::LifetreeHealthBar(Entity* host)
{
	m_active = true;
	m_host = host;
	m_referenceHealthValue = GetHostCurrentHealth();
	m_healthBarOffset = 5;
	
	//sf::Texture rectangle;
	m_texture.loadFromFile("assets/StaticSprites/LifeTreeHealthBAR.png");
	m_sprite.setTexture(m_texture);
	
	
	m_sprite.setPosition(60, 500);
	RefreshBar(GetHostCurrentHealth());
	m_collider = nullptr;
}
LifetreeHealthBar::~LifetreeHealthBar()
{
}
void LifetreeHealthBar::Update(float deltatime)
{
	if (m_host->IsActive())
		RefreshBar(GetHostCurrentHealth());
}
EType LifetreeHealthBar::GetType()
{
	return E_HUD;
}
void LifetreeHealthBar::SetReferenceHealthValue(int referenceHealthValue)
{
	m_referenceHealthValue = referenceHealthValue;
}
void LifetreeHealthBar::RefreshBar(int currentHealthValue)
{
	//m_x = m_host->GetX();
	//m_y = m_host->GetSprite()->getPosition().y - GetH() - m_healthBarOffset;

	float health = currentHealthValue;

	if (health < 0){
		health = 0;
		m_active = false;
	}
		

	float healthRatio = health / m_referenceHealthValue;

	int red, green, blue, alpha;

	red = (1 - healthRatio) * 255;
	green = healthRatio * 255;
	blue = 0;
	alpha = 255;

	int barWidth = 233 * healthRatio;

	//m_sprite.setTexture(m_te)
	m_sprite.setTextureRect(sf::IntRect(0, 0, 45, barWidth));
	m_sprite.setRotation(180);

}
int LifetreeHealthBar::GetHostCurrentHealth()
{

	LifeTree* lifetree = static_cast<LifeTree*>(m_host);
	return lifetree->GetCurrentHealth();

}