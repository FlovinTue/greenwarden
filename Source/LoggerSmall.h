// LoggerSmall.h

#pragma once
#include "AbstractEnemy.h"

class LoggerSmall : public AbstractEnemy
{
public:
	LoggerSmall(std::vector<Entity*>* entities, sf::Texture* unitTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder);
	~LoggerSmall();

	SubType GetSubType();
	void Deactivate();
	void Reactivate(int x, int y);

private:
	void AnimateMovement();
	void AnimateAttack();
	void Initialize();
	void Attack();
};