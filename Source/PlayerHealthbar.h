// HealthBar.h

#pragma once

#include "Entity.h"

class PlayerHealthBar : public Entity
{
public:
	PlayerHealthBar(Entity* host);
	~PlayerHealthBar();

	void Update(float deltatime);
	EType GetType();

	void SetReferenceHealthValue(int referenceHealthValue);
private:
	void RefreshBar(int currentHealthValue);
	int GetHostCurrentHealth();

private:
	float m_referenceHealthValue;
	int m_healthBarOffset;
	sf::Texture m_texture;
	Entity* m_host;
};