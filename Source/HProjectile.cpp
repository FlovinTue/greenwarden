// HProjectile.cpp

#include "stdafx.h"
#include "HProjectile.h"
#include "CollisionManager.hpp"
#include "AttributeManager.h"
#include "AbstractEnemy.h"

HProjectile::HProjectile(Entity* target, sf::Texture* texture, int x, int y, int projectileType)
{
	m_x = x;
	m_y = y;
	m_w = 16;
	m_h = 16;
	m_target = target;
	m_sprite.setTexture(*texture);
	m_effectDuration = 0;
	m_slowPercentage = 0;
	m_damage = 0;
	m_speed = AttributeManager::GetAttribute("Projectile_global_speed");
	m_active = false;
	SetProjectileType(projectileType);	// 1: small range(damage) 2: small magic(damage) 3: large range(high damage) 4: DoT 5: Slow 
	m_collider = new Collider(m_x, m_y, GetW(), GetH());
	m_collider->SetParent(this);
}
HProjectile::~HProjectile()
{
	if (m_collider)
		delete m_collider;
}
void HProjectile::Update(float deltatime)
{
	if (!m_target)
		return;

		if (m_target->IsActive() == true && IsActive() == true)
		{
			HomeOnTarget(deltatime);

			int overlapX = 2;
			int overlapY = 2;
			if (CollisionManager::Check(m_collider, m_target->GetCollider(), overlapX, overlapY))
			{
				ApplyProjectileEffect();
				Deactivate();
			}

			m_collider->Refresh();
			m_sprite.setPosition(m_x, m_y);
			m_sprite.setRotation(m_spriteRotation);
		}
		else if (m_target->IsActive() == false)
		{
			Deactivate();
		}

}

EType HProjectile::GetType()
{
	return E_HPROJECTILE;
}
float HProjectile::GetSpeed()
{
	return m_speed;
}
void HProjectile::SetSpeed(float speed)
{
	m_speed = speed;
}
int HProjectile::GetDamage()
{
	return m_damage;
}
void HProjectile::SetDamage(int damage)
{
	m_damage = damage;
}
Entity* HProjectile::GetTarget()
{
	return m_target;
}
void HProjectile::SetTarget(Entity* target)
{
	m_target = target;
}
void HProjectile::HomeOnTarget(float deltatime)
{
	float distX = (m_target->GetX() + (m_target->GetW() / 2)) - (GetX() + (GetW() / 2));
	float distY = (m_target->GetY() + (m_target->GetH() / 2)) - (GetY() + (GetH() / 2));
	float units = 1 / (abs(distX) + abs(distY));
	float directionX = (distX * units);
	float directionY = (distY * units);

		m_x += directionX * m_speed * deltatime;
		m_y += directionY * m_speed * deltatime;

		// SpriteRotation
		short spriteRotation;

		if (directionX > 0)
		{
			if (directionY < 0)
				spriteRotation = directionX * 90.0f;
			else if (directionY > 0)
				spriteRotation = (directionY * 90.0f) + 90.0f;
		}
		else if (directionX < 0)
		{
			if (directionY > 0)
			spriteRotation = abs(directionX * 90.0f) + 180.0f;
			else if (directionY < 0)
				spriteRotation = abs(directionY * 90.0f) + 270.0f;
		}
		short rotationOffset = spriteRotation + 270; // Offset

		m_spriteRotation = rotationOffset % 360;
}
void HProjectile::ApplyProjectileEffect()	 
{
	AbstractEnemy* abstractEnemy = static_cast<AbstractEnemy*>(m_target);

	switch (m_projectileType)
	{
	case(1) :
		abstractEnemy->SubstractHealth(m_damage);
		break;
	case(2) :
		abstractEnemy->SubstractHealth(m_damage);
		break;
	case(3) :
		abstractEnemy->SubstractHealth(m_damage);
		break;
	case(4) :
		abstractEnemy->SubstractHealthOverTime(m_damage, m_effectDuration);
		break;
	case(5) :
		abstractEnemy->ReduceSpeed(m_slowPercentage, m_effectDuration);
		break;
	}
}
void HProjectile::SetProjectileType(int projectileType) // 1: small range(damage) 2 : small magic(damage) 3 : large range(high damage) 4 : DoT 5 : Slow 6 : Piercing
{
	if (projectileType <= 0)
		m_projectileType = 1;
	else if (projectileType > 5)
		m_projectileType = 1;
	else
		m_projectileType = projectileType;
	
	switch (m_projectileType)
	{
	case(1) :
	{
		m_damage = AttributeManager::GetAttribute("Projectile_shooter_damage");
		break;
	}
	case(2) :
	{
		m_damage = AttributeManager::GetAttribute("Projectile_magic_damage");
		break;
	}
	case(3) :
	{
		m_damage = AttributeManager::GetAttribute("Projectile_rangerange_damage");
		break;
	}
	case(4) :
	{
		m_damage = AttributeManager::GetAttribute("Projectile_rangemagic_damage");
		m_effectDuration = AttributeManager::GetAttribute("Projectile_rangemagic_duration");
		break;
	}
	case(5) :
	{
		m_effectDuration = AttributeManager::GetAttribute("Projectile_defensemagic_duration");
		m_slowPercentage = AttributeManager::GetAttribute("Projectile_defensemagic_percentage");
		break;
	}

	}
}
void HProjectile::Reactivate(Entity* target, sf::Texture* texture, int projectileType, int x, int y)
{
	if (m_active == false)
	{
		m_active = true;
		m_target = target;
		m_x = x;
		m_y = y;
		m_collider->Refresh();

		if (projectileType != m_projectileType)
		{
			m_sprite.setTexture(*texture);

			if (projectileType < 1)
				m_projectileType = 1;
			else if (projectileType > 5)
				m_projectileType = 1;
			else
				m_projectileType = projectileType;

			SetProjectileType(m_projectileType);
		}
		if (m_projectileType == 5)
			AnimationStore::Play(this, "towerprojectile5", true);
	}
}
void HProjectile::Deactivate()
{
	AnimationStore::Stop(this);
	m_active = false;
}