// Archer.h

#pragma once

#include "AbstractEnemy.h"

class Archer : public AbstractEnemy
{
public:
	Archer(std::vector<Entity*>* entities, sf::Texture* unitTexture, sf::Texture* projectileTexture, Entity* mainTarget, int x, int y, Pathfinding* pathfinder);
	~Archer();

	SubType GetSubType();
	void Deactivate();
	void Reactivate(int x, int y);

private:
	void Initialize();
	void Attack();
	void AnimateMovement();
	void AnimateAttack();
	void PreGenerateProjectiles();

private:
	sf::Texture* m_projectileTexture;
};