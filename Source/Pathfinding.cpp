/// Pathfinding.cpp
//
#include "stdafx.h"
#include "Pathfinding.h"

#include "Tilemap.h"


Pathfinding::Pathfinding()
{
	m_tilemap = nullptr;
}
Pathfinding::~Pathfinding()
{
}


void Pathfinding::setTilemap(Tilemap* parent)
{
	m_tilemap = parent;

	m_node_vector.resize(m_tilemap->size());

	m_map_width = m_tilemap->size_width();
	m_map_height = m_tilemap->size_height();

	for (int index = 0; index < m_node_vector.size(); index++)
	{
		if (m_tilemap->getTile(index).state == 1)
			m_node_vector[index].passable = false;
		else
			m_node_vector[index].passable = true;
	}
}


std::vector<sf::Vector2f> Pathfinding::getPath(sf::Vector2f start_pos, sf::Vector2f goal_pos)
{
	short start_index = m_tilemap->getTile(start_pos).index;
	short goal_index = m_tilemap->getTile(goal_pos).index;

	return getPath(start_index, goal_index);
}

bool Pathfinding::IsInaccessible(sf::Vector2f targetLoc)
{
	short targetTile = m_tilemap->getTile(targetLoc).index;

	if (m_node_vector[targetTile].passable == false)
		return false;

	return true;
}

std::vector<sf::Vector2f> Pathfinding::getPath(short start_index, short goal_index)
{
	ResetNodes();
	setTilemap(m_tilemap);

	// horizontally/vertically = 1.0
	// diagonally = 1.4
	float score_list[8]{1.0f, 1.0f, 1.0f, 1.0f, 1.4f, 1.4f, 1.4f, 1.4f};

	// set start & finish
	m_start_index = start_index;
	m_goal_index = goal_index;
	AddNodeToList(m_open_list, &m_node_vector[start_index]);

	// searching for path ...
	while (true)//<--breaks when path is found, or not.
	{
		Node* node = FindLowestScoreInOpenList();

		if (!node)
			break;

		AddNodeToList(m_closed_list, node);

		// neighboring nodes
		Node* adj[8]
		{
			GetNodeAt(node->x, node->y-1),// up
			GetNodeAt(node->x, node->y+1),// down
			GetNodeAt(node->x-1, node->y),// left
			GetNodeAt(node->x+1, node->y),// right
			GetNodeAt(node->x-1, node->y-1), // up-left
			GetNodeAt(node->x+1, node->y-1), // up-right
			GetNodeAt(node->x-1, node->y+1), // down-left
			GetNodeAt(node->x+1, node->y+1)  // damn-right
		};

		// now calculating F=G+H
		for (int i = 0; i < 8; i++)
		{
			// filtering out unwanted nodes
			if (adj[i] == nullptr)	continue;
			if (adj[i]->passable == false)  continue;
			if (IsNodeInList(m_closed_list, adj[i]))  continue;
			if (IsNodeInList(m_open_list, adj[i]))  continue;

			adj[i]->parent = node;
			adj[i]->H = calculate_H(adj[i]->x, adj[i]->y);
			adj[i]->G = (node->G + score_list[i]) * 1.0f;
			adj[i]->F = adj[i]->H + adj[i]->G;

			AddNodeToList(m_open_list, adj[i]);

			if (adj[i]->H == 0)
				goto path_found;
		}

		RemoveNodeFromList(m_open_list, node);
	}


path_found:
	std::vector<sf::Vector2f> path_vector;
	Node* node = m_closed_list.back();
	while (node->parent)
	{
		// getting each position on path
		path_vector.push_back(m_tilemap->getTile(node->x, node->y).position);
		// next
		node = node->parent;
	}

	m_closed_list.clear();
	m_open_list.clear();

	return path_vector;
}



// prirates
Node* Pathfinding::GetNodeAt(unsigned x, unsigned y)
{
	if (x >= m_map_width || y >= m_map_height)
		return nullptr;

	unsigned index = y * m_map_width + x;

	if (m_node_vector[index].passable == false)
		return nullptr;

	return &m_node_vector[index];
}


Node* Pathfinding::FindLowestScoreInOpenList()
{
	if (m_open_list.empty())
		return nullptr;

	Node* lowest_node = m_open_list[0];

	for (int i = 0; i < m_open_list.size(); i++)
	{
		if (lowest_node->F > m_open_list[i]->F)
			lowest_node = m_open_list[i];
	}

	return lowest_node;
}



void Pathfinding::AddNodeToList(std::vector<Node*>& list, Node* node)
{
	list.push_back(node);
}

void Pathfinding::RemoveNodeFromList(std::vector<Node*>& list, Node* node)
{
	auto itr = list.begin();
	while (itr != list.end())
	{
		if ((*itr) == node)
		{
			list.erase(itr);
			break;
		}
		itr++;
	}
}



bool Pathfinding::IsNodeInList(std::vector<Node*>& list, Node* node)
{
	for (unsigned int index = 0; index < list.size(); index++)
	{
		if (list[index] == node)
			return true;
	}
	return false;
}



float Pathfinding::calculate_H(float x, float y)
{
	float dx = x - m_node_vector[m_goal_index].x;
	float dy = y - m_node_vector[m_goal_index].y;

	return fabs(dx) + fabs(dy);
}



void Pathfinding::ResetNodes()
{
	for (int index = 0; index < m_node_vector.size(); index++)
	{
		m_node_vector[index].x = index % m_map_width;
		m_node_vector[index].y = index / m_map_width;
		m_node_vector[index].F = 0;
		m_node_vector[index].G = 0;
		m_node_vector[index].H = 0;
		m_node_vector[index].parent = nullptr;
		//m_node_vector[index].passable = true???
	}
}