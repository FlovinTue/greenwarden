// GameState.hpp

#ifndef GAMESTATE_HPP_INCLUDED
#define GAMESTATE_HPP_INCLUDED

#include "AbstractState.hpp"
#include "Entity.h"

class TowerShop;
class Tilemap;
class AnimationStore;
class EnemySpawnPoint;
class LifeTree;
class Pathfinding;
class Player;
class TextureManager;
class DrawManager;
class AudioManager;

class GameState : public AbstractState
{
public:
	GameState();
	~GameState();

	virtual bool Enter();
	virtual void Exit();
	virtual bool Update(float deltatime);
	virtual void Draw();
	virtual std::string GetNextState();

	void InitializeMap(const char* filename);
	void SetDrawOrder();

private:
	std::vector<Entity*> m_entities;

	TextureManager* m_texture_manager;
	DrawManager* m_draw_manager;
	AudioManager* m_audio_manager;

	AnimationStore* m_animationStore;
	TowerShop* m_towerShop;
	Tilemap* m_tilemap;
	EnemySpawnPoint* m_enemySpawnPoint;
	std::vector<EnemySpawnPoint*> m_spawnpoints;


	Pathfinding* m_pathfinder;
	Player* m_player;
	LifeTree* m_lifetree;

	sf::Text m_seedcount1;
	sf::Text m_seedcount2;
	sf::Text m_seedcount3;
	sf::Text m_seedtipsR;
	sf::Text m_seedtipsRR;
	sf::Text m_seedtipsRG;
	sf::Text m_seedtipsRB;
	sf::Text m_seedtipsGG;
	sf::Text m_seedtipsGB;
	sf::Text m_seedtipsBB;
	sf::Text m_seedtipsG;
	sf::Text m_seedtipsB;
	sf::Text m_time_text;
	sf::Font m_font;
	sf::Sprite m_background;
	std::string* m_string1;

	sf::Texture m_lifetreehealthbaroverlaytexture;
	sf::Sprite m_liftreehealthbaroverlay;

	sf::Texture m_playerhealthbaroverlaytexture;
	sf::Sprite m_playerhealthbaroverlay;

	sf::Texture m_seedcountertexture;
	sf::Sprite m_seedcounter;

	sf::Texture m_seedcounterbackgroundtexture;
	sf::Sprite m_seedcounterbackground;

	short m_drawOrder[5000];
};
#endif // GAMESTATE_HPP_INCLUDED
