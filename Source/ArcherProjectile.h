// ArcherProjectile.h

#pragma once

#include "Entity.h"

class ArcherProjectile : public Entity
{
public:
	ArcherProjectile(int x, int y, sf::Texture* texture);
	~ArcherProjectile();

	void Update(float deltatime);
	EType GetType();
	void Reactivate(Entity* target, int x, int y);
	void Deactivate();

private:
	void HomeOnTarget(float deltatime);
	void ApplyProjectileEffect();

private:
	Entity* m_target;
	float m_speed;
	float m_spriteRotation;
	int m_damage;
	int m_effectDuration;
};