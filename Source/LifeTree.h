/// LifeTree.h
//
#pragma once
#include "Entity.h"

#include "Seed.h"
#include "DOTEffect.h"


class Seed;
class LifetreeHealthBar;
class EffectHost;
class HOTEffect;

class LifeTree : public Entity
{
public:
	LifeTree(std::vector<Entity*>* entities, sf::Texture* texture_lifetree);
	~LifeTree();

	void Update(float deltatime);

	void setPosition(sf::Vector2f position);
	void setActive(bool active);
	EType GetType();

	int GetCurrentHealth();
	int GetMaxHealth();
	void SubstractHealth(float value);
	void AddHealth(float value);

	void SubstractHealthOverTime(float totalDamage, int duration);
	void AddHealthOverTime(float totalHealing, int duration);
	void RefreshOverTimeEffects();
	
	void SpawnSeed(SEED_TYPE seedtype);

private:
	std::vector<Seed*> m_seeds;
	short m_seedcount;

	float m_time;
	float m_seed1_timer;
	float m_seed2_timer;
	float m_seed3_timer;
	float m_seed1_spawntime;
	float m_seed2_spawntime;
	float m_seed3_spawntime;

	float m_currentHealth;
	int m_maxHealth;

	EffectHost* m_effectHost;
	LifetreeHealthBar* m_lifeTreeBar;

	std::vector<DOTEffect> m_doteffects;
	std::vector<HOTEffect> m_hOTEffects;
};