// main.cpp

#include "stdafx.h"
#include "Engine.hpp"

int main(int argc, char* argv[])
{
	Engine engine;
	if (engine.Initialize())
		engine.Run();
	engine.Shutdown();
	return 0;
}
