// Entity.h (Abstract)
#pragma once

#include "Collider.h"

enum EType
{
	E_PLAYER,
	E_LIFETREE,
	E_TOWER,
	E_WORLD,
	E_BACKGROUND,
	E_SEED,
	E_GUI,
	E_ENEMY,
	E_HPROJECTILE,
	E_PPROJECTILE,
	E_HEALTHBAR,
	E_UNKNOWN,
	E_POWERUP,
	E_PLAYERPROJECTILE,
	E_ARCHERPROJECTILE,
	E_FOREST,
	E_HUD,
	E_EFFECTHOST
};

class Entity
{
public:
	virtual ~Entity() {};

	virtual void Update(float deltatime);
	virtual sf::Sprite* GetSprite();
	virtual float GetX();
	virtual float GetY();
	virtual int GetW();
	virtual int GetH();
	virtual bool IsActive();
	virtual EType GetType();
	virtual Collider* GetCollider();
	virtual void Deactivate();

protected:
	float m_x;
	float m_y;
	int m_w;
	int m_h;
	Collider* m_collider;
	bool m_active;
	sf::Sprite m_sprite;
};