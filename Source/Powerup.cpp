#include "stdafx.h"
#include "Powerup.h"
#include "CollisionManager.hpp"

PowerUp::PowerUp(std::vector<Entity*>* entities, int type, float x, float y)
{
	m_type = type;
	m_x = x;
	m_y = y;
	sf::IntRect r1(257, 31, 15, 16);
	switch (m_type)
	{
	case 1:
		m_texture.loadFromFile("assets/StaticSprites/pinepowerup.png");
		break;
	case 2:
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	case 3:
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	case 4: 
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	}
	
	sf::Sprite spriteavatar(m_texture);
	m_sprite = spriteavatar;
	m_collider = new Collider(m_x, m_y, 32,32);
	m_collider->SetParent(this);
	m_active = true;
}

PowerUp::~PowerUp()
{
	if (m_collider)
		delete m_collider;
}

void PowerUp::Update(float deltatime)
{
	//m_x = 700.0f;
	//m_y = 700.0f;
	m_sprite.setPosition(m_x, m_y);
	m_collider->Refresh();
}

EType PowerUp::GetType()
{
	return E_POWERUP;
}

int PowerUp::PowerUpType()
{
	return m_type;
}

void PowerUp::Reactivate(int type, float x, float y)
{
	m_x = x;
	m_y = y;

	m_active = true;
	m_type = type;
	switch (m_type)
	{
	case 1:
		m_texture.loadFromFile("assets/StaticSprites/pinepowerup.png");
		break;
	case 2:
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	case 3:
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	case 4:
		m_texture.loadFromFile("assets/StaticSprites/maxtower.png");
		break;
	}

	sf::Sprite spriteavatar(m_texture);
	m_sprite = spriteavatar;
}