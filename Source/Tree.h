/// Tree.h
//
#pragma once

#include "Entity.h"


class Tree : public Entity
{
public:
	Tree(sf::Vector2f position);
	~Tree();

	static void addTexture(sf::Texture* texture) { m_static_textures.push_back(texture); };
	static void clearTextures() { m_static_textures.clear(); };

	EType GetType();

private:
	static std::vector<sf::Texture*> m_static_textures;
};