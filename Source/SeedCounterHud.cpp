#include "stdafx.h"
#include "SeedCounterHud.h"
#include "Player.h"
SeedCounterHud::SeedCounterHud(Player* host, float x, float y, int type)
{
	m_player = host;
	m_x = x;
	m_y = y; 
	m_type = type;
	//m_rect = sf::IntRect(0, 0, 64, 64);
	switch (type)
	{
	case 1:
		m_texture.loadFromFile("assets/StaticSprites/Red0.png");
		break;
	case 2:
		m_texture.loadFromFile("assets/StaticSprites/Green0.png");
		break;
	case 3:
		m_texture.loadFromFile("assets/StaticSprites/Blue0.png");
		break;
	}
	
	m_sprite.setTexture(m_texture);
	m_sprite.setPosition(m_x, m_y);
	m_active = true;
}
SeedCounterHud::~SeedCounterHud()
{

}

void SeedCounterHud::Update(float deltatime)
{
	switch (m_type)
	{
	case 1:
		if (!m_player->Getm_seed1_active()){
			m_texture.loadFromFile("assets/StaticSprites/Red0.png");
		}
		if (m_player->Getm_seed1_2_active()){
			m_texture.loadFromFile("assets/StaticSprites/Red2.png");
		}
		else if (m_player->Getm_seed1_active()){
			m_texture.loadFromFile("assets/StaticSprites/Red1.png");
		}
	break;

	case 2:
		if (!m_player->Getm_seed2_active()){
			m_texture.loadFromFile("assets/StaticSprites/Green0.png");
		}
		if (m_player->Getm_seed2_2_active()){
			m_texture.loadFromFile("assets/StaticSprites/Green2.png");
		}
		else if (m_player->Getm_seed2_active()){
			m_texture.loadFromFile("assets/StaticSprites/Green1.png");
		}
		break;

	case 3:
		if (!m_player->Getm_seed3_active()){
			m_texture.loadFromFile("assets/StaticSprites/Blue0.png");
		}
		if (m_player->Getm_seed3_2_active()){
			m_texture.loadFromFile("assets/StaticSprites/Blue2.png");
		}
		else if (m_player->Getm_seed3_active()){
			m_texture.loadFromFile("assets/StaticSprites/Blue1.png");
		}
		break;
	}
}
EType SeedCounterHud::GetType()
{
	return E_HUD;
}
void SeedCounterHud::PlayAnimation()
{

}