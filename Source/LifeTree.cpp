/// LifeTree.cpp
//
#include "stdafx.h"
#include "LifeTree.h"

#include "Entity.h"
#include "Seed.h"

#include "HOTEffect.h"
#include "DOTEffect.h"
#include "AnimationStore.h"
#include "AttributeManager.h"
#include "LifeTreeHealthbars.h"
#include "EffectHost.h"

// 
#define MAX_SEEDS_ONSCREEN 50
// spawn area
#define SEED_SPAWNRADIUS_MAX 90
#define SEED_SPAWNRADIUS_MIN 80
// spawntime on start
#define SEED1_SPAWNTIME_BASE 15
#define SEED2_SPAWNTIME_BASE 10.0
#define SEED3_SPAWNTIME_BASE 20.0
// spawntime-alteration/minute
#define SEED1_SPAWNTIME_ALTER -1
#define SEED2_SPAWNTIME_ALTER -0.5
#define SEED3_SPAWNTIME_ALTER -1.5
// numbers of spawned seeds on start
#define SEED1_ON_START 3
#define SEED2_ON_START 4
#define SEED3_ON_START 2

LifeTree::LifeTree(std::vector<Entity*>* entities, sf::Texture* texture_lifetree)
{
	m_active = true;
	m_sprite.setTexture(*texture_lifetree);
	m_sprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().width / 2);
	m_maxHealth = AttributeManager::GetAttribute("LifeTree_MaxHealth");
	m_currentHealth = m_maxHealth;

	m_seedcount = 0;
	m_time = 0.0f;

	// pre-allocating a max number of seeds
	for (int i = 0; i < MAX_SEEDS_ONSCREEN; i++)
	{
		m_seeds.push_back(new Seed());
		entities->push_back(m_seeds[i]);
	}

	m_sprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().width / 2);

	entities->push_back(m_lifeTreeBar = new LifetreeHealthBar(this));
	AnimationStore::Play(this, "lifetreeidle", true);

	m_effectHost = new EffectHost(this);
	entities->push_back(m_effectHost);
	m_collider = new Collider(m_x, m_y, m_w, m_h);
	m_collider->SetParent(this);
}

LifeTree::~LifeTree()
{
	if (m_collider)
		delete m_collider;
}



// ----- [UPDATE] -----
void LifeTree::Update(float deltatime)
{
	// timers++
	m_time += deltatime;
	m_seed1_timer += deltatime;
	m_seed2_timer += deltatime;
	m_seed3_timer += deltatime;
	// spawntimes, increasing over time	 ->  y=mx+b 'where' m=[spawntime-change/min]/60, x=time, b=[start-spawntime]
	m_seed1_spawntime = (SEED1_SPAWNTIME_ALTER / 60.0f) * m_time + SEED1_SPAWNTIME_BASE;
	m_seed2_spawntime = (SEED2_SPAWNTIME_ALTER / 60.0f) * m_time + SEED2_SPAWNTIME_BASE;
	m_seed3_spawntime = (SEED3_SPAWNTIME_ALTER / 60.0f) * m_time + SEED3_SPAWNTIME_BASE;
	// spawn a seed of type...
	//...1
	if (m_seed1_timer >= m_seed1_spawntime) { SpawnSeed(SEED_TYPE_1); m_seed1_timer = 0; };
	//...2
	if (m_seed2_timer >= m_seed2_spawntime) { SpawnSeed(SEED_TYPE_2); m_seed2_timer = 0; };
	//...3
	if (m_seed3_timer >= m_seed3_spawntime) { SpawnSeed(SEED_TYPE_3); m_seed3_timer = 0; };


	RefreshOverTimeEffects();
}



// ----- [SEED SPAWNING] -----
void LifeTree::SpawnSeed(SEED_TYPE seedtype)
{
	// trigometry
	int hypotenuse = rand() % (SEED_SPAWNRADIUS_MAX - SEED_SPAWNRADIUS_MIN) + SEED_SPAWNRADIUS_MIN;
	int angle = rand() % 360;
	float posX = cos(angle) * hypotenuse;
	float posY = sin(angle) * hypotenuse;
	// initialize new seed
	m_seeds[m_seedcount]->setSeedType(seedtype);
	m_seeds[m_seedcount]->setPosition(posX + this->m_x, posY + this->m_y);
	m_seeds[m_seedcount++]->setActive(true);
	// reset seed counter if
	if (m_seedcount == MAX_SEEDS_ONSCREEN) { m_seedcount = 0; }
}



void LifeTree::setPosition(sf::Vector2f position)
{
	m_x = position.x;
	m_y = position.y;
	m_sprite.setPosition(position);
	m_collider->Refresh();
	// spawn couple of seeds on start
	for (int i = 0; i < SEED1_ON_START; i++) SpawnSeed(SEED_TYPE_1);
	for (int i = 0; i < SEED2_ON_START; i++) SpawnSeed(SEED_TYPE_2);
	for (int i = 0; i < SEED3_ON_START; i++) SpawnSeed(SEED_TYPE_3);
}


void LifeTree::setActive(bool active)
{
	m_active = active;
}



int LifeTree::GetCurrentHealth()
{
	return m_currentHealth;
}
void LifeTree::SubstractHealth(float value)
{
	int i = m_currentHealth;
	m_currentHealth -= value;
	if (m_currentHealth <= 0)
	{
		m_active = false;
	}
}

EType LifeTree::GetType()
{
	return E_LIFETREE;
}


void LifeTree::SubstractHealthOverTime(float totalDamage, int duration)
{
	DOTEffect doteffect;

	doteffect.m_check = 0;
	doteffect.m_totalDamage = totalDamage;
	doteffect.m_duration = duration;
	doteffect.m_timer.restart();

	m_doteffects.push_back(doteffect);

	if (AnimationStore::GetActiveAnimation(m_effectHost) != "healingeffect")
		AnimationStore::Play(m_effectHost, "fireeffect1", true);
}
void LifeTree::AddHealthOverTime(float totalHealing, int duration)
{
	HOTEffect hOTEffect;

	hOTEffect.m_check = 0;
	hOTEffect.m_totalHealing = totalHealing;
	hOTEffect.m_duration = duration;
	hOTEffect.m_timer.restart();

	m_hOTEffects.push_back(hOTEffect);

	AnimationStore::Play(m_effectHost, "healingeffect", true);
}
void LifeTree::RefreshOverTimeEffects()
{
	if (m_doteffects.size() > 0)
	{
		for (int i = 0; i < m_doteffects.size(); i++)
		{
			if (m_doteffects[i].m_timer.getElapsedTime().asSeconds() >= (m_doteffects[i].m_check + 1))
			{
				int tickDamage = m_doteffects[i].m_totalDamage / m_doteffects[i].m_duration;
				SubstractHealth(tickDamage);
				m_doteffects[i].m_check++;

				if (m_doteffects[i].m_check >= m_doteffects[i].m_duration)
					m_doteffects.erase(m_doteffects.begin() + i);
			}
		}
		if (m_doteffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}
	if (m_hOTEffects.size() > 0)
	{
		for (int i = 0; i < m_hOTEffects.size(); i++)
		{
			if (m_hOTEffects[i].m_timer.getElapsedTime().asSeconds() >= (m_hOTEffects[i].m_check + 1))
			{
				float tickHealing = m_hOTEffects[i].m_totalHealing / m_hOTEffects[i].m_duration;
				AddHealth(tickHealing);
				m_hOTEffects[i].m_check++;

				if (m_hOTEffects[i].m_check >= m_hOTEffects[i].m_duration)
					m_hOTEffects.erase(m_hOTEffects.begin() + i);
			}
		}
		if (m_hOTEffects.size() == 0)
			AnimationStore::Stop(m_effectHost);
	}
}
void LifeTree::AddHealth(float value)
{
	m_currentHealth += value;
	if (m_currentHealth >= m_maxHealth)
		m_currentHealth = m_maxHealth;
}
int LifeTree::GetMaxHealth()
{
	return m_maxHealth;
}