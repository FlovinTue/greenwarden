#pragma once

#include "Entity.h"

class Player;
class SeedCounterHud : public Entity
{
public:
	SeedCounterHud(Player* host, float x, float y, int type);
	~SeedCounterHud();

	void Update(float deltatime);
	EType GetType();
	void PlayAnimation();



private:
	int m_type;
	Player* m_player;
	//sf::Sprite m_sprite;
	sf::Texture m_texture;
	//sf::IntRect m_rect;
};