// AnimationStore.h

#pragma once

#include "Entity.h"
#include "TextureManager.hpp"

struct Animation
{
	Entity* m_host;

	std::vector<sf::IntRect> m_spriteCords;

	sf::Texture* m_spriteSheet;
	sf::Texture* m_idleTexture;

	std::string m_animationName;

	float m_timer;
	float m_frameDuration;

	int m_currentFrame;

	bool m_looping;
};

class AnimationStore
{
public:
	AnimationStore(TextureManager* textureManager);
	~AnimationStore();

	void Update(float deltatime);
	void AddAnimation(std::string animationName, std::string defFilePath, std::string spriteSheet, float totalDuration, std::string idleSpritePath);

	static void Play(Entity* host, std::string animationName, bool looping);
	static void Stop(Entity* host);
	static std::string GetActiveAnimation(Entity* host);

private:
	void Initialize();

private:
	static std::vector<Animation> m_animationStore;
	static std::vector<Animation> m_activeAnimations;

	TextureManager* m_texture_manager;
};