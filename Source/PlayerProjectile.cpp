#include "stdafx.h"
#include "PlayerProjectile.h"
#include "CollisionManager.hpp"
#include "AbstractEnemy.h"
PlayerProjectile::PlayerProjectile( std::vector<Entity*>* entities, int direction, int damage, int speed, float x, float y)
{
	m_entities = entities;
	m_damage = damage;
	m_speed = speed;
	m_direction = direction;
	//sf::IntRect r1(257, 31, 15, 16);
	m_texture.loadFromFile("assets/StaticSprites/p.png");
	sf::Sprite spriteavatar(m_texture);
	m_sprite = spriteavatar;
	m_x = x;
	m_y = y;
	start_x = x;
	start_y = y;
	m_active = true;

	m_collider = new Collider(m_x, m_y, 10, 10);
	m_collider->SetParent(this);
}

PlayerProjectile::~PlayerProjectile(){
	if (m_collider) {
		delete m_collider;
	}
}

void PlayerProjectile::Update(float deltatime)
{
	switch (m_direction)
	{
	case 1: 
		m_y -= m_speed * deltatime;
		break;
	case 2:
		m_y += m_speed * deltatime;
		break;
	case 3:
		m_x += m_speed * deltatime;
		break;
	case 4: 
		m_x -= m_speed * deltatime;
		break;
	}
		
	//m_y -= 1000.0f * deltatime;
	
	if (m_x > start_x + 128){
		m_active = false;
		
	}
	if (m_x < start_x - 128)
	{
		m_active = false;
		
	}
	if (m_y < start_y - 128)
	{
		m_active = false;
		
	}
	if (m_y > start_y + 128){
		m_active = false;
		
	}
	for (unsigned int i = 0; i < m_entities->size(); i++)
	{
		if (m_entities->at(i)->GetType() == E_ENEMY)
		{

		//	Enemy* enemy = static_cast<Enemy*>(m_entities->at(i));
			AbstractEnemy* enemy = static_cast<AbstractEnemy*>(m_entities->at(i));
			int overlapX = 2;
			int overlapY = 2;
			if (enemy->IsActive() == true) {


				if (CollisionManager::Check(m_collider, enemy->GetCollider(), overlapX, overlapY) == true)
				{
					enemy->SubstractHealth(m_damage);
					Deactivate();
				}
			}
		}

	}
	m_sprite.setPosition(m_x, m_y);
	m_collider->Refresh();
}
EType PlayerProjectile::GetType(){
	return E_PLAYERPROJECTILE;
}

void PlayerProjectile::Reactivate(std::vector<Entity*>* entities, int direction, int damage, int speed, float x, float y)
{
	
		m_entities = entities;
		m_active = true;
		m_direction = direction;
		m_damage = damage;
		m_speed = speed;
		m_x = x;
		m_y = y;
		start_x = x; 
		start_y = y;
	
}